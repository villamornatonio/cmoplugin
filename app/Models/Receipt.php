<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Receipt
 * @package App\Models
 */
class Receipt extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'trainee_id',
        'room_id',
        'room_name',
        'course_id',
        'course_name',
        'course_code',
        'course_date',
        'course_time',
        'discount',
        'or_no',
        'price',
        'total_amount',
        'amount_paid',
        'remaining_balance',
        'created_by',
        'company_id'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trainee()
    {
        return $this->belongsTo('App\Models\Trainee');
    }
}
