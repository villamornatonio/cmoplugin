<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'shopify_order_id',
        'user_id',
        'item_name',
        'amount',
        'buyer',
        'buyer_email',
        'buyer_phone',
        'street',
        'province',
        'city',
        'barangay',
        'zip',
        'status',
        'tracking_number',

    ];
}
