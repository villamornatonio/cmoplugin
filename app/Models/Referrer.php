<?php

namespace App\Models;

use App\Events\ModelWasChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Referrer
 * @package App\Models
 */
class Referrer extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'company', 'email', 'contact_no', 'dob','company_id'];

    /**
     *
     */
    public static function boot()
    {
        static::saved(function () {
            event(new ModelWasChanged());
        });

        static::deleted(function () {
            event(new ModelWasChanged());
        });

    }
}
