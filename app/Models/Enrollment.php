<?php

namespace App\Models;

use App\Events\ModelWasChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Enrollment
 * @package App\Models
 */
class Enrollment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'trainee_id',
        'schedule_id',
        'referred_by',
        'is_company_charged',
        'is_completed',
        'created_by',
        'company',
        'has_fully_paid',
        'has_partially_paid',
        'company_id'
    ];

    /**
     *
     */
    public static function boot()
    {
        static::saved(function () {
            event(new ModelWasChanged());
        });

        static::deleted(function () {
            event(new ModelWasChanged());
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function trainee()
    {
        return $this->belongsTo('App\Models\Trainee');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room()
    {
        return $this->belongsTo('App\Models\Room');

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function schedule()
    {
        return $this->belongsTo('App\Models\Schedule');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    /**
     * @return string
     */
    public function enrollees()
    {
        return '23';
    }
}
