<?php

namespace App\Models;

use App\Events\ModelWasChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Course
 * @package App\Models
 */
class Course extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'price', 'duration', 'created_by','company_id'];

    /**
     *
     */
    public static function boot()
    {
        static::saved(function () {
            event(new ModelWasChanged());
        });

        static::deleted(function () {
            event(new ModelWasChanged());
        });

    }


}
