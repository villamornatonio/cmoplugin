<?php

namespace App\Models;

use App\Events\ModelWasChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Trainee
 * @package App\Models
 */
class Trainee extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'position',
        'firstname',
        'middlename',
        'lastname',
        'passport_no',
        'seamans_no',
        'mobile',
        'dob',
        'email',
        'emergency_person',
        'emergency_contact',
        'avatar',
        'created_by',
        'company_id',
        'temp_id'
    ];

    /**
     *
     */
    public static function boot()
    {
        static::saved(function () {
            event(new ModelWasChanged());
        });

        static::deleted(function () {
            event(new ModelWasChanged());
        });

    }
}
