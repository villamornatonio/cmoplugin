<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment
 * @package App\Models
 */
class Payment extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'amount',
        'remaining_balance',
        'price',
        'trainee_id',
        'discount',
        'or_no',
        'paid_enrollment_ids',
        'created_by',
        'company_id'
    ];
}
