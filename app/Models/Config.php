<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'configs';
    protected $fillable = [
        'shop_url',
        'name',
        'address',
        'phone',
        'user_id',
        'shop_hostname',
        'api_key',
        'password',
        'location_id',
        'tracking_company'
    ];
}

