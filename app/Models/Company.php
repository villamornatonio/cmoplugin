<?php

namespace App\Models;

use App\Events\ModelWasChanged;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

/**
 * Class Company
 * @package App\Models
 */
class Company extends Model
{
    protected $table = 'companies';
    /**
     * @var array
     */
    protected $fillable = ['name', 'phone','address','email','short_name','logo_url'];

}


