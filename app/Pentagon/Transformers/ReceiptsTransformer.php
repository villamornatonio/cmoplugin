<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/20/15
 * Time: 7:08 AM
 */

namespace Transformers;


use Carbon\Carbon;

class ReceiptsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($receipt)
    {
        return [
            'id'    => $receipt['id'],
            'trainee_id'    => $receipt['trainee_id'],
            'temp_id'    => $receipt['trainee']['temp_id'],
            'avatar'    => $receipt['trainee']['avatar'],
            'name'    => $receipt['trainee']['firstname'] . ' ' . $receipt['trainee']['lastname'],
            'lastname'    => $receipt['trainee']['lastname'],
            'firstname'    => $receipt['trainee']['firstname'],
            'middlename'    => $receipt['trainee']['middlename'],
            'seamans'    => $receipt['trainee']['seamans_no'],
            'position'    => $receipt['trainee']['position'],
            'passport'    => $receipt['trainee']['passport_no'],
            'dob'    => Carbon::parse($receipt['trainee']['dob'])->toFormattedDateString(),
            'company'    => isset($receipt['trainee']['company']) ? $receipt['trainee']['company'] : 'NONE',
            'mobile'    => $receipt['trainee']['mobile'],
            'email'    => $receipt['trainee']['email'],
            'course'    => $receipt['course_name'],
            'code'    => $receipt['course_code'],
            'price'    => 'PHP' . ' ' . number_format($receipt['price']),
            'date'    => $receipt['course_date'],
            'time'    => $receipt['course_time'],
            'room'    => $receipt['room_name'],
            'or_no'    => $receipt['or_no'],
            'discount'    => $receipt['discount'] . '%',
            'amount_paid'    => 'PHP' . ' ' . number_format($receipt['amount_paid']),
            'total_amount'    => 'PHP' . ' ' . number_format($receipt['total_amount']),
            'remaining_balance'    => 'PHP' . ' ' . number_format($receipt['remaining_balance']),
        ];
    }
}