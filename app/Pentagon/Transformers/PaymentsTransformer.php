<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:36 AM
 */

namespace Transformers;


class PaymentsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($payment)
    {
        return [
            'id'    => $payment['id'],
            'amount'    => 'PHP ' . number_format($payment['amount']),
            'remainingBalance'    => 'PHP ' . number_format($payment['remaining_balance']),
            'price'    => 'PHP ' . number_format($payment['price']),
            'discount'    => 'PHP ' . number_format($payment['discount']),
            'orNo'    => $payment['or_no'],
            'trainee'    => $payment['trainee'],
            'author'    => $payment['author']
        ];
    }
}
