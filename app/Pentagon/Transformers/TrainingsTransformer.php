<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 4:50 PM
 */

namespace Transformers;


use Carbon\Carbon;

class TrainingsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($training)
    {
        return [
            'id' => $training['id'],
            'enrollment_id' => $training['enrollment_id'],
            'name' => $training['trainee'],
            'course' => $training['course'],
            'startDate'    => Carbon::parse($training['start_date'])->toFormattedDateString(),
            'endDate'    => Carbon::parse($training['end_date'])->toFormattedDateString(),
            'startTime'    => Carbon::parse($training['start_time'])->format('h:i A'),
            'endTime'    => Carbon::parse($training['end_time'])->format('h:i A'),
            'completed'    => ($training['is_completed'] == 1) ? 'YES' : 'NO',
            'companyCharge'    => ($training['is_company_charged'] == 1) ? 'YES' : 'NO'
        ];
    }
}