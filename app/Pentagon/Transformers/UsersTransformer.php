<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:38 AM
 */

namespace Transformers;


class UsersTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($user)
    {
        return [
            'id'    => $user['id'],
            'name'    => $user['name'],
            'email'    => $user['email'],
            'role'    => $user['role'],
            'avatar'    => $user['avatar'],
            'mobile'    => $user['contact_no'],
            'active'    => ($user['is_activated'] == 1) ? 'YES' : 'NO',

        ];
    }
}