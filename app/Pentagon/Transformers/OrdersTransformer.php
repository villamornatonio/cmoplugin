<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:36 AM
 */

namespace Transformers;


use Carbon\Carbon;

class OrdersTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($item)
    {
        return [
            'id'    => $item['id'],
            'buyer'    => $item['buyer'],
            'item_name'    => $item['item_name'],
            'amount'    => $item['amount'],
            'buyer_phone'    => $item['buyer_phone'],
            'status'    => $item['status'],
        ];
    }
}

