<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:37 AM
 */

namespace Transformers;


use Carbon\Carbon;

class ReferrersTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($referrer)
    {
        return [
            'id'    => $referrer['id'],
            'firstname'    => $referrer['firstname'],
            'lastname'    => $referrer['lastname'],
            'name'    => $referrer['firstname'] . ' ' . $referrer['lastname'],
            'mobile'    => $referrer['contact_no'],
            'company'    => $referrer['company'],
            'email'    => $referrer['email'],
            'dob'    => Carbon::parse($referrer['dob'])->toFormattedDateString(),
        ];
    }
}
