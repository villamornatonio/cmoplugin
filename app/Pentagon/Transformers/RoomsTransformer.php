<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/14/15
 * Time: 11:16 AM
 */

namespace Transformers;


class RoomsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($room)
    {
        return [
            'id'    => $room['id'],
            'name' => $room['name'],
            'author' => $room['author']['firstname'] . ' ' . $room['author']['lastname']
        ];
    }
}