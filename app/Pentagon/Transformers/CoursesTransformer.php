<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:35 AM
 */

namespace Transformers;

class CoursesTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($course)
    {
        return [
            'id'    => $course['course_id'],
            'name'    => $course['course_name'],
            'code'    => strtoupper($course['course_code']),
            'price'    => 'PHP ' . number_format($course['course_price']),
            'duration'    => $course['course_duration'] . ' Day/s',
            'author'    => $course['author'],
        ];
    }
}
