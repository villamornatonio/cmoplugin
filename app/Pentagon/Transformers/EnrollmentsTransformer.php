<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 5:36 AM
 */

namespace Transformers;


use Carbon\Carbon;

class EnrollmentsTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($enrollment)
    {
        return [
            'id'    => $enrollment['id'],
            'trainee'    => $enrollment['trainee'],
            'course'    => $enrollment['course'],
            'room'    => $enrollment['room'],
            'price'    => 'PHP ' . number_format($enrollment['price']),
            'startDate'    => Carbon::parse($enrollment['start_date'])->toFormattedDateString(),
            'endDate'    => Carbon::parse($enrollment['end_date'])->toFormattedDateString(),
            'startTime'    => Carbon::parse($enrollment['start_time'])->format('h:i A'),
            'endTime'    => Carbon::parse($enrollment['end_time'])->format('h:i A'),
            'completed'    => ($enrollment['is_completed'] == 1) ? 'YES' : 'NO',
            'companyCharge'    => ($enrollment['is_company_charged'] == 1) ? 'YES' : 'NO',
            'author'    => $enrollment['author']
        ];
    }
}

