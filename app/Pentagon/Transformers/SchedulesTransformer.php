<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/15/15
 * Time: 12:11 PM
 */

namespace Transformers;


use Carbon\Carbon;

class SchedulesTransformer extends Transformer{

    /**
     * @param $item
     * @return mixed
     */
    public function transform($schedule)
    {
        return [
            'id'    => $schedule['id'],
            'room'    => $schedule['room'],
            'course'    => $schedule['course'],
            'price'    => 'PHP ' . number_format($schedule['price']),
            'startDate'    => Carbon::parse($schedule['start_date'])->toFormattedDateString(),
            'endDate'    => Carbon::parse($schedule['end_date'])->toFormattedDateString(),
            'startTime'    => Carbon::parse($schedule['start_time'])->format('h:i A'),
            'endTime'    => Carbon::parse($schedule['end_time'])->format('h:i A')
        ];
    }
}

