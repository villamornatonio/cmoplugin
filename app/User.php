<?php

namespace App;

use App\Events\ModelWasChanged;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

/**
 * Class User
 * @package App
 */
class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;
    /**
     * @var array
     */
    protected $dates = ['deleted_at'];
    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'password',
        'contact_no',
        'avatar',
        'is_activated',
        'role_id'
    ];

    /**
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     *
     */
    public static function boot()
    {
        static::saved(function () {
            event(new ModelWasChanged());
        });

        static::deleted(function () {
            event(new ModelWasChanged());
        });

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    /**
     * @return string
     */
    public function name()
    {
        return ucwords($this->firstname) . ' ' . ucwords($this->lastname);
    }

    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }

    public function scopeCompany($query)
    {
        return $query->where('company',session('company.id'));
    }
}
