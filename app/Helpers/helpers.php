<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/13/15
 * Time: 12:58 PM
 */

use Ixudra\Curl\Facades\Curl;

if (!function_exists('pre')) {
    function pre($value, $exit = false)
    {

        echo '<pre>';
        print_r($value);
        echo '</pre>';

        if ($exit) {
            exit;
        }
    }
}

if (!function_exists('set_active')) {
    function set_active($path, $active = 'active')
    {

        return call_user_func_array('Request::is', (array)$path) ? $active : '';

    }
}

if (!function_exists('percentage')) {
    function percentage($value, $totalValue)
    {
        $percentage = ($value / $totalValue) * 100;

        return round($percentage);
    }
}

if (!function_exists('getListMonths')) {
    function getListMonths()
    {
        $currentMonth = \Carbon\Carbon::now();

        $months[$currentMonth->firstOfMonth()->format('Y-m-d')] = $currentMonth->firstOfMonth()->format('M Y');
        for ($i = 1; $i <= 11; $i++) {
            $months[\Carbon\Carbon::now()->subMonth($i)->firstOfMonth()->format('Y-m-d')] = \Carbon\Carbon::now()->subMonth($i)->format('M Y');
        }

        return $months;
    }
}

if (!function_exists('getListWeeks')) {
    function getListWeeks()
    {
        $currentWeek = \Carbon\Carbon::now();

        $weeks[$currentWeek->format('Y-m-d')] = 'Week ' . $currentWeek->format("$currentWeek->weekOfYear Y");
        for ($i = 1; $i <= 23; $i++) {
            $weeks[\Carbon\Carbon::now()->subWeek($i)->format('Y-m-d')] = 'Week ' . \Carbon\Carbon::now()->subWeek($i)->format('W Y');
        }

        return $weeks;
    }
}

if (!function_exists('cleanText')) {
    function cleanText($text)
    {
        $response = '';

        $response = str_replace("'",'',$text);

        return $response;
    }
}

if (!function_exists('getCheckMeOutPay')) {
    function getCheckMeOutPay($amount)
    {
        $response = 0;
        $checkmeOutFee = 100;
        $checkmeOutInsurancePercentage = 0.01;

        $checkMeOutInsuranceAmount = $amount * $checkmeOutInsurancePercentage;
        if($checkMeOutInsuranceAmount < 5){
            $checkMeOutInsuranceAmount = 5;
        }


        $checkMeOutTotalDeduction = $checkMeOutInsuranceAmount + $checkmeOutFee;
        $checkMeOutTOtalAmount = $amount - $checkMeOutTotalDeduction;
        $response = round($checkMeOutTOtalAmount);



        return $response;
    }
}


if (!function_exists('getLocation')) {
    function getLocation($zip)
    {
        $response = NULL;
        $data = [
            'city' => 'Talisay City',
            'province' => 'Cebu'

        ];
        $response = $data;

        return $response;
    }
}


if (!function_exists('getLocationByCity')) {
    function getLocationByCity($city)
    {
        $response = NULL;
        $data = [
            'city' => '',
            'province' => ''

        ];
        $googleAPIKey = config('google.api_key');
        $googleURL = config('google.api_url');
        $county = config('google.api_country');
        $city = urlencode($city);
        $url = "{$googleURL}{$city}+{$county}&key={$googleAPIKey}";
        $curlData = Curl::to($url)
            ->asJson(true)
            ->get();
        if(!isset($curlData['results'][0]['address_components'][0]['long_name'])){
            return $data;
        }
        $response = [
            'city' => $curlData['results'][0]['address_components'][0]['long_name'],
            'province' => $curlData['results'][0]['address_components'][1]['long_name'],
        ];

        return $response;
    }
}


