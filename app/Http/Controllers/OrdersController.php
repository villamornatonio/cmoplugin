<?php

namespace App\Http\Controllers;



use App\Models\Config;
use App\Models\Order;
use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Ixudra\Curl\Facades\Curl;
use Maatwebsite\Excel\Excel;

class OrdersController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {

        return view('orders.index');
    }


    public function create()
    {
        return view('orders.create');
    }

    public function import()
    {
        return view('orders.import');
    }

    public function export()
    {
        return view('orders.export');
    }

    public function postExport(Request $request)
    {
        include app_path('Libraries/parsecsv.lib.php');
        $file = $request->file('orders');
        $path = $file;
        $csv = new \parseCSV();
        $csv->auto($path);
        $shopifyOrderDatas = $csv->data;
        $cmoPreparedData = [];

        foreach ($shopifyOrderDatas as $key => $shopifyOrderData){
            $email = "no@email.com";
            if($shopifyOrderData['Email'] != ""){
                $email = $shopifyOrderData['Email'];
            }

            // GET LOCATION
            $province = "";
            $city = cleanText($shopifyOrderData['Shipping City']);
            $zip = cleanText($shopifyOrderData['Shipping Zip']);
            $locations = getLocationByCity($city);
            if(!is_null($locations)){
                $city = $locations['city'];
                $province = $locations['province'];
            }
            $cmoPreparedData[] = [
                'item_name' => $shopifyOrderData['Lineitem name'],
                'item_amount' => $shopifyOrderData['Total'],
                'item_buyer_name' => ucwords($shopifyOrderData['Shipping Name']),
                'item_buyer_email' => strtolower($email),
                'item_buyer_mobile_number' => cleanText(trim(str_replace(' ','',$shopifyOrderData['Shipping Phone']))),
                'item_street_address' => ucwords($shopifyOrderData['Shipping Address1']),
                'item_province' => $province,
                'item_city_municipality' => $city,
                'item_banragay' => cleanText($shopifyOrderData['Shipping Address2']),
                'item_zipcode' => $zip,
            ];


            $orderData = [
                'shopify_order_id' => $shopifyOrderData['Id'],
                'user_id' => auth()->user()->id,
                'item_name' => $shopifyOrderData['Lineitem name'],
                'buyer' => ucwords($shopifyOrderData['Shipping Name']),
                'buyer_email' => strtolower($email),
                'buyer_phone' => cleanText(trim(str_replace(' ','',$shopifyOrderData['Shipping Phone']))),
                'street' => ucwords($shopifyOrderData['Shipping Address1']),
                'province' => $province,
                'city' => $city,
                'barangay' => cleanText($shopifyOrderData['Shipping Address2']),
                'zip' => $zip,
                'amount' => $shopifyOrderData['Total'],
                'status' => "IMPORTED FROM SHOPIFY",

            ];

            Order::updateOrCreate($orderData);
        }



        $cmoSamplePath = app_path('Libraries/files/cmo-sample-template.xlsx');

        \Excel::load($cmoSamplePath, function ($reader) use ($cmoPreparedData) {

            $sheet = $reader->setActiveSheetIndexByName("COD Orders");
            $startingSheetIndex = 3;

            foreach ($cmoPreparedData as $key => $cmoPrepared){
                $amount = getCheckMeOutPay($cmoPrepared['item_amount']);
                $itemNameIndex = "A{$startingSheetIndex}";
                $sheet->setCellValue("A{$startingSheetIndex}", $cmoPrepared['item_name']);
                $sheet->setCellValue("B{$startingSheetIndex}", $amount);
                $sheet->setCellValue("C{$startingSheetIndex}", $cmoPrepared['item_buyer_name']);
                $sheet->setCellValue("D{$startingSheetIndex}", $cmoPrepared['item_buyer_email']);
                $sheet->setCellValue("E{$startingSheetIndex}", $cmoPrepared['item_buyer_mobile_number']);
                $sheet->setCellValue("F{$startingSheetIndex}", $cmoPrepared['item_street_address']);
                $sheet->setCellValue("G{$startingSheetIndex}", $cmoPrepared['item_province']);
                $sheet->setCellValue("H{$startingSheetIndex}", $cmoPrepared['item_city_municipality']);
                $sheet->setCellValue("I{$startingSheetIndex}", $cmoPrepared['item_banragay']);
                $sheet->setCellValue("J{$startingSheetIndex}", $cmoPrepared['item_zipcode']);
                $startingSheetIndex++;
            }

        })->download('xlsx');


        return view('orders.export');
    }

    public function postImport(Request $request)
    {
        include app_path('Libraries/parsecsv.lib.php');
        $file = $request->file('orders');
        $path = $file;
        $csv = new \parseCSV();
        $csv->auto($path);
        $cmpOrderDatas = $csv->data;
//        foreach ($cmpOrderDatas as $key => $cmpOrderData){
//            if(Order::where('item_name',$cmpOrderData['Item'])->where('buyer',$cmpOrderData['Buyer Name'])->where('status','IMPORTED FROM SHOPIFY')->exists()){
//                $order = Order::where('item_name',$cmpOrderData['Item'])->where('buyer',$cmpOrderData['Buyer Name'])->where('status','IMPORTED FROM SHOPIFY')->first();
//                $this->fulfill($cmpOrderData['Tracking #'],$order->shopify_order_id);
//                $order->update(['status' => 'FULFILLED','tracking_number' => $cmpOrderData['Tracking #']]);
//
//
//            }
//        }

        $config = Config::where('user_id',auth()->user()->id)->first();

        $data = [
            'cmoDatas' =>array_chunk($cmpOrderDatas,3),
            'config' =>$config
        ];
        $pdf = app()->make('dompdf.wrapper');
        $pdf->loadView('orders.packing_pdf', $data);

        return $pdf->stream();


    }



    public function printLBC()
    {
        return view('orders.packing_lbc');
    }

    public function printSlip()
    {
        return view('orders.packing_slip');
    }

    public function fulfill($trackingNumber, $orderID)
    {
//        $shopHostName = "trendy-bargain-shop.myshopify.com";
//        $apiKey = "ad347a79b8d1d4f8ec853aa51501cd8d";
//        $password = "b7cb716343648b78d574c543b2ff1162";
//        $locationID = "10392600634";
//        $trackingCompany = "Other";

        $config = Config::where('user_id',auth()->user()->id)->first();
        $shopHostName = $config->shop_hostname;
        $apiKey = $config->api_key;
        $password = $config->password;
        $locationID = $config->location_id;
        $trackingCompany = $config->tracking_company;


        $trackingURL = "https://www.checkmeout.ph/track/{$trackingNumber}";
        $url = "https://{$apiKey}:{$password}@{$shopHostName}/admin/orders/{$orderID}/fulfillments.json";
        $fulfillmentData = [
            'fulfillment' => [
                'location_id' => $locationID,
                'tracking_url' => $trackingURL,
                'tracking_number' => $trackingNumber,
                'tracking_company' => $trackingCompany,
            ],
            'notify_customer' =>  true


        ];
        $curlData = Curl::to($url)
            ->withData($fulfillmentData)
            ->asJson(true)
            ->post();

    }


}
