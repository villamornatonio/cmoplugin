<?php

namespace App\Http\Controllers;

use App\Models\Enrollment;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\TrainingsTransformer;
use yajra\Datatables\Datatables;


/**
 * Class TrainingsController
 * @package App\Http\Controllers
 */
class TrainingsController extends AdminController
{
    /**
     * @var TrainingsTransformer
     */
    protected $trainingsTransformer;

    /**
     * @param TrainingsTransformer $trainingsTransformer
     */
    function __construct(TrainingsTransformer $trainingsTransformer)
    {
        $this->middleware('auth');
        $this->trainingsTransformer = $trainingsTransformer;
        $this->restrictTo(['admin', 'record']);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function completed()
    {
        $data = [];

        return view('trainings.completed', $data);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function unfinished()
    {
        $data = [];

        return view('trainings.unfinished', $data);
    }

    /**
     * @param int $status
     * @param Request $request
     * @return mixed
     */
    public function getAll($status = 1, Request $request)
    {
        $cachedData = Cache::rememberForever('api.training.' . $request->getRequestUri() , function() use($status){
            $trainings = DB::table('enrollments as enrollments')
                ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
                ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
                ->join('users as users', 'enrollments.created_by', '=', 'users.id')
                ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
                ->where('enrollments.is_completed', $status)
                ->where('enrollments.has_fully_paid', 1)
                ->where('schedules.start_date', '<', Carbon::now())
                ->where('enrollments.company_id',session('company.id'))
                ->select([
                    'schedules.id',
                    'trainees.firstname',
                    'trainees.lastname',
                    'courses.name as course',
                    'schedules.start_date',
                    'schedules.end_date',
                    'schedules.start_time',
                    'schedules.end_time',
                    'enrollments.is_completed',
                    'enrollments.is_company_charged',
                    'enrollments.id as enrollment_id',
                    DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
                ]);

            return Datatables::of($trainings)
                ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->filterColumn('trainee', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
                ->setTransformer('Transformers\TrainingsTransformer')
                ->make(true);
        });

        return $cachedData;



    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function releaseEnrollmentCertificate($id)
    {
        $enrollment = Enrollment::select('*')->findOrFail($id);
        $enrollment->is_released = 1;
        if (!$enrollment->save()) {
            return $this->redirectWithError('Failed on Releasing Certificate!.');
        }

        return $this->redirect('Successfuly Released a CERTIFICATE!', 'enrollments');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getToggleCompleted($id)
    {
        $status = 0;
        $enrollment = Enrollment::findOrFail($id);

        if ($enrollment->is_completed == 0) {
            $status = 1;
        }
        $enrollment->is_completed = $status;
        if (!$enrollment->save()) {
            return $this->redirectWithError('Failed on Updating an Enrollment!.');
        }

        return $this->redirect('Successfuly Updated an Enrollments!', 'trainings/completed');
    }

}
