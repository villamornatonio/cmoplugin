<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class PagesController
 * @package App\Http\Controllers
 */
class PagesController extends AdminController
{
    /**
     *
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function home()
    {
        $data = [];

        return view('pages.home.index');
    }

    /**
     *
     */
    public function contactUs()
    {
        $data = [];

        return view('pages.contact_us.index');

    }

    public function aboutUs()
    {
        $data = [];

        return view('pages.about_us.index');

    }

    public function courses()
    {
        $data = [];

        return view('pages.courses.index');

    }

    public function team()
    {
        $data = [];

        return view('pages.team.index');

    }

    public function facilities()
    {
        $data = [];

        return view('pages.facilities.index');

    }

    public function convertCSV()
    {
        $filePath = '/Users/villamornantoniojr/Desktop/TSIKOT/cars.xlsx';
        Excel::load($filePath, function ($reader) {
//            $reader->getActiveSheetIndex(0)->setCellValue('A1', 'Test String');
            $sheet = $reader->setActiveSheetIndex(0);
            $rows = $reader->toArray();
//            dd($rows);
            foreach ($rows as $key => $row) {
                $rowIndex = $key + 2;
                $cModel = explode(' ', $row['model']);
                $cModel = $cModel[0];
                $bodyType = json_decode($row['chassis'], true);
                $bodyType = $bodyType['Body Type'];
                $engineType = json_decode($row['drivetrain'], true);
                $engineType = $engineType['Cylinder Layout'];
                $transmissionType = json_decode($row['drivetrain'], true);
                $transmissionType = $transmissionType['Transmission'];
                $fuelType = json_decode($row['drivetrain'], true);
                $fuelType = $fuelType['Fuel Type'];
                $power = json_decode($row['drivetrain'], true);
                $power = explode('@', $power['Max Power']);
                $powerPS = (isset($power[0])) ? $power[0] : $power;
                $powerRPM = (isset($power[1])) ? $power[1] : '';
                $fuelCapacity = json_decode($row['chassis'], true);
                $fuelCapacity = $fuelCapacity['Fuel Tank Capacity'];
                $wheelBase = json_decode($row['chassis'], true);
                $wheelBase = $wheelBase['Wheelbase'];
                $topSpeed = json_decode($row['performance_amp_efficiency'], true);
                $topSpeed = $topSpeed['Top Speed'];
                $valves = json_decode($row['drivetrain'], true);
                $valves = $valves['Valvetrain'];


                $matchKeys = [
                    'body_type' => 'model_body',
                    'cylinder_layout' => 'model_engine_cyl',
                    'fuel_type' => 'model_engine_fuel',
                    'valvetrain' => 'model_engine_valves_per_cyl',
                    'max_power' => 'model_engine_power_ps',
                    'max_torque' => 'model_engine_torque_nm',
                    'top_speed' => 'model_top_speed_kph',
                    '0-100_km/h' => 'model_0_to_100_kph',
                    'drive_train' => 'model_drive',
                    'transmission' => 'model_transmission_type',
                    'seating' => 'model_seats',
                    'doors' => 'model_doors',
                    'curb_weight' => 'model_weight_kg',
                    'width' => 'model_width_mm',
                    'height' => 'model_height_mm',
                    'length' => 'model_length_mm',
                    'wheelbase' => 'model_wheelbase_mm',
                    'fuel_tank_capacity' => 'model_fuel_cap_l',
                    'consumption_(city)' => 'model_lkm_city',
                    'consumption_(highway)' => 'model_lkm_hwy',
                    'consumption_(mixed)' => 'model_lkm_mixed',
                ];



                $fullSpecification = [
                    'model_make_id' => '',
                    'model_name' => '',
                    'model_trim' => '',
                    'model_year' => '',
                    'model_body' => '',
                    'model_engine_position' => '',
                    'model_engine_cc' => '',
                    'model_engine_cyl' => '',
                    'model_engine_type' => '',
                    'model_engine_valves_per_cyl' => '',
                    'model_engine_power_ps' => '',
                    'model_engine_power_rpm' => '',
                    'model_engine_torque_nm' => '',
                    'model_engine_torque_rpm' => '',
                    'model_engine_bore_mm' => '',
                    'model_engine_stroke_mm' => '',
                    'model_engine_compression' => '',
                    'model_engine_fuel' => '',
                    'model_top_speed_kph' => '',
                    'model_0_to_100_kph' => '',
                    'model_drive' => '',
                    'model_transmission_type' => '',
                    'model_seats' => '',
                    'model_doors' => '',
                    'model_weight_kg' => '',
                    'model_length_mm' => '',
                    'model_width_mm' => '',
                    'model_height_mm' => '',
                    'model_wheelbase_mm' => '',
                    'model_lkm_hwy' => '',
                    'model_lkm_mixed' => '',
                    'model_lkm_city' => '',
                    'model_fuel_cap_l' => '',
                    'model_sold_in_us' => '',
                    'model_co2' => '',
                    'model_make_display' => '',
                ];
                $fullSpecification['model_name'] = $cModel;
                $fullSpecification['model_make_id'] = $cModel;
                $fullSpecification['model_make_display'] = ucfirst($cModel);
                $fullSpecification['model_year'] = (int) $row['year'];
                $fullSpecification['model_trim'] = substr(strstr($row['model'], " "), 1);

                $items = json_decode($row['ownership'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }

                $items = json_decode($row['drivetrain'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['chassis'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['interior'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['convenience'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['entertainment'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['security'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                  $items = json_decode($row['safety'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        $fullSpecification[$index] = $item;
                    }
                }
                       $items = json_decode($row['eco_features'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }
                       $items = json_decode($row['performance_amp_efficiency'], true);
                if(is_array($items)){
                    foreach($items as $key =>$item){
                        $index = str_replace(' ', '_',strtolower($key));
                        if(isset($matchKeys[$index])){
                            $index = $matchKeys[$index];
                        }
                        $fullSpecification[$index] = $item;
                    }
                }

                $fullSpecification = serialize($fullSpecification);

                $trim = substr(strstr($row['model'], " "), 1);
                $sheet->setCellValue('D' . $rowIndex, $fullSpecification);
                $sheet->setCellValue('E' . $rowIndex, $cModel);
                $sheet->setCellValue('F' . $rowIndex, $trim);
                $sheet->setCellValue('G' . $rowIndex, $bodyType);
                $sheet->setCellValue('H' . $rowIndex, $engineType);
                $sheet->setCellValue('I' . $rowIndex, $transmissionType);
                $sheet->setCellValue('J' . $rowIndex, $fuelType);
                $sheet->setCellValue('K' . $rowIndex, $powerPS);
                $sheet->setCellValue('L' . $rowIndex, $powerRPM);
                $sheet->setCellValue('M' . $rowIndex, $fuelCapacity);
                $sheet->setCellValue('O' . $rowIndex, $topSpeed);
                $sheet->setCellValue('P' . $rowIndex, $wheelBase);
                $sheet->setCellValue('Q' . $rowIndex, $valves);


//                dd($cModel);
//
//                dd($row['model']);
            }


        })->store('csv', '/Users/villamornantoniojr/Desktop/TSIKOT/brands');
    }

//    private function parse


}
