<?php

namespace App\Http\Controllers;

use App\Models\Enrollment;
use App\Models\Payment;
use App\Models\Trainee;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use PDO;
use Transformers\TraineesTransformer;

/**
 * Class ReportsController
 * @package App\Http\Controllers
 */
class ReportsController extends AdminController
{

    /**
     *
     */

    protected $traineesTransformer;

    /**
     * @param TraineesTransformer $traineesTransformer
     */
    function __construct(TraineesTransformer $traineesTransformer)
    {
        $this->middleware('auth');
        $this->traineesTransformer = $traineesTransformer;
        $this->restrictTo(['admin','finance']);
    }

    /**
     * @param string $range
     * @return \Illuminate\View\View
     * @internal param string $date
     */
    public function daily($range = '')
    {
        $date = Carbon::now()->format('Y-m-d');
        $week = Carbon::now()->subWeek()->weekOfYear;
        $month = Carbon::now()->format('m');
        if(!empty($range)){
            $date = Carbon::parse($range)->format('Y-m-d');
            $week = Carbon::parse($range)->subWeek()->weekOfYear;
            $month = Carbon::parse($range)->format('m');
        }

        $recentlyJoined = Trainee::orderBy('created_at', 'desc')->get()->take(5);
        $data['recentlyJoined'] = $this->traineesTransformer->transformCollection($recentlyJoined->all());

        $data['mostCompletedCourses'] = $this->getMostCompletedCourses();

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $data['enrollments'] = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where(DB::raw('DATE(enrollments.created_at)'), $date)
            ->orderBy('schedules.created_at', 'asc')
            ->groupBy('courses.code')
            ->select([
                'courses.code as code',
                DB::raw('count(*) as enrollees, trainees.id')
            ])
            ->get();


        DB::setFetchMode(PDO::FETCH_CLASS);


        $data['newTrainees'] = Trainee::where(DB::raw('DATE(created_at)'),
            $date)->count();
        $data['newEnrollments'] = Enrollment::where(DB::raw('DATE(created_at)'),
            $date)->count();
        $data['graduates'] = Enrollment::where('date_completed',
            $date)->where('is_completed', 1)->count();
        $data['dailyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('DATE(created_at)'), $date)->sum('amount'));
        $data['weeklyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('WEEK(created_at)'), $week)->sum('amount'));
        $data['monthlyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('MONTH(created_at)'), $month)->sum('amount'));
        $data['monthlyTotalEnrollees'] = Enrollment::where(DB::raw('MONTH(created_at)'),
            $month)->count();

        $data['dates'] = [];
        $data['date'] = $date;


        return view('reports.daily', $data);

    }

    /**
     * @param string $range
     * @return \Illuminate\View\View
     * @internal param string $week
     */
    public function weekly($range = '')
    {
        $date = Carbon::now()->format('Y-m-d');
        $week = Carbon::now()->subWeek()->weekOfYear;
        $month = Carbon::now()->format('m');
        if(!empty($range)){
            $date = Carbon::parse($range)->format('Y-m-d');
            $week = Carbon::parse($range)->subWeek()->weekOfYear;
            $month = Carbon::parse($range)->format('m');
        }
        $recentlyJoined = Trainee::orderBy('created_at', 'desc')->get()->take(5);
        $data['recentlyJoined'] = $this->traineesTransformer->transformCollection($recentlyJoined->all());

        $data['mostCompletedCourses'] = $this->getMostCompletedCourses();

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $data['enrollments'] = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where(DB::raw('WEEK(enrollments.created_at)'), $week)
            ->orderBy('schedules.created_at', 'asc')
            ->groupBy('courses.code')
            ->select([
                'courses.code as code',
                DB::raw('count(*) as enrollees, trainees.id')
            ])
            ->get();


        DB::setFetchMode(PDO::FETCH_CLASS);


        $data['newTrainees'] = Trainee::where(DB::raw('WEEK(created_at)'),
            $week)->count();
        $data['newEnrollments'] = Enrollment::where(DB::raw('WEEK(created_at)'),
            $week)->count();
        $data['graduates'] = Enrollment::where('date_completed',
            $date)->where('is_completed', 1)->count();
        $data['dailyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('DATE(created_at)'), $date)->sum('amount'));
        $data['weeklyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('WEEK(created_at)'), $week)->sum('amount'));
        $data['monthlyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('MONTH(created_at)'), $month)->sum('amount'));
        $data['monthlyTotalEnrollees'] = Enrollment::where(DB::raw('MONTH(created_at)'),
            $month)->count();

        $data['weeks'] = getListWeeks();
        $data['week'] = $date;

        return view('reports.weekly', $data);
    }

    /**
     * @param string $range
     * @return \Illuminate\View\View
     * @internal param string $month
     */
    public function monthly($range = '')
    {
        $date = Carbon::now()->format('Y-m-d');
        $week = Carbon::now()->subWeek()->weekOfYear;
        $month = Carbon::now()->format('m');
        if(!empty($range)){
            $date = Carbon::parse($range)->format('Y-m-d');
            $week = Carbon::parse($range)->subWeek()->weekOfYear;
            $month = Carbon::parse($range)->format('m');
        }
        $recentlyJoined = Trainee::orderBy('created_at', 'desc')->get()->take(5);
        $data['recentlyJoined'] = $this->traineesTransformer->transformCollection($recentlyJoined->all());

        $data['mostCompletedCourses'] = $this->getMostCompletedCourses();

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $data['enrollments'] = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where(DB::raw('MONTH(enrollments.created_at)'), $month)
            ->orderBy('schedules.created_at', 'asc')
            ->groupBy('courses.code')
            ->select([
                'courses.code as code',
                DB::raw('count(*) as enrollees, trainees.id')
            ])
            ->get();


        DB::setFetchMode(PDO::FETCH_CLASS);


        $data['newTrainees'] = Trainee::where(DB::raw('MONTH(created_at)'),
            $month)->count();
        $data['newEnrollments'] = Enrollment::where(DB::raw('MONTH(created_at)'),
            $month)->count();
        $data['graduates'] = Enrollment::where('date_completed',
            $date)->where('is_completed', 1)->count();
        $data['dailyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('DATE(created_at)'), $date)->sum('amount'));
        $data['weeklyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('WEEK(created_at)'), $week)->sum('amount'));
        $data['monthlyEarning'] = 'PHP ' . number_format(Payment::where(DB::raw('MONTH(created_at)'), $month)->sum('amount'));
        $data['monthlyTotalEnrollees'] = Enrollment::where(DB::raw('MONTH(created_at)'),
            $month)->count();

        $data['months'] = getListMonths();
        $data['month'] = $date;

        return view('reports.monthly', $data);

    }

    /**
     * @return mixed
     */
    private function getMostCompletedCourses()
    {
        DB::setFetchMode(PDO::FETCH_ASSOC);

        return DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where('enrollments.is_completed', 1)
            ->orderBy('courseCompleted', 'desc')
            ->groupBy('trainee')
            ->take(5)
            ->select([
                'enrollments.id',
                'courses.name as course',
                'courses.price as price',
                'rooms.name as room',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'trainees.id as trainee_id',
                'trainees.avatar as avatar',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee'),
                DB::raw('count(*) as courseCompleted, courses.name')
            ])->get();
        DB::setFetchMode(PDO::FETCH_CLASS);


    }


    public function getWeeklyData($date)
    {
        $data['earnings'] = [
            [ 'y' =>  '10-2015', 'earnings' => 30],
            [ 'y' =>  '09-2015', 'earnings' => 35],
            [ 'y' =>  '08-2015', 'earnings' => 60],
            [ 'y' =>  '07-2015', 'earnings' => 70],
            [ 'y' =>  '06-2015', 'earnings' => 35],
            [ 'y' =>  '05-2015', 'earnings' => 35],
            [ 'y' =>  '04-2015', 'earnings' => 70],
            [ 'y' =>  '03-2015', 'earnings' => 35],
            [ 'y' =>  '02-2015', 'earnings' => 23],
            [ 'y' =>  '01-2015', 'earnings' => 35],
            [ 'y' =>  '12-2014', 'earnings' => 6],
            [ 'y' =>  '11-2014', 'earnings' => 35],

        ];
        $data['trainees'] = [
            [0 ,50],
            [0.5 ,  65],
            [1 , 55],
            [1.5 ,  62],
            [2 ,  55],
            [2.5 ,  58],
            [3 ,  65],
            [3.5 ,  58],
            [4 ,  63],
            [4.5 ,  65],
            [5 ,  83],
            [5.5 ,  78],
            [6 ,  80]
        ];
        $data['enrollments'] = [
            [0, 20],
            [0.5, 35],
            [1, 20],
            [1.5, 25],
            [2, 17],
            [2.5, 10],
            [3, 15],
            [3.5, 28],
            [4, 15],
            [4.5, 20],
            [5, 35],
            [5.5, 30],
            [6, 32]
        ];

        $data['graduates'] = [
            [0, 10],
            [0.5, 25],
            [1, 10],
            [1.5, 15],
            [2, 7],
            [2.5, 5],
            [3, 20],
            [3.5, 18],
            [4, 35],
            [4.5, 50],
            [5, 40],
            [5.5, 30],
            [6, 8]
        ];


        return $this->respond('Data Found', $data);
    }
    public function getMonthlyData($date)
    {
        $data['earnings'] = [
            [ 'y' =>  '10-2015', 'earnings' => 30],
            [ 'y' =>  '09-2015', 'earnings' => 35],
            [ 'y' =>  '08-2015', 'earnings' => 60],
            [ 'y' =>  '07-2015', 'earnings' => 70],
            [ 'y' =>  '06-2015', 'earnings' => 35],
            [ 'y' =>  '05-2015', 'earnings' => 35],
            [ 'y' =>  '04-2015', 'earnings' => 70],
            [ 'y' =>  '03-2015', 'earnings' => 35],
            [ 'y' =>  '02-2015', 'earnings' => 23],
            [ 'y' =>  '01-2015', 'earnings' => 35],
            [ 'y' =>  '12-2014', 'earnings' => 6],
            [ 'y' =>  '11-2014', 'earnings' => 35],

        ];
        $data['trainees'] = [
            [0 ,50],
            [0.5 ,  65],
            [1 , 55],
            [1.5 ,  62],
            [2 ,  55],
            [2.5 ,  58],
            [3 ,  65],
            [3.5 ,  58],
            [4 ,  63],
            [4.5 ,  65],
            [5 ,  83],
            [5.5 ,  78],
            [6 ,  80]
        ];
        $data['enrollments'] = [
            [0, 20],
            [0.5, 35],
            [1, 20],
            [1.5, 25],
            [2, 17],
            [2.5, 10],
            [3, 15],
            [3.5, 28],
            [4, 15],
            [4.5, 20],
            [5, 35],
            [5.5, 30],
            [6, 32]
        ];

        $data['graduates'] = [
            [0, 10],
            [0.5, 25],
            [1, 10],
            [1.5, 15],
            [2, 7],
            [2.5, 5],
            [3, 20],
            [3.5, 18],
            [4, 35],
            [4.5, 50],
            [5, 40],
            [5.5, 30],
            [6, 8]
        ];


        return $this->respond('Data Found', $data);
    }

    public function getDailyData($date)
    {
        $data['earnings'] = [
            [ 'y' =>  '10-2015', 'earnings' => 30],
            [ 'y' =>  '09-2015', 'earnings' => 35],
            [ 'y' =>  '08-2015', 'earnings' => 60],
            [ 'y' =>  '07-2015', 'earnings' => 70],
            [ 'y' =>  '06-2015', 'earnings' => 35],
            [ 'y' =>  '05-2015', 'earnings' => 35],
            [ 'y' =>  '04-2015', 'earnings' => 70],
            [ 'y' =>  '03-2015', 'earnings' => 35],
            [ 'y' =>  '02-2015', 'earnings' => 23],
            [ 'y' =>  '01-2015', 'earnings' => 35],
            [ 'y' =>  '12-2014', 'earnings' => 6],
            [ 'y' =>  '11-2014', 'earnings' => 35],

        ];
        $data['trainees'] = [
            [0 ,50],
            [0.5 ,  65],
            [1 , 55],
            [1.5 ,  62],
            [2 ,  55],
            [2.5 ,  58],
            [3 ,  65],
            [3.5 ,  58],
            [4 ,  63],
            [4.5 ,  65],
            [5 ,  83],
            [5.5 ,  78],
            [6 ,  80]
        ];
        $data['enrollments'] = [
            [0, 20],
            [0.5, 35],
            [1, 20],
            [1.5, 25],
            [2, 17],
            [2.5, 10],
            [3, 15],
            [3.5, 28],
            [4, 15],
            [4.5, 20],
            [5, 35],
            [5.5, 30],
            [6, 32]
        ];

        $data['graduates'] = [
            [0, 10],
            [0.5, 25],
            [1, 10],
            [1.5, 15],
            [2, 7],
            [2.5, 5],
            [3, 20],
            [3.5, 18],
            [4, 35],
            [4.5, 50],
            [5, 40],
            [5.5, 30],
            [6, 8]
        ];


        return $this->respond('Data Found', $data);
    }



}
