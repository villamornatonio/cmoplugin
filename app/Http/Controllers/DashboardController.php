<?php

namespace App\Http\Controllers;

use App\Http\Requests;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends AdminController
{
    /**
     *
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->restrictTo(['admin', 'finance']);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index()
    {
        $user = auth()->user();
        switch ($user->role->name) {
            case "admin":
                return $this->redirect('Welcome ' . $user->name(), 'reports/daily');
            case "finance":
                return $this->redirect('Welcome', 'payments');
            case "registrar":
                return $this->redirect('Welcome', 'trainees');
            case "cashier":
                return $this->redirect('Welcome', 'payments/pending');
            case "record":
                return $this->redirect('Welcome', 'trainings/completed');
            case "marketing":
                return $this->redirect('Welcome', 'enrollments');
            default:

        }
    }

}
