<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePartialPaymentRequest;
use App\Http\Requests\StorePaymentRequest;
use App\Models\Enrollment;
use App\Models\Payment;
use App\Models\Receipt;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use PDO;
use Transformers\EnrollmentsTransformer;
use yajra\Datatables\Datatables;

/**
 * Class PaymentsController
 * @package App\Http\Controllers
 */
class PaymentsController extends AdminController
{
    /**
     * @var EnrollmentsTransformer
     */
    protected $enrollmentTransformer;

    /**
     * @param EnrollmentsTransformer $enrollmentsTransformer
     */
    function __construct(EnrollmentsTransformer $enrollmentsTransformer)
    {
        $this->middleware('auth');
        $this->enrollmentTransformer = $enrollmentsTransformer;
        $this->restrictTo(['admin', 'finance', 'cashier']);
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        return view('payments.index');
    }

    /**
     * @param $trainee_id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create($trainee_id)
    {
        $totalAmount = 0;
        $data['trainee_id'] = $trainee_id;


        DB::setFetchMode(PDO::FETCH_ASSOC);
        $enrollments = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where('enrollments.trainee_id', $trainee_id)
            ->where('enrollments.has_partially_paid', 0)
            ->where('enrollments.has_fully_paid', 0)
            ->select([
                'trainees.id as id',
                'courses.name as course',
                'rooms.name as room',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'courses.price as price',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ])->get();
        DB::setFetchMode(PDO::FETCH_CLASS);
        foreach ($enrollments as $key => $enrollment) {
            $totalAmount = $totalAmount + (int)$enrollment['price'];
        }
        $data['totalAmount'] = 'PHP ' . number_format($totalAmount);

        $data['enrollments'] = $this->enrollmentTransformer->transformCollection($enrollments);

        return view('payments.create', $data);
    }


    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getPartial($id)
    {
        $totalAmount = 0;

        $payment = Payment::findOrFail($id);
        if (empty($payment->paid_enrollment_ids)) {
            return $this->redirectWithError('Resourse Not Found!');
        }
        $enrollment_id = array_flatten(json_decode($payment->paid_enrollment_ids));
        $data['trainee_id'] = $payment->trainee_id;
        $data['payment_id'] = $payment->id;


        DB::setFetchMode(PDO::FETCH_ASSOC);
        $enrollments = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->whereIn('enrollments.id', $enrollment_id)
            ->where('enrollments.has_partially_paid', 1)
            ->where('enrollments.has_fully_paid', 0)
            ->select([
                'trainees.id as id',
                'courses.name as course',
                'rooms.name as room',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'courses.price as price',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ])->get();
        DB::setFetchMode(PDO::FETCH_CLASS);
        foreach ($enrollments as $key => $enrollment) {
            $totalAmount = $totalAmount + (int)$enrollment['price'];
        }
        $data['totalAmount'] = 'PHP ' . number_format($totalAmount);
        $data['amountDue'] = 'PHP ' . number_format($payment->remaining_balance);
        $data['remainingBalance'] = 'PHP ' . number_format($payment->remaining_balance);

        $data['enrollments'] = $this->enrollmentTransformer->transformCollection($enrollments);


        return view('payments.create_partial', $data);
    }

    /**
     * @param StorePartialPaymentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postPartial(StorePartialPaymentRequest $request)
    {
        DB::transaction(function () use ($request) {
            try {
                $parameters = $request->only('trainee_id', 'amount', 'discount', 'or_no', 'payment_id');
                $payment = Payment::findOrFail($parameters['payment_id']);
                $enrollment_ids = array_flatten(json_decode($payment->paid_enrollment_ids));


                $remainingBalance = (int)$payment->remaining_balance - (int)$request->amount;
                $parameters['remaining_balance'] = $remainingBalance;
                $parameters['price'] = $payment->price;
                $parameters['paid_enrollment_ids'] = $payment->paid_enrollment_ids;
                $parameters['created_by'] = auth()->user()->id;
                $parameters['company_id'] = session('company.id');

                if (!Payment::create($parameters)) {
                    return $this->redirectWithError('Failed on Processing the PAYMENT!.');
                }

                if ($remainingBalance <= 0) {
                    Enrollment::whereIn('id', $enrollment_ids)->update(['has_fully_paid' => 1]);
                } else {
                    Enrollment::whereIn('id', $enrollment_ids)->update(['has_partially_paid' => 1]);

                }


                DB::setFetchMode(PDO::FETCH_ASSOC);
                $enrollments = DB::table('enrollments as enrollments')
                    ->join('users as users', 'enrollments.created_by', '=', 'users.id')
                    ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
                    ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
                    ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
                    ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
                    ->whereIn('enrollments.id', $enrollment_ids)
                    ->select([
                        'trainees.id as id',
                        'enrollments.id as enrollement_id',
                        'courses.name as course',
                        'courses.id as course_id',
                        'courses.code as course_code',
                        'rooms.name as room',
                        'rooms.id as room_id',
                        'schedules.start_date as start_date',
                        'schedules.end_date as end_date',
                        'schedules.start_time as start_time',
                        'schedules.end_time as end_time',
                        'enrollments.is_completed',
                        'enrollments.is_company_charged',
                        'courses.price as price',
                        DB::raw('concat (users.firstname," ",users.lastname) as author'),
                        DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
                    ])->get();
                DB::setFetchMode(PDO::FETCH_CLASS);


                foreach ($enrollments as $key => $enrollment) {

                    $receiptParameters = [
                        'trainee_id' => $parameters['trainee_id'],
                        'room_id' => $enrollment['room_id'],
                        'room_name' => $enrollment['room'],
                        'course_id' => $enrollment['course_id'],
                        'course_name' => $enrollment['course'],
                        'price' => $enrollment['price'],
                        'course_code' => $enrollment['course_code'],
                        'course_date' => Carbon::parse($enrollment['start_date'])->toFormattedDateString() . ' - ' . Carbon::parse($enrollment['end_date'])->toFormattedDateString(),
                        'course_time' => Carbon::parse($enrollment['start_time'])->format('h:i A') . ' - ' . Carbon::parse($enrollment['end_time'])->format('h:i A'),
                        'discount' => $parameters['discount'],
                        'or_no' => $parameters['or_no'],
                        'amount_paid' => $parameters['amount'],
                        'total_amount' => $payment->remaining_balance,
                        'remaining_balance' => $remainingBalance,
                        'created_by' => auth()->user()->id,
                        'company_id' => session('company.id')
                    ];


                    Receipt::create($receiptParameters);
                }

            } catch (\Exception $e) {
                return $this->redirectWithError('Failed on Saving Payment!');
            }


        });

        return $this->redirect('Successfuly processed the PAYMENT!', 'payments');
    }


    /**
     * @param StorePaymentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StorePaymentRequest $request)
    {
        DB::transaction(function () use ($request) {
            $totalAmount = 0;
            $parameters = $request->only('trainee_id', 'amount', 'discount', 'or_no');
            $enrollment_ids = [];

            try {

                DB::setFetchMode(PDO::FETCH_ASSOC);
                $enrollments = DB::table('enrollments as enrollments')
                    ->join('users as users', 'enrollments.created_by', '=', 'users.id')
                    ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
                    ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
                    ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
                    ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
                    ->where('enrollments.trainee_id', $request->trainee_id)
                    ->where('enrollments.has_partially_paid', 0)
                    ->where('enrollments.has_fully_paid', 0)
                    ->select([
                        'trainees.id as id',
                        'enrollments.id as enrollement_id',
                        'courses.name as course',
                        'courses.id as course_id',
                        'courses.code as course_code',
                        'rooms.name as room',
                        'rooms.id as room_id',
                        'schedules.start_date as start_date',
                        'schedules.end_date as end_date',
                        'schedules.start_time as start_time',
                        'schedules.end_time as end_time',
                        'enrollments.is_completed',
                        'enrollments.is_company_charged',
                        'courses.price as price',
                        DB::raw('concat (users.firstname," ",users.lastname) as author'),
                        DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
                    ])->get();
                DB::setFetchMode(PDO::FETCH_CLASS);
                foreach ($enrollments as $key => $enrollment) {
                    $totalAmount = $totalAmount + (int)$enrollment['price'];
                    $enrollment_ids[] = $enrollment['enrollement_id'];
                }

                $remainingBalance = $totalAmount - (int)$request->amount;
                $parameters['remaining_balance'] = $remainingBalance;
                $parameters['price'] = $totalAmount;
                $parameters['paid_enrollment_ids'] = json_encode($enrollment_ids);
                $parameters['created_by'] = auth()->user()->id;
                $parameters['company_id'] = session('company.id');


                if (!Payment::create($parameters)) {
                    return $this->redirectWithError('Failed on Processing the PAYMENT!.');
                }

                if ($remainingBalance <= 0) {
                    Enrollment::whereIn('id', $enrollment_ids)->update(['has_fully_paid' => 1]);
                } else {
                    Enrollment::whereIn('id', $enrollment_ids)->update(['has_partially_paid' => 1]);

                }

                foreach ($enrollments as $key => $enrollment) {

                    $receiptParameters = [
                        'trainee_id' => $parameters['trainee_id'],
                        'room_id' => $enrollment['room_id'],
                        'room_name' => $enrollment['room'],
                        'course_id' => $enrollment['course_id'],
                        'course_name' => $enrollment['course'],
                        'price' => $enrollment['price'],
                        'course_code' => $enrollment['course_code'],
                        'course_date' => Carbon::parse($enrollment['start_date'])->toFormattedDateString() . ' - ' . Carbon::parse($enrollment['end_date'])->toFormattedDateString(),
                        'course_time' => Carbon::parse($enrollment['start_time'])->format('h:i A') . ' - ' . Carbon::parse($enrollment['end_time'])->format('h:i A'),
                        'discount' => $parameters['discount'],
                        'or_no' => $parameters['or_no'],
                        'amount_paid' => $parameters['amount'],
                        'total_amount' => $totalAmount,
                        'remaining_balance' => $remainingBalance,
                        'created_by' => auth()->user()->id,
                        'company_id' => session('company.id')
                    ];


                    Receipt::create($receiptParameters);
                }
            } catch (\Exception $e) {
                return $this->redirectWithError('Failed on Saving Payment!');
            }


        });


        return $this->redirect('Successfuly processed the PAYMENT!', 'payments');
    }


    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $payment = Payment::findOrFail();
        $payment->delete();

        return $this->respond('Successfuly Deleted a Payment!');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function pending()
    {
        $data = [];

        return view('payments.pending', $data);
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function partial()
    {
        $data = [];

        return view('payments.partial', $data);
    }

    /**
     * @return mixed
     */
    public function getAllPendingEnrollment()
    {
        $enrollments = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where('enrollments.has_partially_paid', 0)
            ->where('enrollments.has_fully_paid', 0)
            ->select([
                'trainees.id as id',
                'courses.name as course',
                'rooms.name as room',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'courses.price as price',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ]);

        return Datatables::of($enrollments)
            ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
            ->filterColumn('trainee', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
            ->setTransformer('Transformers\EnrollmentsTransformer')
            ->make(true);

    }

    /**
     * @return mixed
     */
    public function getAllPartialPayments()
    {
        $payments = DB::table('payments as payments')
            ->join('users as users', 'payments.created_by', '=', 'users.id')
            ->join('trainees as trainees', 'payments.trainee_id', '=', 'trainees.id')
            ->where('payments.remaining_balance', '!=', 0)
            ->where('payments.company_id',session('company.id'))
            ->select([
                'payments.id',
                'payments.amount',
                'payments.remaining_balance',
                'payments.price',
                'payments.discount',
                'payments.or_no',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ]);

        return Datatables::of($payments)
            ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
            ->filterColumn('trainee', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
            ->setTransformer('Transformers\PaymentsTransformer')
            ->make(true);

    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        $payments = DB::table('payments as payments')
            ->join('users as users', 'payments.created_by', '=', 'users.id')
            ->join('trainees as trainees', 'payments.trainee_id', '=', 'trainees.id')
            ->where('payments.company_id',session('company.id'))
            ->select([
                'payments.id',
                'payments.amount',
                'payments.remaining_balance',
                'payments.price',
                'payments.discount',
                'payments.or_no',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ]);

        return Datatables::of($payments)
            ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
            ->filterColumn('trainee', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
            ->setTransformer('Transformers\PaymentsTransformer')
            ->make(true);


    }


}
