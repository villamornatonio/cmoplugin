<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


/**
 * Class AdminController
 * @package App\Http\Controllers
 */
class AdminController extends Controller
{

    /**
     * @var int
     */
    protected $statusCode = 200;
    /**
     * @var int
     */
    protected $paginationLimit = 10;


    /**
     *
     */
    function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;

    }


    /**
     * @param $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }


    /**
     * @return int
     */
    public function getPaginationLimit()
    {
        return $this->paginationLimit;
    }


    /**
     * @param $paginationLimit
     */
    public function setPaginationLimit($paginationLimit)
    {
        $this->paginationLimit = $paginationLimit;
    }


    /**
     * @param string $message
     * @param string $uri
     * @param string $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirect($message = '', $uri = '/', $status = 'success')
    {
        return redirect()->to($uri)->with(['notification' => ['message' => $message, 'status' => $status]]);
    }


    /**
     * @param string $message
     * @param string $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectWithError($message = '', $status = 'danger')
    {
        return redirect()->back()->with(['notification' => ['message' => $message, 'status' => $status]]);
    }


    /**
     * @param string $message
     * @param string $status
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectPermissionDenied($message = 'Permission Denied!', $status = 'danger')
    {
        return redirect()->back()->with(['notification' => ['message' => $message, 'status' => $status]]);
    }


    /**
     * @param $role
     */
    public function restrictOnly($role)
    {
        if (auth()->guest() || auth()->user()->role->name != $role) {
            $this->redirectPermissionDenied();
        }
    }

    /**
     * @param array $roles
     */
    public function restrictTo($roles = [])
    {
        if (auth()->guest() || !in_array(auth()->user()->role->name, $roles)) {
            $this->redirectPermissionDenied();
        }
    }

    /**
     * @param string $message
     * @param array $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respond($message = '', $data = [])
    {

        return response()->json(['message' => $message, 'data' => $data, 'status' => $this->getStatusCode()],
            $this->getStatusCode());
    }


    public function responseWithError($message = '')
    {
        $this->statusCode(400);
        return $this->respond($message);

    }

    /**
     * @param Request $request
     * @param string $path
     * @return string
     */
    public function getAvatarUploaded($request, $path = '' )
    {
        $avatar = 'user-default.png';
        $uploadPath = config()->get('uploads.user');
        if(!empty($path)){
            $uploadPath = $path;
        }
        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {

            $filename = sha1(Carbon::now()->timestamp . $request->file('avatar')->getClientOriginalName()) . '.' . $request->file('avatar')->getClientOriginalExtension();
            $request->file('avatar')->move($uploadPath, $filename);
            $imageWithFullPath = $uploadPath . '/' . $filename;
            $img = Image::make($imageWithFullPath)->resize(160, 160);
            $img->save($imageWithFullPath);
            $saving = Storage::disk('dropbox')->put($uploadPath . $filename,
                file_get_contents($imageWithFullPath));
            if($saving){
                Storage::delete($imageWithFullPath);
            }
            $avatar = $filename;
        }
        return $avatar;

    }


}
