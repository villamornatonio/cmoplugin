<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreEnrollmentRequest;
use App\Models\Enrollment;
use App\Models\Referrer;
use App\Models\Trainee;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\EnrollmentsTransformer;
use Transformers\SchedulesTransformer;
use Transformers\TraineesTransformer;
use yajra\Datatables\Datatables;

/**
 * Class EnrollmentsController
 * @package App\Http\Controllers
 */
class EnrollmentsController extends AdminController
{
    /**
     * @var EnrollmentsTransformer
     */
    protected $enrollmentsTransformer;
    /**
     * @var TraineesTransformer
     */
    protected $traineesTransformer;
    /**
     * @var SchedulesTransformer
     */
    protected $schedulesTransformer;

    /**
     * @param EnrollmentsTransformer $enrollmentsTransformer
     * @param TraineesTransformer $traineesTransformer
     * @param SchedulesTransformer $schedulesTransformer
     */
    function __construct(
        EnrollmentsTransformer $enrollmentsTransformer,
        TraineesTransformer $traineesTransformer,
        SchedulesTransformer $schedulesTransformer
    ) {
        $this->middleware('auth');
        $this->enrollmentsTransformer = $enrollmentsTransformer;
        $this->traineesTransformer = $traineesTransformer;
        $this->schedulesTransformer = $schedulesTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];

        return view('enrollments.index', $data);
    }

    /**
     * @param $scheduleId
     * @param $traineeId
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create($scheduleId, $traineeId)
    {

        $data['trainee'] = $this->traineesTransformer->transform(Trainee::findOrFail($traineeId));
        $data['referrers'] = Referrer::select('id',
            DB::raw('CONCAT(firstname, " ", lastname) AS name'))->lists('name', 'id');

        $data['schedule'] = DB::table('schedules as schedules')
            ->join('users as users', 'schedules.created_by', '=', 'users.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->where('schedules.id', $scheduleId)
            ->select([
                'schedules.id as id',
                'schedules.start_date',
                'schedules.end_date',
                'schedules.start_time',
                'schedules.end_time',
                DB::raw('rooms.name as room'),
                DB::raw('courses.name as course'),
                DB::raw('courses.price as price')
            ])->first();

        return view('enrollments.create', $data);
    }

    /**
     * @param StoreEnrollmentRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreEnrollmentRequest $request)
    {
        $enrollment = Enrollment::where('trainee_id', $request->trainee_id)->where('schedule_id',
            $request->schedule_id)->first();
        if (!is_null($enrollment)) {
            return $this->redirectWithError('You are already an enrolled to this course!.');
        }
        $parameters = $request->only('is_company_charged', 'company', 'schedule_id', 'trainee_id', 'referrer_id');
        $parameters['is_company_charged'] = !is_null($parameters['is_company_charged']) ? 1 : 0;
        $parameters['created_by'] = auth()->user()->id;
        $parameters['company_id'] = session('company.id');


        if (!Enrollment::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New Enrollment!.');
        }

        return $this->redirect('Successfuly Added a New Enrollment!', 'enrollments');

    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $enrollment = Enrollment::findOrFail($id);
        $enrollment->delete();

        return $this->redirect('Successfuly Deleted an Enrollment!', 'enrollments');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.enrollments.' . $request->getRequestUri(), function () {
            $enrollments = DB::table('enrollments as enrollments')
                ->join('users as users', 'enrollments.created_by', '=', 'users.id')
                ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
                ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
                ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
                ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
                ->orderBy('schedules.created_at', 'asc')
                ->select([
                    'enrollments.id',
                    'courses.name as course',
                    'courses.price as price',
                    'rooms.name as room',
                    'schedules.start_date as start_date',
                    'schedules.end_date as end_date',
                    'schedules.start_time as start_time',
                    'schedules.end_time as end_time',
                    'enrollments.is_completed',
                    'enrollments.is_company_charged',
                    DB::raw('concat (users.firstname," ",users.lastname) as author'),
                    DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
                ]);

            return Datatables::of($enrollments)
                ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->filterColumn('trainee', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
                ->setTransformer('Transformers\EnrollmentsTransformer')
                ->make(true);
        });

        return $cachedData;

    }


}
