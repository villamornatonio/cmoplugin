<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Models\Room;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\RoomsTransformer;
use yajra\Datatables\Datatables;


/**
 * Class RoomsController
 * @package App\Http\Controllers
 */
class RoomsController extends AdminController
{
    /**
     * @var RoomsTransformer
     */
    protected $roomsTransformer;

    /**
     * @param RoomsTransformer $roomsTransformer
     */
    function __construct(RoomsTransformer $roomsTransformer)
    {
        $this->middleware('auth');
        $this->roomsTransformer = $roomsTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];

        return view('rooms.index', $data);
    }


    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create()
    {
        $data = [];
        return view('rooms.create', $data);
    }

    /**
     * @param StoreRoomRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRoomRequest $request)
    {
        $parameters = $request->only('name');
        $parameters['created_by'] = auth()->user()->id;
        $parameters['company_id'] = session('company.id');

        if (!Room::create($parameters)) {
            return $this->redirectWithError(trans('message.rooms.create.fail'));
        }

        return $this->redirect(trans('message.rooms.create.success'), 'rooms');

    }


    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function show($id)
    {
        $data = Room::select('*')->findOrFail($id);
        return $this->respond('Data Found',$this->roomsTransformer->transform($data));
    }


    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['room'] = $this->roomsTransformer->transform(Room::select('*')->findOrFail($id));

        return view('rooms.edit', $data);
    }

    /**
     * @param UpdateRoomRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRoomRequest $request, $id)
    {
        $parameters = $request->only('name');
        $room = Room::select('*')->findOrFail($id);
        $room->name = $parameters['name'];
        if (!$room->save()) {
            return $this->redirectWithError(trans('message.rooms.update.fail'));
        }

        return $this->redirect(trans('message.rooms.update.success'), 'rooms');
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();

        return $this->redirect(trans('message.rooms.delete.success'), 'rooms');
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.rooms.' . $request->getRequestUri(), function () {
            $rooms = DB::table('rooms as rooms')
                ->join('users as users', 'rooms.created_by', '=', 'users.id')
                ->where('company_id',session('company.id'))
                ->select([
                    'rooms.id',
                    'rooms.name',
                    DB::raw('concat (users.firstname," ",users.lastname) as author')
                ]);

            return Datatables::of($rooms)
                ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->make(true);

        });

        return $cachedData;
    }
}
