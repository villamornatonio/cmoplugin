<?php

namespace App\Http\Controllers;

use App\Models\Receipt;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests;
use Transformers\ReceiptsTransformer;

/**
 * Class ReceiptsController
 * @package App\Http\Controllers
 */
class ReceiptsController extends AdminController
{

    /**
     * @var ReceiptsTransformer
     */
    protected $receiptsTransformer;

    /**
     * @param ReceiptsTransformer $receiptsTransformer
     */
    function __construct(ReceiptsTransformer $receiptsTransformer)
    {
//        $this->middleware('auth');
        $this->receiptsTransformer = $receiptsTransformer;
//        $this->restrictTo(['admin', 'finance', 'cashier']);
    }

    /**
     * @param $orNo
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function fullPayment($orNo)
    {
        $receipts = Receipt::where('or_no', $orNo)->get();
        if (empty($receipts->all())) {
            throw new ModelNotFoundException;
        }
        $data['receipts'] = $this->receiptsTransformer->transformCollection($receipts->all());
        return view('receipts.full_payment', $data);
    }

}
