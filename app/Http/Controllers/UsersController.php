<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangeUserPasswordRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;
use App\User;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use yajra\Datatables\Datatables;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends AdminController
{
    /**
     *
     */
    function __construct()
    {
        $this->middleware('auth');
        $this->restrictOnly('admin');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        return view('users.index');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    function create()
    {
        $data['roles'] = Role::where('name', '!=', 'admin')->lists('name', 'id');

        return view('users.create', $data);
    }

    /**
     * @param StoreUserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreUserRequest $request)
    {

        $parameters = $request->only('firstname', 'lastname', 'role_id', 'email', 'contact_no',
            'password');
        $parameters['password'] = Hash::make($request->password);
        $parameters['avatar'] = $this->getAvatarUploaded($request);
        $parameters['is_activated'] = 1;
        $parameters['company_id'] = session('company.id');
        if (!User::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New User!.');
        }

        return $this->redirect('Successfuly Added a New User!', 'users');

    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['roles'] = Role::where('name', '!=', 'admin')->lists('name', 'id');
        $data['user'] = User::select('*')->findOrFail($id);

        return view('users.edit', $data);
    }

    /**
     * @param UpdateUserRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateUserRequest $request, $id)
    {


        $user = User::select('*')->findOrFail($id);
        $parameters = $request->only('firstname', 'lastname', 'role_id', 'email', 'contact_no');
        $user->firstname = $parameters['firstname'];
        $user->lastname = $parameters['lastname'];
        $user->role_id = $parameters['role_id'];
        $user->email = $parameters['email'];
        $user->contact_no = $parameters['contact_no'];
        $user->avatar = $this->getAvatarUploaded($request);

        if (!$user->save()) {
            return $this->redirectWithError('Failed on Updating a User!.');
        }

        return $this->redirect('Successfuly Updated a User!', 'users');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return $this->redirect('Successfully Deleted a User!', 'users');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {

        $cachedData = Cache::rememberForever('api.users.' . $request->getRequestUri(), function () {
            $users = DB::table('users as users')
                ->join('roles as roles', 'users.role_id', '=', 'roles.id')
                ->where('company_id',session('company.id'))
                ->select([
                    'users.id',
                    'users.firstname',
                    'users.lastname',
                    'users.email',
                    'users.contact_no',
                    'users.avatar',
                    'users.is_activated',
                    'roles.name as role',
                    DB::raw('concat (users.firstname," ",users.lastname) as name')
                ]);

            return Datatables::of($users)
                ->filterColumn('name', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->setTransformer('Transformers\UsersTransformer')
                ->make(true);
        });

        return $cachedData;
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getToggleActivateUser($id)
    {
        $status = 0;
        $user = User::findOrFail($id);
        if ($user->is_activated == 0) {
            $status = 1;
        }
        $user->is_activated = $status;
        if (!$user->save()) {
            return $this->redirectWithError('Failed on Updating a User!.');
        }

        return $this->redirect('Successfuly Updated a User!', 'users');

    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function getChangeUserPassword($id)
    {
        $data['user'] = User::select('*')->findOrFail($id);

        return view('users.changePassword', $data);

    }

    /**
     * @param ChangeUserPasswordRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function putChangeUserPassword(ChangeUserPasswordRequest $request, $id)
    {
        $user = User::select('*')->findOrFail($id);
        $user->password = Hash::make($request->password);
        if (!$user->save()) {
            return $this->redirectWithError('Failed on Updating a User!.');
        }

        return $this->redirect('Successfuly Updated a User!', 'users');
    }
}
