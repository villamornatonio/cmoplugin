<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCourseRequest;
use App\Http\Requests\UpdateCourseRequest;
use App\Models\Course;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\CoursesTransformer;
use yajra\Datatables\Datatables;


/**
 * Class CoursesController
 * @package App\Http\Controllers
 */
class CoursesController extends AdminController
{

    /**
     * @var CoursesTransformer
     */
    protected $coursesTransformer;


    /**
     * @param CoursesTransformer $coursesTransformer
     */
    function __construct(CoursesTransformer $coursesTransformer)
    {
        $this->middleware('auth');
        $this->coursesTransformer = $coursesTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        return view('courses.index');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create()
    {
        return view('courses.create');
    }

    /**
     * @param StoreCourseRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCourseRequest $request)
    {
        $parameters = $request->only('name', 'code', 'duration', 'price');
        $parameters['created_by'] = 1; //logged in USER;
        $parameters['company_id'] = session('company.id');

        if (!Course::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New Course!.');
        }

        return $this->redirect('Successfuly Added a New Course!', 'courses');
    }


    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['course'] = $this->coursesTransformer->transform(Course::select([
                'id as course_id',
                'name as course_name',
                'price as course_price',
                'duration as course_duration',
                'code as course_code'
            ]
        )->findOrFail($id));

        return view('courses.edit', $data);
    }

    /**
     * @param UpdateCourseRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateCourseRequest $request, $id)
    {
        $parameters = $request->only('name', 'code', 'duration', 'price');
        $course = Course::select('*')->findOrFail($id);

        $course->name = $parameters['name'];
        $course->code = $parameters['code'];
        $course->duration = $parameters['duration'];
        $course->price = $parameters['price'];
        if (!$course->save()) {
            return $this->redirectWithError('Failed on Updating a Course!.');
        }

        return $this->redirect('Successfuly Updated a Course!', 'courses');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $course = Course::findOrFail($id);
        $course->delete();

        return $this->redirect('Successfuly Deleted a Course!', 'courses');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.courses.' . $request->getRequestUri(), function () {
            $courses = DB::table('courses as courses')
                ->join('users as users', 'courses.created_by', '=', 'users.id')
                ->where('courses.company_id',session('company.id'))
                ->select([
                    'courses.id as course_id',
                    'courses.name as course_name',
                    'courses.price as course_price',
                    'courses.code as course_code',
                    'courses.duration as course_duration',
                    DB::raw('concat (users.firstname," ",users.lastname) as author, users.id')
                ]);

            return Datatables::of($courses)
                ->setTransformer('Transformers\CoursesTransformer')
                ->make(true);
        });

        return $cachedData;

    }
}
