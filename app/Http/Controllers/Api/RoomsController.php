<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\AdminController;
use App\Http\Requests\StoreRoomRequest;
use App\Http\Requests\UpdateRoomRequest;
use App\Models\Room;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\RoomsTransformer;
use yajra\Datatables\Datatables;

class RoomsController extends AdminController
{

    protected $roomsTransformer;

    /**
     * @param RoomsTransformer $roomsTransformer
     */
    function __construct(RoomsTransformer $roomsTransformer)
    {
        $this->middleware('auth');
        $this->roomsTransformer = $roomsTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $cachedData = Cache::rememberForever('api.rooms.' . $request->getRequestUri(), function () {
            $rooms = DB::table('rooms as rooms')
                ->join('users as users', 'rooms.created_by', '=', 'users.id')
                ->where('rooms.company_id',session('company.id'))
                ->select([
                    'rooms.id',
                    'rooms.name',
                    DB::raw('concat (users.firstname," ",users.lastname) as author')
                ]);

            return Datatables::of($rooms)
                ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->make(true);

        });

        return $cachedData;
    }


    /**
     * Store a newly created resource in storage.
     * @param StoreRoomRequest $request
     * @return Response
     * @internal param Request $request
     */
    public function store(StoreRoomRequest $request)
    {
        $parameters = $request->only('name');
        $parameters['created_by'] = auth()->user()->id;

        $room = Room::create($parameters);

        if (!$room) {
            return $this->responseWithError('Failed on Adding a New Room!.');
        }

        return $this->respond('Successfuly Added a New Room!', $this->roomsTransformer->transform($room));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $data = Room::select('*')->findOrFail($id);
        return $this->respond('Data Found',$this->roomsTransformer->transform($data));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(UpdateRoomRequest $request, $id)
    {
        $parameters = $request->only('name');
        $room = Room::select('*')->findOrFail($id);
        $room->name = $parameters['name'];
        if (!$room->save()) {
            return $this->responseWithError('Failed on Updating a Room!.');
        }

        return $this->respond('Successfuly Updated a Room!', 'rooms');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();

        return $this->respond('Successfuly Deleted a Room!', 'rooms');
    }


    public function bulkDelete($id,Request $request)
    {
        $selectedItems = explode(',',$id);
        Room::destroy($selectedItems);
        return $this->respond('Successfuly Deleted a Room!/s', 'rooms');
    }
}
