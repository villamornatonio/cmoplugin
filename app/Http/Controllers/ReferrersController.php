<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreReferrerRequest;
use App\Http\Requests\UpdateReferrerRequest;
use App\Models\Referrer;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Transformers\ReferrersTransformer;
use yajra\Datatables\Datatables;

/**
 * Class ReferrersController
 * @package App\Http\Controllers
 */
class ReferrersController extends AdminController
{
    /**
     * @var ReferrersTransformer
     */
    protected $referrersTransformer;

    /**
     * @param ReferrersTransformer $referrersTransformer
     */
    function __construct(ReferrersTransformer $referrersTransformer)
    {
        $this->middleware('auth');
        $this->referrersTransformer = $referrersTransformer;
        $this->restrictOnly('admin');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        return view('referrers.index');
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create()
    {
        return view('referrers.create');
    }

    /**
     * @param StoreReferrerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreReferrerRequest $request)
    {
        $parameters = $request->only('firstname', 'lastname', 'company', 'email', 'contact_no');
        $parameters['created_by'] = auth()->user()->id;
        $parameters['company_id'] = session('company.id');

        if (!Referrer::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New Refferer!.');
        }

        return $this->redirect('Successfuly Added a New Referrer!', 'referrers');
    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function show($id)
    {
        return view('referrers.show');
    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['referrer'] = $this->referrersTransformer->transform(Referrer::select('*')->findOrFail($id));

        return view('referrers.edit', $data);
    }

    /**
     * @param UpdateReferrerRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateReferrerRequest $request, $id)
    {
        $parameters = $request->only('firstname', 'lastname', 'company', 'email', 'contact_no');
        $referrer = Referrer::select('*')->findOrFail($id);
        $referrer->firstname = $parameters['firstname'];
        $referrer->lastname = $parameters['lastname'];
        $referrer->company = $parameters['company'];
        $referrer->email = $parameters['email'];
        $referrer->contact_no = $parameters['contact_no'];
        if (!$referrer->save()) {
            return $this->redirectWithError('Failed on Updating a Referrer!.');
        }

        return $this->redirect('Successfuly Updated a Referrer!', 'referrers');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $referrer = Referrer::findOrFail($id);
        $referrer->delete();

        return $this->redirect('Successfuly Deleted a Referrer!', 'referrers');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.referrer.' . $request->getRequestUri(), function () {
            $referrers = DB::table('referrers as referrers')
                ->select([
                    'referrers.id',
                    'referrers.firstname',
                    'referrers.lastname',
                    'referrers.contact_no',
                    'referrers.company',
                    'referrers.email',
                    'referrers.dob',
                    DB::raw('concat (referrers.firstname," ",referrers.lastname) as name')
                ]);

            return Datatables::of($referrers)
                ->setTransformer('Transformers\ReferrersTransformer')
                ->filterColumn('name', 'whereRaw', "CONCAT(referrers.firstname,' ',referrers.lastname) like ?", ["$1"])
                ->make(true);
        });

        return $cachedData;
    }
}
