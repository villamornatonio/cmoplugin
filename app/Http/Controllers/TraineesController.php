<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTraineeRequest;
use App\Http\Requests\UpdateTraineeRequest;
use App\Models\Trainee;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDO;
use Transformers;
use Transformers\TraineesTransformer;
use yajra\Datatables\Datatables;

/**
 * Class TraineesController
 * @package App\Http\Controllers
 */
class TraineesController extends AdminController
{
    /**
     * @var TraineesTransformer
     */
    protected $traineesTransformer;
    /**
     * @var Transformers\EnrollmentsTransformer
     */
    protected $enrollmentsTransformer;

    /**
     * @param TraineesTransformer $traineesTransformer
     * @param Transformers\EnrollmentsTransformer $enrollmentsTransformer
     */
    function __construct(
        TraineesTransformer $traineesTransformer,
        Transformers\EnrollmentsTransformer $enrollmentsTransformer
    ) {
        $this->middleware('auth');
        $this->traineesTransformer = $traineesTransformer;
        $this->enrollmentsTransformer = $enrollmentsTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index()
    {
        $data = [];

        return view('trainees.index', $data);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create()
    {
        return view('trainees.create');
    }

    /**
     * @param StoreTraineeRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreTraineeRequest $request)
    {

        $parameters = $request->only('position',
            'firstname',
            'middlename',
            'lastname',
            'address',
            'passport_no',
            'seamans_no',
            'mobile',
            'dob',
            'email',
            'emergency_person',
            'emergency_contact',
            'temp_id'
        );
        $parameters['dob'] = Carbon::parse($parameters['dob'])->format('Y-m-d');
        $parameters['created_by'] = auth()->user()->id;
        $parameters['avatar'] = $this->getAvatarUploaded($request, config()->get('uploads.trainee'));
        $parameters['company_id'] = session('company.id');

        if (!Trainee::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New Trainee!.');
        }

        return $this->redirect('Successfuly Added a New Trainee!', 'trainees');
    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function show($id)
    {
        $data['trainee'] = $this->traineesTransformer->transform(Trainee::select('*')->findOrFail($id));
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $enrollments = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where('enrollments.trainee_id', $id)
            ->select([
                'trainees.id as id',
                'enrollments.id as enrollement_id',
                'courses.name as course',
                'courses.id as course_id',
                'courses.code as course_code',
                'rooms.name as room',
                'rooms.id as room_id',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'courses.price as price',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ])->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        $data['enrollments'] = $this->enrollmentsTransformer->transformCollection($enrollments);

        return view('trainees.show', $data);
    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {

        $data['trainee'] = $this->traineesTransformer->transform(Trainee::select('*')->findOrFail($id));
        $data['trainee']['dob'] = Carbon::parse($data['trainee']['dob'])->format('Y-m-d');

        return view('trainees.edit', $data);
    }

    /**
     * @param UpdateTraineeRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateTraineeRequest $request, $id)
    {
        $parameters = $request->only('position',
            'firstname',
            'middlename',
            'lastname',
            'address',
            'passport_no',
            'seamans_no',
            'mobile',
            'dob',
            'email',
            'emergency_person',
            'emergency_contact',
            'temp_id'
        );

        $trainee = Trainee::select('*')->findOrFail($id);
        $trainee->firstname = $parameters['firstname'];
        $trainee->middlename = $parameters['middlename'];
        $trainee->lastname = $parameters['lastname'];
        $trainee->address = $parameters['address'];
        $trainee->passport_no = $parameters['passport_no'];
        $trainee->seamans_no = $parameters['seamans_no'];
        $trainee->mobile = $parameters['mobile'];
        $trainee->dob = Carbon::parse($parameters['dob'])->format('Y-m-d');
        $trainee->email = $parameters['email'];
        $trainee->emergency_person = $parameters['emergency_person'];
        $trainee->emergency_contact = $parameters['emergency_contact'];
        $trainee->temp_id = $parameters['temp_id'];
        $trainee->avatar = $this->getAvatarUploaded($request, config()->get('uploads.trainee'));

        if (!$trainee->save()) {
            return $this->redirectWithError('Failed on Updating a Trainee!.');
        }

        return $this->redirect('Successfuly Updated a Trainee!', 'trainees');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $trainee = Trainee::findOrFail($id);
        $trainee->delete();

        return $this->redirect('Successfuly Deleted a Trainee!', 'trainees');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.trainees.' . $request->getRequestUri(), function () {
            $trainess = DB::table('trainees as trainees')
                ->join('users as users', 'trainees.created_by', '=', 'users.id')
                ->where('trainees.company_id',session('company.id'))
                ->select([
                    'trainees.id',
                    'trainees.firstname',
                    'trainees.lastname',
                    'trainees.position',
                    'trainees.middlename',
                    'trainees.passport_no',
                    'trainees.seamans_no',
                    'trainees.mobile',
                    'trainees.dob',
                    'trainees.email',
                    'trainees.emergency_person',
                    'trainees.emergency_contact',
                    'trainees.created_by',
                    'trainees.created_at',
                    'trainees.avatar',
                    'trainees.temp_id',
                    DB::raw('concat (users.firstname," ",users.lastname) as author'),
                    DB::raw('concat (trainees.firstname," ",trainees.lastname) as name')

                ]);

            return Datatables::of($trainess)
                ->filterColumn('author', 'whereRaw', "CONCAT(users.firstname,' ',users.lastname) like ?", ["$1"])
                ->filterColumn('name', 'whereRaw', "CONCAT(trainees.firstname,' ',trainees.lastname) like ?", ["$1"])
                ->setTransformer('Transformers\TraineesTransformer')
                ->make(true);
        });

        return $cachedData;

    }
}
