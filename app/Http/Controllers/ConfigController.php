<?php

namespace App\Http\Controllers;

use App\Models\Config;
use Illuminate\Http\Request;


class ConfigController extends AdminController
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->restrictTo(['admin', 'registrar']);
    }

    public function create()
    {
        $configData = [
            'shop_url' => '',
            'name' => '',
            'address' => '',
            'phone' => '',
            'shop_hostname' => '',
            'api_key' => '',
            'password' => '',
            'location_id' => '',
            'tracking_company' => ''
        ];


        if(Config::where('user_id',auth()->user()->id)->exists()){

            $config = Config::where('user_id',auth()->user()->id)->first();
            $configData = [
                'shop_url' => $config->shop_url,
                'name' => $config->name,
                'address' => $config->address,
                'phone' => $config->phone,
                'shop_hostname' => $config->shop_hostname,
                'api_key' => $config->api_key,
                'password' => $config->password,
                'location_id' => $config->location_id,
                'tracking_company' => $config->tracking_company
            ];
        }
        $data['configData'] = $configData;
        return view('config.create', $data);

    }


    public function store(Request $request)
    {
        $parameters = $request->all();
        $config = NULL;
        if(Config::where('user_id',auth()->user()->id)->exists()){
            //update
            $config = Config::where('user_id',auth()->user()->id)->first();
        }else{
            //create new
            $config = new Config();
        }

        $config->shop_url = $parameters['shop_url'];
        $config->name = $parameters['name'];
        $config->address = $parameters['address'];
        $config->phone = $parameters['phone'];
        $config->user_id = auth()->user()->id;
        $config->shop_hostname = $parameters['shop_hostname'];
        $config->api_key = $parameters['api_key'];
        $config->password = $parameters['password'];
        $config->location_id = $parameters['location_id'];
        $config->tracking_company = $parameters['tracking_company'];


        if (!$config->save()) {
            return $this->redirectWithError(trans('message.rooms.create.fail'));
        }

        return $this->redirect(trans('message.rooms.create.success'), 'config/create');
    }


}
