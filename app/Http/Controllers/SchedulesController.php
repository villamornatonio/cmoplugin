<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreScheduleRequest;
use App\Http\Requests\UpdateScheduleRequest;
use App\Models\Course;
use App\Models\Room;
use App\Models\Schedule;
use App\Models\Trainee;
use Carbon\Carbon;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use PDO;
use Transformers\SchedulesTransformer;
use Transformers\TraineesTransformer;
use yajra\Datatables\Datatables;


/**
 * Class SchedulesController
 * @package App\Http\Controllers
 */
class SchedulesController extends AdminController
{

    /**
     * @var SchedulesTransformer
     */
    protected $schedulesTransformer;
    /**
     * @var TraineesTransformer
     */
    protected $traineesTransformer;


    /**
     * @param SchedulesTransformer $schedulesTransformer
     * @param TraineesTransformer $traineesTransformer
     */
    function __construct(SchedulesTransformer $schedulesTransformer, TraineesTransformer $traineesTransformer)
    {
        $this->middleware('auth');
        $this->schedulesTransformer = $schedulesTransformer;
        $this->traineesTransformer = $traineesTransformer;
        $this->restrictTo(['admin', 'registrar']);
    }


    /**
     * @param null $trainee_id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function index($trainee_id = null)
    {
        $data = [];
        if (isset($trainee_id)) {
            $data['trainee'] = $this->traineesTransformer->transform(Trainee::findOrFail($trainee_id));
            $data['trainee_id'] = $trainee_id;
        }

        return view('schedules.index', $data);
    }

    /**
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function create()
    {
        $data['rooms'] = Room::lists('name', 'id')->toArray();
        $data['courses'] = Course::lists('code', 'id')->toArray();

        return view('schedules.create', $data);
    }

    /**
     * @param StoreScheduleRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreScheduleRequest $request)
    {
        $parameters = $request->only('room_id', 'course_id', 'start_date', 'end_date', 'start_time', 'end_time');
        $parameters['created_by'] = auth()->user()->id;
        $parameters['company_id'] = session('company.id');

        if (!Schedule::create($parameters)) {
            return $this->redirectWithError('Failed on Adding a New Schedule!.');
        }

        return $this->redirect('Successfuly Added a New Schedule!', 'schedules');


    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function show($id)
    {

        DB::setFetchMode(PDO::FETCH_ASSOC);
        $enrollees = DB::table('enrollments as enrollments')
            ->join('users as users', 'enrollments.created_by', '=', 'users.id')
            ->join('schedules as schedules', 'enrollments.schedule_id', '=', 'schedules.id')
            ->join('trainees as trainees', 'enrollments.trainee_id', '=', 'trainees.id')
            ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
            ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
            ->where('enrollments.schedule_id', $id)
            ->where('enrollments.company_id', session('company.id'))
            ->orderBy('schedules.created_at', 'asc')
            ->select([
                'enrollments.id',
                'courses.name as course',
                'courses.price as price',
                'rooms.name as room',
                'schedules.start_date as start_date',
                'schedules.end_date as end_date',
                'schedules.start_time as start_time',
                'schedules.end_time as end_time',
                'enrollments.is_completed',
                'enrollments.is_company_charged',
                'trainees.firstname as firstname',
                'trainees.middlename as middlename',
                'trainees.lastname as lastname',
                'trainees.passport_no as passport_no',
                'trainees.seamans_no as seamans_no',
                'trainees.mobile as mobile',
                'trainees.dob as dob',
                'trainees.email as email',
                'trainees.emergency_contact as emergency_contact',
                'trainees.emergency_person as emergency_person',
                'trainees.created_at as created_at',
                'trainees.position as position',
                'trainees.avatar as avatar',
                DB::raw('concat (users.firstname," ",users.lastname) as author'),
                DB::raw('concat (trainees.firstname," ",trainees.lastname) as trainee')
            ])->get();
        DB::setFetchMode(PDO::FETCH_CLASS);

        if (empty($enrollees)) {
            return $this->redirectWithError('Resource Not Found!');
        }


        $data['course'] = isset($enrollees[0]['course']) ? $enrollees[0]['course'] : 'NONE';
        $data['schedule'] = Carbon::parse($enrollees[0]['start_date'])->toFormattedDateString() . ' - ' . Carbon::parse($enrollees[0]['end_date'])->toFormattedDateString();
        $data['enrollees'] = $this->traineesTransformer->transformCollection($enrollees);

        return view('schedules.show', $data);
    }

    /**
     * @param $id
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function edit($id)
    {
        $data['rooms'] = Room::lists('name', 'id')->toArray();
        $data['courses'] = Course::lists('name', 'id')->toArray();
        $data['schedule'] = Schedule::findOrFail($id)->toArray();

        return view('schedules.edit', $data);
    }

    /**
     * @param UpdateScheduleRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateScheduleRequest $request, $id)
    {
        $parameters = $request->only('room_id', 'course_id', 'start_date', 'end_date', 'start_time', 'end_time');
        $schedule = Schedule::findOrFail($id);
        $schedule->room_id = $parameters['room_id'];
        $schedule->course_id = $parameters['course_id'];
        $schedule->start_date = $parameters['start_date'];
        $schedule->end_date = $parameters['end_date'];
        $schedule->start_time = $parameters['start_time'];
        $schedule->end_time = $parameters['end_time'];
        if (!$schedule->save()) {
            return $this->redirectWithError('Failed on Updating a Schedule!.');
        }

        return $this->redirect('Successfuly Updated a Schedule!', 'schedules');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->delete();

        return $this->redirect('Successfuly Deleted a Schedule!', 'schedules');
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getAll(Request $request)
    {
        $cachedData = Cache::rememberForever('api.schedules.' . $request->getRequestUri(), function () {
            $schedules = DB::table('schedules as schedules')
                ->join('users as users', 'schedules.created_by', '=', 'users.id')
                ->join('rooms as rooms', 'schedules.room_id', '=', 'rooms.id')
                ->join('courses as courses', 'schedules.course_id', '=', 'courses.id')
                ->select([
                    'schedules.id',
                    'schedules.start_date',
                    'schedules.end_date',
                    'schedules.start_time',
                    'schedules.end_time',
                    DB::raw('rooms.name as room'),
                    DB::raw('courses.name as course'),
                    DB::raw('courses.price as price')
                ]);

            return Datatables::of($schedules)
                ->setTransformer('Transformers\SchedulesTransformer')
                ->make(true);
        });

        return $cachedData;
    }

}
