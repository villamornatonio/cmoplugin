<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$router->get('/', 'PagesController@home');
$router->get('/contact-us', 'PagesController@contactUs');
$router->get('/about-us', 'PagesController@aboutUs');
$router->get('/team', 'PagesController@team');
$router->get('/courses', 'PagesController@courses');
$router->get('/facilities', 'PagesController@facilities');
$router->get('/convert-csv', 'PagesController@convertCSV');
$router->controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

$router->resource('users','UsersController',['only' => ['create','store','edit','update','destroy','index']]);
$router->get('users/{user}/toggle/active', 'UsersController@getToggleActivateUser');
$router->get('users/{user}/password', 'UsersController@getChangeUserPassword');
$router->put('users/{user}/password', 'UsersController@putChangeUserPassword');
$router->get('reports/daily/{date}', 'ReportsController@daily');
$router->get('reports/daily', 'ReportsController@daily');
$router->get('reports/weekly', 'ReportsController@weekly');
$router->get('reports/weekly/{date}', 'ReportsController@weekly');
$router->get('reports/monthly', 'ReportsController@monthly');
$router->get('reports/monthly/{date}', 'ReportsController@monthly');
$router->resource('rooms','RoomsController',['only' => ['create','store','edit','update','destroy', 'index']]);
$router->resource('referrers','ReferrersController',['only' => ['create','store','edit','update','destroy', 'index']]);
$router->resource('trainees','TraineesController',['only' => ['show','create','store','edit','update','destroy','index']]);
$router->resource('courses','CoursesController',['only' => ['create','store','edit','update','destroy','index']]);
$router->resource('schedules','SchedulesController',['only' => ['show','create','store','edit','update','destroy','index']]);
$router->get('schedules/{user}/enroll', 'SchedulesController@index');
$router->get('schedules/{schedule}/enrolled', 'SchedulesController@getEnrolled');
$router->resource('payments','PaymentsController',['only' => ['store','edit','update','destroy','index']]);
$router->get('payments/{enrollment}/create', 'PaymentsController@create');
$router->get('payments/{enrollment}/partial', 'PaymentsController@getPartial');
$router->post('payments/partial', 'PaymentsController@postPartial');
$router->get('payments/pending', 'PaymentsController@pending');
$router->get('payments/partial', 'PaymentsController@partial');
$router->resource('enrollments','EnrollmentsController',['only' => ['destroy','create','edit','update','store','index']]);
$router->get('enrollments/{schedule}/trainee/{trainee}/create', 'EnrollmentsController@create');
$router->get('certificates/{enrollment}/release', 'TrainingsController@releaseEnrollmentCertificate');
$router->get('trainings/completed', 'TrainingsController@completed');
$router->get('trainings/unfinished', 'TrainingsController@unfinished');
$router->get('trainings/{user}/toggle/completed', 'TrainingsController@getToggleCompleted');
$router->resource('trainings', 'TrainingsController',['only' => ['update']]);
$router->get('dashboard', 'DashboardController@index');
$router->get('receipts/{or}/full', 'ReceiptsController@fullPayment');

$router->group(['prefix' => 'orders'], function($router){
    $router->get('/','OrdersController@index');
    $router->get('create','OrdersController@create');
    $router->get('export','OrdersController@export');
    $router->post('export','OrdersController@postExport');
    $router->get('import','OrdersController@import');
    $router->post('import','OrdersController@postImport');
    $router->get('print-packing-slip','OrdersController@printSlip');
    $router->get('print-packing-lbc','OrdersController@printLBC');
});

$router->group(['prefix' => 'config'], function($router){
    $router->get('create','ConfigController@create');
    $router->post('create','ConfigController@store');
});

$router->group(['prefix' => 'api/v1/'], function($router){
    $router->resource('rooms','Api\RoomsController',['only' => ['store','update','destroy', 'index', 'show']]);
    $router->delete('rooms/bulk/delete/{id}','Api\RoomsController@bulkDelete');
    $router->get('orders','Api\OrdersController@index');
    $router->get('referrers','ReferrersController@getAll');
    $router->get('trainees','TraineesController@getAll');
    $router->get('courses','CoursesController@getAll');
    $router->get('users','UsersController@getAll');
    $router->get('payments','PaymentsController@getAll');
    $router->get('payments/pending','PaymentsController@getAllPendingEnrollment');
    $router->get('payments/partials','PaymentsController@getAllPartialPayments');
    $router->get('enrollments','EnrollmentsController@getAll');
    $router->get('schedules','SchedulesController@getAll');
    $router->get('trainings/{status}/all','TrainingsController@getAll');
    $router->get('reports/monthly/{date}','ReportsController@getMonthlyData');
    $router->get('reports/weekly/{date}','ReportsController@getWeeklyData');
    $router->get('reports/daily/{date}','ReportsController@getDailyData');

});

$router->resource('vues', 'VueController');

$router->get('test', 'TestsController@test');