<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreTraineeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyId = session('company.id');
        return [
//            'position' => 'required|max:50',
            'firstname' => 'required|max:50',
            'middlename' => 'max:50',
            'lastname' => 'required|:max:50',
//            'passport_no' => 'max:50|unique:trainees,passport_no',
//            'seamans_no' => 'max:50|unique:trainees,seamans_no',
            'mobile' => 'required|:max:50',
            'dob' => 'required|date|date_format:"Y-m-d"|max:50',
            'temp_id' => "required|should_be_unique_to_company:trainees,temp_id,{$companyId}",
//            'email' => 'required|email|:max:50|unique:trainees,email',
//            'emergency_person' => 'max:50',
//            'emergency_contact' => 'max:50',
//            'avatar' => 'mimes:png,jpg,jpeg'
        ];
    }
}
