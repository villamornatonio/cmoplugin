<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreCourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyId = session('company.id');
        return [
            'name' => "required|max:60|should_be_unique_to_company:courses,name,{$companyId}",
            'code' => "required|max:20|should_be_unique_to_company:courses,code,{$companyId}",
            'price' => 'required|numeric',
            'duration' => 'required|numeric',
        ];
    }
}
