<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UpdateReferrerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required|max:60',
            'lastname' => 'required|max:60',
            'company' => 'max:60',
            'email' => 'email|max:60',
            'contact_no' => 'required|max:60',
            'dob' => 'date|date_format:"Y-m-d"|max:50',
        ];
    }
}
