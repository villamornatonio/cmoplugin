<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StorePartialPaymentRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyId = session('company.id');
        return [
            'payment_id' => 'required',
            'amount' => 'required|numeric',
            'trainee_id' => 'required|numeric',
            'discount' => 'required|numeric',
            'or_no' => "required|max:50|should_be_unique_to_company:payments,or_no,{$companyId}"

        ];
    }
}
