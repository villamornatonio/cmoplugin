<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreReferrerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyId = session('company.id');
        return [
            'firstname' => 'required|max:60',
            'lastname' => 'required|max:60',
            'company' => 'max:60',
            'email' => "email|max:60|should_be_unique_to_company:referrers,email,{$companyId}",
            'contact_no' => 'required|max:60',
            'dob' => 'date|date_format:"Y-m-d"|max:50',
        ];
    }
}
