<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $companyId = session('company.id');
        return [
            'role_id' => 'required',
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'email' => "required|email|max:255|should_be_unique_to_company:rooms,name,{$companyId}",
            'password' => 'required|confirmed|min:6',
            'contact_no' => 'required',
            'avatar' => 'mimes:png,jpg,jpeg'

        ];
    }
}
