<?php

namespace App\Listeners;

use App\Events\ModelWasChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Cache;

class ForgetCachedData
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ModelWasChanged  $event
     * @return void
     */
    public function handle(ModelWasChanged $event)
    {
//        Cache::forget('api.users.*');
//        Cache::forget('api.rooms.*');
//        Cache::forget('*');
//        Cache::forget('api.referrers.*');
//        Cache::forget('api.courses.*');
//        Cache::forget('api.schedules.*');
//        Cache::forget('api.trainees.*');
//        Cache::forget('api.courses.*');
//        Cache::forget('api.enrollments.*');
//        Cache::forget('api.training.*');
//        Cache::forget('api.training.*');
        Cache::flush();
    }
}
