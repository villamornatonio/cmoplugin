<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class CustomValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('should_be_unique_to_company', function($attribute, $value, $parameters, $validator) {
            $table = $parameters[0];
            $field = $parameters[1];
            $company = $parameters[2];
            $isValueUniqueOnDB = DB::table($table)->where($field,$value)->where('company_id',$company)->count();
            if($isValueUniqueOnDB == 0){
                return true;
            }
            return false;
        });

        Validator::replacer('should_be_unique_to_company', function($message, $attribute, $rule, $parameters) {
            $message = [
                "should_be_unique_to_company" => "The {$attribute} already exists on the Company."
            ];
            return $message[$rule];
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
