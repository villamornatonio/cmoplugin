/**
 * Created by villamornatonio on 9/13/15.
 */


$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});


$(document).ready(function () {
    /* ================================================= UTILITIES =======================================================*/

    /**
     * Utility Object - Get Base URL, Image URL, Image Loader URL etc.
     */
    util = new Object({
        //get base url
        getBaseURL: function () {

            return location.protocol + "//" + location.host + '/';

        },
        //get image url /
        getImageURL: function () {
            return this.getBaseURL() + "img/";
        },
        //get image loader url
        getImageLoadURL: function () {
            return this.getImageURL() + "ajax-loader.gif";
        },
        //get image pre loader url
        getPreloader: function () {
            return this.getImageURL() + "loading.gif";
        },
        //get image error url
        getImageErrURL: function () {
            return this.getImageURL() + "error-img.jpg";
        },

        redirect: function ($uri) {
            window.location.href = this.getBaseURL() + $uri;
        }
    });

    api = new Object({
        getWeeklyData: function(date){
            var uri = 'api/v1/reports/weekly/' + date;
            var response = null;
            $.ajax({
                url: util.getBaseURL() + uri,
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function (resp) {
                    response = resp;
                }
            });

            return response;

        },
        getMonthylyData: function(date){
            var uri = 'api/v1/reports/monthly/' + date;
            var response = null;
            $.ajax({
                url: util.getBaseURL() + uri,
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function (resp) {
                    response = resp;
                }
            });

            return response;
        },
        getDailyData: function(date){
            var uri = 'api/v1/reports/daily/' + date;
            var response = null;
            $.ajax({
                url: util.getBaseURL() + uri,
                type: 'GET',
                dataType: 'JSON',
                async: false,
                success: function (resp) {
                    response = resp;
                }
            });

            return response;
        }
    });


});

$(document).on('click', 'a[data-method="delete"]', function (event) {
    event.preventDefault();
    var options = {
        onConfirm: function() {
            var that = $(this).parent().parent().parent().parent().find('a[data-method="delete"]');
            var href = $(that).data('url');
            console.log(href);
            var method = $(that).data('method');
            var csrfToken = $('meta[name="csrf-token"]').attr('content');
            var form = $('<form method="post" action="' + href + '"></form>');
            var metadataInput = '<input name="_method" value="' + method + '" type="hidden" />';
            metadataInput += '<input name="_token" value="' + csrfToken + '" type="hidden" />';
            form.hide().append(metadataInput).appendTo('body');
            form.submit();
        }

    };

    $(this).confirmation(options);

});



$(document).on('change', '#monthly-range', function (event) {
    var uri = 'reports/monthly/' + $(this).val();
    util.redirect(uri);
});
$(document).on('change', '#weekly-range', function (event) {
    var uri = 'reports/weekly/' + $(this).val();
    util.redirect(uri);
});
$(document).on('change', '#daily-range', function (event) {
    var uri = 'reports/daily/' + $(this).val();
    util.redirect(uri);
});


