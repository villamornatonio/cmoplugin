@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li class="active"> Config  </li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Create Order</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'config/create',
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}
                        <div class="form-group {{ ($errors->has('shop_url') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Shop URL <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('shop_url',$configData['shop_url'],['class' => 'form-control' ,'placeholder' => 'https://www.exmaple.com'])
                                !!}
                                @if($errors->has('shop_url'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('shop_url') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('name') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Name <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('name',$configData['name'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('name'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('address') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Address <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('address',$configData['address'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('address'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('address') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('phone') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Phone <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('phone',$configData['phone'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('phone'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('phone') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('shop_hostname') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Shop Hostname <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('shop_hostname',$configData['shop_hostname'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('shop_hostname'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('shop_hostname') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('api_key') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">API Key <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('api_key',$configData['api_key'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('api_key'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('api_key') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('password') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Password <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('password',$configData['password'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('password'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('password') }}</label>
                                @endif
                            </div>
                        </div>


                        <div class="form-group {{ ($errors->has('location_id') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Location ID <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('location_id',$configData['location_id'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('location_id'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('location_id') }}</label>
                                @endif
                            </div>
                        </div>

                        <div class="form-group {{ ($errors->has('tracking_company') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Tracking Company <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('tracking_company',$configData['tracking_company'],['class' => 'form-control' ,'placeholder' => ''])
                                !!}
                                @if($errors->has('tracking_company'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('tracking_company') }}</label>
                                @endif
                            </div>
                        </div>



                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Save Config</button>
                                <button type="reset"
                                        class="btn btn-quirk btn-wide btn-default">{{ trans('button.rooms.create.reset') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection