@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('orders') }}}">Orders</a></li>
            <li class="active"> Export CMO Order  </li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Export CMO Order</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => '/orders/export',
                        'method' => 'POST',
                        'class' => 'form-horizontal',
                        'files'=>'true'

                        ]) !!}

                        <div class="form-group {{ ($errors->has('orders') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">Orders List CSV <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                <input name="orders" type="file" class="form-control">
                                @if($errors->has('name'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('orders') }}</label>
                                @endif
                            </div>
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Export CMO Order</button>
                                <button type="reset"
                                        class="btn btn-quirk btn-wide btn-default">{{ trans('button.rooms.create.reset') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection