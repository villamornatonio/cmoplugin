@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('orders') }}}">Orders</a></li>
            <li class="active"> Create Order  </li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Create Order</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'orders',
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}

                        <div class="form-group {{ ($errors->has('name') ? 'has-error' : '' ) }}">
                            <label class="col-sm-3 control-label">{{ trans('label.rooms.create.name')  }} <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('name',null,['class' => 'form-control' ,'placeholder' => 'example: 209'])
                                !!}
                                @if($errors->has('name'))
                                    <label id="name-error" class="error" for="name">{{ $errors->first('name') }}</label>
                                @endif
                            </div>
                        </div>


                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Create Order</button>
                                <button type="reset"
                                        class="btn btn-quirk btn-wide btn-default">{{ trans('button.rooms.create.reset') }}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection