@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('orders') }}}">Orders</a></li>
            <li class="active">Display Orders List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Orders List</h4>

                <p>Manage Referrer</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>BUYER</th>
                            <th>ITEM</th>
                            <th>AMOUNT</th>
                            <th>MOBILE</th>
                            <th>STATUS</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/orders',
                "columnDefs": [ {
                    "targets": 6,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'><li><a href='/referrers/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li>" +
                            "<li>" +
                            "<a  data-url='/referrers/" + row['id'] + "' data-method='delete' class='btn' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                            "<i class='fa fa-trash'></i>" +
                            "</a>" +
                            "</li>" +
                            "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'buyer', name: 'buyer'},
                    {data: 'item_name', name: 'item_name'},
                    {data: 'amount', name: 'amount'},
                    {data: 'buyer_phone', name: 'buyer_phone'},
                    {data: 'status', name: 'status'}
                ],
                "order": [[ 0, "desc" ]],

                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                    });
                }

            });

        });
    </script>
@endsection