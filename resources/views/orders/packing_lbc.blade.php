@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('orders') }}}">Orders</a></li>
            <li class="active">Print Packing LBC</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Orders List</h4>

                <p>Manage Referrer</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>COMPANY</th>
                            <th>MOBILE</th>
                            <th>EMAIL</th>
                            <th>DATE OF BIRTH</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/referrers',
                "columnDefs": [ {
                    "targets": 6,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'><li><a href='/referrers/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li>" +
                            "<li>" +
                            "<a  data-url='/referrers/" + row['id'] + "' data-method='delete' class='btn' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                            "<i class='fa fa-trash'></i>" +
                            "</a>" +
                            "</li>" +
                            "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'referrers.id'},
                    {data: 'name', name: 'name'},
                    {data: 'company', name: 'referrers.company'},
                    {data: 'mobile', name: 'referrers.contact_no'},
                    {data: 'email', name: 'referrers.email'},
                    {data: 'dob', name: 'referrers.dob'}
                ],
                "order": [[ 0, "desc" ]],

                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                            .on('change', function () {
                                var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                column.search(val ? val : '', true, false).draw();
                            });
                    });
                }

            });

        });
    </script>
@endsection