@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('users') }}}">Users</a></li>
            <li class="active">Add New User</li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Add New User</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'users/' . $user->id,
                        'method' => 'PUT',
                        'files' => true,
                        'class' => 'form-horizontal'

                        ]) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Firstname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('firstname',isset($user->firstname) ? $user->firstname : null ,['class' => 'form-control' ,'placeholder' => 'John',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lastname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('lastname',isset($user->lastname) ? $user->lastname : null,['class' => 'form-control' ,'placeholder' => 'Doe',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Role <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::select('role_id', $roles ,isset($user->role_id) ? $user->role_id : null,['class' => 'form-control' ,
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::email('email',isset($user->email) ? $user->email : null,['class' => 'form-control' ,'placeholder' => 'johndoe@example.com',
                                'required' => true]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile No. <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('contact_no',isset($user->contact_no) ? $user->contact_no : null,['class' => 'form-control' ,'placeholder' => '092300000',
                                'required' => true]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Avatar <span class="text-danger">*</span></label>

                            <div class="col-sm-8 dropzone">
                                <div class="fallback">
                                    <input name="avatar" type="file" multiple/>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection