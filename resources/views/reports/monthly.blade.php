@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <div class="row">
            <div class="col-md-9 col-lg-8 dash-left">

                <div class="panel panel-site-traffic">
                    <div class="panel-heading">
                        <ul class="panel-options">
                            <li><a><i class="fa fa-refresh"></i></a></li>
                        </ul>
                        <h4 class="panel-title text-success">Monthly Report</h4>

                        <div class="row">
                            <div class="col-md-3">
                                <strong>Select Month</strong>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                {!! Form::select('monthly-range',$months,$month,['id' => 'monthly-range', 'class' =>
                                'form-control']) !!}
                            </div>
                        </div>

                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-6 col-sm-4">
                                <div class="pull-left">
                                    <div class="icon icon ion-stats-bars"></div>
                                </div>
                                <div class="pull-left">
                                    <h4 class="panel-title">New Trainees</h4>

                                    <h3>{{{ $newTrainees }}}</h3>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="pull-left">
                                    <div class="icon icon ion-eye"></div>
                                </div>
                                <h4 class="panel-title">Enrollments</h4>

                                <h3>{{{ $newEnrollments }}}</h3>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="pull-left">
                                    <div class="icon icon ion-clock"></div>
                                </div>
                                <h4 class="panel-title">Graduates</h4>

                                <h3>{{{ $graduates }}}</h3>
                            </div>
                        </div>
                        <!-- row -->

                        <div class="mb20"></div>

                        <div id="earnings-chart" style="height: 263px"></div>

                    </div>
                    <!-- panel-body -->

                    @if(!empty($enrollments))


                        <div class="table-responsive">
                            <table class="table table-bordered table-default table-striped nomargin">
                                <thead class="success">
                                <tr>
                                    <th>Courses</th>
                                    <th class="text-right">Percent of Enrollees</th>
                                    <th class="text-right">No. Of Enrollees</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($enrollments as $key => $enrollment)
                                    <tr>
                                        <td>{{ $enrollment['code'] }}</td>
                                        <td class="text-right">{{ percentage($enrollment['enrollees'],$newEnrollments) . '%' }}</td>
                                        <td class="text-right">{{ $enrollment['enrollees'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                                <!-- table-responsive -->

                </div>
                <!-- panel -->


                <div class="row row-col-join panel-earnings">
                    <div class="col-xs-3 col-sm-4 col-lg-3">
                        <div class="panel">
                            <ul class="panel-options">
                                <li><a><i class="glyphicon glyphicon-option-vertical"></i></a></li>
                            </ul>
                            <div class="panel-heading">
                                <h4 class="panel-title">Total Earnings</h4>
                            </div>
                            <div class="panel-body">
                                <h3 class="earning-amount">{{{ $dailyEarning }}}</h3>
                                <h4 class="earning-today">Todays's Earnings</h4>

                                <ul class="list-group">
                                    <li class="list-group-item">This Week <span
                                                class="pull-right">{{{ $weeklyEarning }}}</span>
                                    </li>
                                    <li class="list-group-item">This Month <span
                                                class="pull-right">{{{ $monthlyEarning }}}</span>
                                    </li>
                                </ul>
                                <hr class="invisible">
                                <p>Total new trainees this month: {{{ $monthlyTotalEnrollees }}}</p>
                            </div>
                        </div>
                        <!-- panel -->
                    </div>
                    <div class="col-xs-9 col-sm-8 col-lg-9">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Earnings Graph Overview</h4>
                            </div>
                            <div class="panel-body">
                                <div id="line-chart" class="body-chart"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row panel-quick-page">
                    <div class="col-xs-4 col-sm-5 col-md-4 page-user">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Manage Users</h4>
                            </div>
                            <div class="panel-body">
                                <div class="page-icon"><i class="icon ion-person-stalker"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-4 page-products">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Manage Courses</h4>
                            </div>
                            <div class="panel-body">
                                <div class="page-icon"><i class="fa fa-shopping-cart"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-5 col-md-2 page-reports">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Reports</h4>
                            </div>
                            <div class="panel-body">
                                <div class="page-icon"><i class="icon ion-arrow-graph-up-right"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-4 col-md-2 page-statistics">
                        <div class="panel">
                            <div class="panel-heading">
                                <h4 class="panel-title">Statistics</h4>
                            </div>
                            <div class="panel-body">
                                <div class="page-icon"><i class="icon ion-ios-pulse-strong"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- row -->

            </div>
            <!-- col-md-9 -->
            <div class="col-md-3 col-lg-4 dash-right">
                <div class="row">
                </div>
                <!-- row -->

                <div class="row">
                    @include('reports.recently_joined')
                    @include('reports.most_courses_completed')


                </div>
            </div>
            <!-- row -->

        </div>
        <!-- col-md-3 -->
    </div>
    <!-- row -->

    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function () {

            'use strict';
            $('#monthly-range').select2();
            var date = "{!! $month !!}";
            var data = api.getMonthylyData(date);

            var m1 = new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'line-chart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: data.data.earnings,
                xkey: 'y',
                ykeys: ['earnings'],
                labels: ['Monthly Earnings'],
                lineColors: ['#5BC0DE'],
                pointFillColors: ['#fff'],
                lineWidth: '3px',
                hideHover: true,
                gridTextColor: '#fff',
                grid: false
            });


            // Tooltip for flot chart
            function showTooltip(x, y, contents) {
                $('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y + 5,
                    left: x + 5
                }).appendTo('body').fadeIn(200);
            }

            var plot = $.plot($('#earnings-chart'), [{
                        data: data.data.trainees,
                        label: 'Monthly New Trainees',
                        color: '#cfd3da'
                    },
                        {
                            data: data.data.enrollments,
                            label: 'Monthly Enrollments',
                            color: '#06b5cf'
                        }
                        ,
                        {
                            data: data.data.graduates,
                            label: 'Monthly Graduates',
                            color: '#259dab'
                        }
                    ],
                    {
                        series: {
                            lines: {
                                show: false,
                            },

                            splines: {
                                show: true,
                                tension: 0.3,
                                lineWidth: 2,
                                fill: .50
                            },

                            shadowSize: 0
                        },

                        points: {show: true},

                        legend: {
                            container: '#basicFlotLegend',
                            noColumns: 0
                        },

                        grid: {
                            hoverable: true,
                            clickable: true,
                            borderColor: '#f3f5f7',
                            borderWidth: 0,
                            labelMargin: 5
                        },

                        yaxis: {
                            min: 0,
                            max: 100,
                            color: '#f3f5f7'
                        },

                        xaxis: {color: '#f3f5f7'}

                    });

            var previousPoint = null;

            $('#earnings-chart').bind('plothover', function (event, pos, item) {
                $('#x').text(pos.x.toFixed(2));
                $('#y').text(pos.y.toFixed(2));

                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $('#tooltip').remove();
                        var x = item.datapoint[0].toFixed(2),
                                y = item.datapoint[1].toFixed(2);

                        showTooltip(item.pageX, item.pageY, item.series.label + ' of ' + x + ' = ' + y);
                    }

                } else {
                    $('#tooltip').remove();
                    previousPoint = null;
                }
            });

            $('#earnings-chart').bind('plotclick', function (event, pos, item) {
                if (item) {
                    plot.highlight(item.series, item.datapoint);
                }
            });

            // Knob
//            $('.dial-success').knob({
//                readOnly: true,
//                width: '70px',
//                bgColor: '#E7E9EE',
//                fgColor: '#259CAB',
//                inputColor: '#262B36'
//            });
//
//            $('.dial-danger').knob({
//                readOnly: true,
//                width: '70px',
//                bgColor: '#E7E9EE',
//                fgColor: '#D9534F',
//                inputColor: '#262B36'
//            });
//
//            $('.dial-info').knob({
//                readOnly: true,
//                width: '70px',
//                bgColor: '#66BAC4',
//                fgColor: '#fff',
//                inputColor: '#fff'
//            });
//
//            $('.dial-warning').knob({
//                readOnly: true,
//                width: '70px',
//                bgColor: '#E48684',
//                fgColor: '#fff',
//                inputColor: '#fff'
//            });


        });

    </script>
@endsection
