<div class="col-sm-5 col-md-12 col-lg-6">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h4 class="panel-title">Most Completed Trainings</h4>
        </div>
        <div class="panel-body">
            @if(isset($mostCompletedCourses))
                <ul class="media-list user-list">
                    @foreach($mostCompletedCourses as $key => $enrollment)
                        <li class="media">
                            <div class="media-left">
                                <a href="{!! url('trainees/' . $enrollment['trainee_id']) !!}">
                                    <img class="media-object img-circle" src="{{ '/' . config()->get('uploads.trainee') . '/' . $enrollment['avatar'] }}"
                                         alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="{!! url('trainees/' . $enrollment['trainee_id']) !!}">{{{ $enrollment['trainee'] }}}</a></h4>
                                <span>{{{ $enrollment['courseCompleted'] }}}</span> Courses
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>