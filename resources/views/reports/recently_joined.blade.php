<div class="col-sm-5 col-md-12 col-lg-6">
    <div class="panel panel-success">
        <div class="panel-heading">
            <h4 class="panel-title">Recently Joined Trainees</h4>
        </div>
        <div class="panel-body">
            @if(isset($recentlyJoined))
                <ul class="media-list user-list">
                    @foreach($recentlyJoined as $key => $joined)
                        <li class="media">
                            <div class="media-left">
                                <a href="{!! url('trainees/' . $joined['id']) !!}">
                                    <img class="media-object img-circle"
                                         src="{{ '/' . config()->get('uploads.trainee') . '/' . $joined['avatar'] }}"
                                         alt="">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading nomargin"><a
                                            href="{!! url('trainees/' . $joined['id']) !!}">{{{ $joined['name'] }}}</a></h4>
                                has joined
                                <small class="date"><i
                                            class="glyphicon glyphicon-time"></i> {{{ $joined['joined'] }}}
                                </small>
                            </div>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
    <!-- panel -->
</div>