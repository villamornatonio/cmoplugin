@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('trainees') }}}">Trainees</a></li>
            <li class="active">Add New Trainee</li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Add New Trainee</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'trainees',
                        'files' => true,
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Temp ID <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('temp_id',null,['class' => 'form-control' ,'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Fisrtname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('firstname',null,['class' => 'form-control' ,'placeholder' => 'John',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Middlename <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('middlename',null,['class' => 'form-control' ,'placeholder' => 'Middle']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lastname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('lastname',null,['class' => 'form-control' ,'placeholder' => 'Doe',
                                'required' => true]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Address</label>

                            <div class="col-sm-8">
                                {!! Form::text('address',null,['class' => 'form-control' ,'placeholder' => 'Doe Street, Manila PH']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date of Birth <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">

                                <div class="input-group mb20">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    {!! Form::text('dob',null,['class' => 'form-control' ,'placeholder' => 'Date of
                                    Birth',
                                    'required' => true,'id' => 'dob']) !!}
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email</label>

                            <div class="col-sm-8">
                                {!! Form::email('email',null,['class' => 'form-control' ,'placeholder' =>
                                'johndoe@example.com',
                                'required' => true]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile No. <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('mobile',null,['class' => 'form-control' ,'placeholder' => '6391236545',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Passport No. <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('passport_no',null,['class' => 'form-control' ,'placeholder' => '']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Position<span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('position',null,['class' => 'form-control' ,'placeholder' => '',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Seamans Book No. <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('seamans_no',null,['class' => 'form-control' ,'placeholder' => '']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Emergency Person<span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('emergency_person',null,['class' => 'form-control' ,'placeholder' =>
                                '']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Emergency Contact No. <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('emergency_contact',null,['class' => 'form-control' ,'placeholder' =>
                                '']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Avatar <span class="text-danger">*</span></label>

                            <div class="col-sm-8 dropzone">
                                <div class="fallback">
                                    <input name="avatar" type="file" multiple/>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function () {
            $("#dob").mask("9999-99-99");
        });

    </script>
@endsection