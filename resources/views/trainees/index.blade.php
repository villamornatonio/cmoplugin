@extends('layouts.master')
@section('content')
    <div class="contentpanel">
        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('trainees') }}}">Trainees</a></li>
            <li class="active">Display Tranees List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Trainees List</h4>

                <p>Manage Trainess</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>POSITION</th>
                            <th>PASSPORT</th>
                            <th>SEAMANS BOOK</th>
                            <th>MOBILE</th>
                            <th>BIRTHDAY</th>
                            <th>EMAIL</th>
                            <th>EMERGENCY PERSON</th>
                            <th>EMERGENCY CONTACT</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/trainees',
                "columnDefs": [ {
                    "targets": 10,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'>" +
                                "<li>" +
                                "<a href='/schedules/" + row['id'] + "/enroll" + "'>" +
                                "<i class='fa fa-send'></i>" +
                                "</a>" +
                                "</li>" +
                                "<li>" +
                                "<a href='/trainees/" + row['id'] + "/edit" + "'>" +
                                "<i class='fa fa-pencil'></i>" +
                                "</a>" +
                                "</li>" +
                                "</li>" +
                                "<li>" +
                                "<a href='/trainees/" + row['id']  + "'>" +
                                "<i class='fa fa-user'></i>" +
                                "</a>" +
                                "</li>" +
                                "<li>" +
                                "<a  data-url='/trainees/" + row['id'] + "' data-method='delete' class='btn confirmation-callback' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                "<i class='fa fa-trash'></i>" +
                                "</a>" +
                                "</li>" +
                                "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'trainees.id'},
                    {data: 'name', name: 'name'},
                    {data: 'position', name: 'trainees.position'},
                    {data: 'passport_no', name: 'trainees.passport_no'},
                    {data: 'seamans_book_no', name: 'trainees.seamans_no'},
                    {data: 'mobile', name: 'trainees.mobile'},
                    {data: 'dob', name: 'trainees.dob'},
                    {data: 'email', name: 'trainees.email'},
                    {data: 'emergency_person', name: 'trainees.emergency_person'},
                    {data: 'emergency_contact', name: 'trainees.emergency_contact'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });

    </script>


@endsection

