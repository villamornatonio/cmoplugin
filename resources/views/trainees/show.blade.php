@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('trainees') }}}">Trainees</a></li>
            <li class="active">Display Trainee Information</li>
        </ol>

        @if(isset($trainee))
            <div class="panel panel-profile list-view">
                <div class="panel-heading">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-circle" src="/images/photos/user-default.png" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $trainee['name'] }}}</h4>

                            <p class="media-usermeta"><i
                                        class="glyphicon glyphicon-briefcase"></i>{{ $trainee['position'] }}</p>
                        </div>
                    </div>
                    <!-- media -->
                    <ul class="panel-options">
                        <li><a class="tooltips" href="" data-toggle="tooltip" title="View Options"><i
                                        class="glyphicon glyphicon-option-vertical"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body people-info">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Date of Birt</label>
                                {{{ $trainee['dob'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Email</label>
                                <i class="fa fa-envelope mr5"></i> {{{ $trainee['email'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Phone</label>
                                {{{ $trainee['mobile'] }}}
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Passport</label>
                                {{{ $trainee['passport_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Seamans Book</label>
                                {{{ $trainee['seamans_book_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Emergency Contact Person</label>

                                <div class="social-account-list">
                                    {{{ $trainee['emergency_person'] }}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div>
            </div><!-- panel -->
        @endif

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Course List</h4>

                <p>Course List</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>COURSE</th>
                            <th>ROOM</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th>HAS COMPLETED</th>
                        </tr>
                        </thead>
                        @if(isset($enrollments))
                            @foreach($enrollments as $ket => $enrollment)
                                <tr>
                                    <td>{!! $enrollment['course'] !!}</td>
                                    <td>{!! $enrollment['room'] !!}</td>
                                    <td>{!! $enrollment['startDate'] !!}</td>
                                    <td>{!! $enrollment['endDate'] !!}</td>
                                    <td>{!! $enrollment['startTime'] !!}</td>
                                    <td>{!! $enrollment['endTime'] !!}</td>
                                    <td>{!! $enrollment['completed'] !!}</td>

                                </tr>
                            @endforeach
                        @endif
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
@endsection
