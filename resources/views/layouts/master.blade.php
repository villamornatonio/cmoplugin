<!DOCTYPE html>
<html lang="en">


@include('partials/head')

<body id="app">


@include('partials/header')

<section>


    @include('partials/sidebar')

    <div class="mainpanel">
        @include('flash::message')
        @include('toast::messages')

        <!--<div class="pageheader">
          <h2><i class="fa fa-home"></i> Dashboard</h2>
        </div>-->


        @yield('content')

    </div><!-- mainpanel -->

</section>



@include('partials/footer')
@yield('js')
@yield('scripts')
@include('partials.notifications')

</body>
</html>








{{--<html lang="en">--}}

{{--@include('partials/head')--}}

{{--<body>--}}

{{--@include('partials/header')--}}

{{--<div class="container-fluid">--}}
    {{--<div class="row">--}}
        {{--@include('partials/sidebar')--}}

        {{--@yield('content')--}}

    {{--</div>--}}
{{--</div>--}}


{{--</body>--}}
{{--</html>--}}