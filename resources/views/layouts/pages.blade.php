<!DOCTYPE html>
<html lang="en-US">

@include('layouts.partials.pages.head')
<body class="home page page-id-3615 page-template page-template-template-alternative page-template-template-alternative-php _masterslider _msp_version_2.10.2 w1170 headerstyle1 wpb-js-composer js-comp-ver-4.4.3 vc_responsive">




<!-- Marine Conten Wrapper -->
<div id="marine-content-wrapper">
    @include('layouts.partials.pages.header')

    @yield('content')

    @include('layouts.partials.pages.footer')


</div>
<!-- /Marine Conten Wrapper -->




<div id="back-to-top">
    <a href="#"></a>
</div>


<div class="media_for_js"></div>
<div class="revsliderstyles">
    <style type="text/css">.tp-caption.big_extraheavy_60 {
            font-size: 82px;
            line-height: 60px;
            font-weight: 900;
            font-family: "Open Sans";
            color: rgb(255, 255, 255);
            text-decoration: none;
            text-shadow: rgb(8, 13, 16) 1px 2px 7px;
            background-color: transparent;
            border-width: 0px;
            border-color: rgb(0, 0, 0);
            border-style: none
        }

        .tp-caption.extrabold_littlesub_open {
            font-size: 14px;
            line-height: 22px;
            font-weight: 900;
            font-family: "Open Sans";
            color: #f8f8f8;
            text-decoration: none;
            background-color: transparent;
            border-width: 0px;
            border-color: rgb(34, 34, 34);
            border-style: none
        }

        .big_extraheavy_60 {
            font-size: 82px;
            line-height: 60px;
            font-weight: 900;
            font-family: "Open Sans";
            color: rgb(255, 255, 255);
            text-decoration: none;
            text-shadow: rgb(8, 13, 16) 1px 2px 7px;
            background-color: transparent;
            border-width: 0px;
            border-color: rgb(0, 0, 0);
            border-style: none
        }

        .extrabold_littlesub_open {
            font-size: 14px;
            line-height: 22px;
            font-weight: 900;
            font-family: "Open Sans";
            color: #f8f8f8;
            text-decoration: none;
            background-color: transparent;
            border-width: 0px;
            border-color: rgb(34, 34, 34);
            border-style: none
        }</style>
</div>

</body>
</html>