<!-- Footer -->
<footer id="footer">


    <!-- Main Footer -->
    <div id="main-footer" class="smallest-padding">
        <div class="container">
            <div class="row">


                <div class="col-lg-4 col-md-4 col-sm-4">

                    <div id="text-2" class="widget widget_text">
                        <div class="textwidget"><img
                                    src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/marine-logo-white.png"
                                    alt="logo">

                            <p>Vivamus orci sem, consectetur ut vestibulum a, mper ac dui. Aenean tellus nisl,
                                commodo eu aliquet ut, pulvinar ut sapien. Proin tate aliquam mi nec hendrerit. </p>
                        </div>
                    </div>
                </div>


                <div class="col-lg-4 col-md-4 col-sm-4">

                    <div id="text-4" class="widget widget_text"><h4>Contact</h4>

                        <div class="textwidget">
                            <ul class="iconic-list">

                                <li>
                                    <i class="icons icon-location-7"></i>
                                    {{ config('company.address') }}
                                </li>

                                <li>
                                    <i class="icons icon-mobile-6"></i>
                                    Phone: {{ config('company.phone') }} <br>
                                </li>

                                <li>
                                    <i class="icons icon-mail-7"></i>
                                    pntci@gmail.com
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-4">

                    <div id="wysija-3" class="widget widget_wysija"><h4>Subscribe to our Newsletter</h4>

                        <div class="widget_wysija_cont">
                            <div id="msg-form-wysija-3" class="wysija-msg ajax"></div>
                            <form id="form-wysija-3" method="post" action="#wysija" class="widget_wysija">
                                By subscribing to our mailing list you will get the latest news from us.

                                <p class="wysija-paragraph">
                                    <label>Email <span class="wysija-required">*</span></label>

                                    <input type="text" name="wysija[user][email]"
                                           class="wysija-input validate[required,custom[email]]" title="Email"
                                           value=""/>



    <span class="abs-req">
        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value=""/>
    </span>

                                </p>
                                <input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!"/>

                                <input type="hidden" name="form_id" value="1"/>
                                <input type="hidden" name="action" value="save"/>
                                <input type="hidden" name="controller" value="subscribers"/>
                                <input type="hidden" value="1" name="wysija-page"/>


                                <input type="hidden" name="wysija[user_list][list_ids]" value="1"/>

                            </form>
                        </div>
                    </div>
                    <div id="social-media-2" class="widget widget_social_media"><h4>Follow Us</h4>
                        <ul class="social-media">

                            <li class="tooltip-ontop" title="Facebook"><a href="dfsdfsdfscvc"><i
                                            class="icon-facebook"></i></a></li>
                            <li class="tooltip-ontop" title="Twitter"><a href="dfsder"><i class="icon-twitter"></i></a>
                            </li>
                            <li class="tooltip-ontop" title="Skype"><a href="fsdf"><i class="icon-skype"></i></a>
                            </li>
                            <li class="tooltip-ontop" title="Google Plus"><a href="sdfs"><i class="icon-google"></i></a>
                            </li>
                            <li class="tooltip-ontop" title="Vimeo"><a href="dfsdf"><i class="icon-vimeo"></i></a>
                            </li>
                            <li class="tooltip-ontop" title="Linkedin"><a href="fsdfsd"><i
                                            class="icon-linkedin"></i></a></li>
                            <li class="tooltip-ontop" title="Instagram"><a href="fsdfsdf"><i
                                            class="icon-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /Main Footer -->

    <!-- Lower Footer -->
    <div id="lower-footer">
        <div class="container">
            <span class="copyright">© {{ Carbon\Carbon::now()->format('Y') }} {{ config('company.shortName') }}. All Rights Reserved</span>
        </div>
    </div>
    <!-- /Lower Footer -->

</footer>
<!-- /Footer -->