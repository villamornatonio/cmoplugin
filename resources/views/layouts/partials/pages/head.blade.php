<head>
    <meta charset="UTF-8"/>
    <meta name='viewport'
          content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=0'>
    <title>{{ config('company.name') }}</title>
    <link rel="profile" href="http://gmpg.org/xfn/11"/>
    <link rel="pingback" href="http://marinewp.wpengine.com/xmlrpc.php"/>
    <link rel="shortcut icon"
          href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/marine_fav1.jpg"
          type="image/x-icon"/>
    <link rel="alternate" type="application/rss+xml" title="Marine &raquo; Feed"
          href="http://marinewp.wpengine.com/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Marine &raquo; Comments Feed"
          href="http://marinewp.wpengine.com/comments/feed/"/>
    <link rel="alternate" type="application/rss+xml" title="Marine &raquo; Main Comments Feed"
          href="http://marinewp.wpengine.com/main/feed/"/>

    <link rel='stylesheet' id='validate-engine-css-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/wysija-newsletters/css/validationEngine.jquery.css?ver=2.6.15'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='contact-form-7-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.1.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='orbit-montserrat-font-css'
          href='//fonts.googleapis.com/css?family=Montserrat%3A400%2C500%2C700' type='text/css' media='all'/>
    <link rel='stylesheet' id='orbit-open-sans-font-css' href='//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='orbit-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/orbit/css/orbit.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='bootstrap-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/orbit/css/orbit-bootstrap.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='font-awesome-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/js_composer/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.4.3'
          type='text/css' media='screen'/>
    <link rel='stylesheet' id='rs-plugin-settings-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/rs-plugin/css/settings.css?rev=4.6.0&#038;ver=4.5.2'
          type='text/css' media='all'/>
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out
        }

        .tp-caption a:hover {
            color: #ffa902
        }

        .largeredbtn {
            font-family: "Raleway", sans-serif;
            font-weight: 900;
            font-size: 16px;
            line-height: 60px;
            color: #fff !important;
            text-decoration: none;
            padding-left: 40px;
            padding-right: 80px;
            padding-top: 22px;
            padding-bottom: 22px;
            background: rgb(234, 91, 31);
            background: -moz-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(234, 91, 31, 1)), color-stop(100%, rgba(227, 58, 12, 1)));
            background: -webkit-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -o-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -ms-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: linear-gradient(to bottom, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ea5b1f', endColorstr='#e33a0c', GradientType=0)
        }

        .largeredbtn:hover {
            background: rgb(227, 58, 12);
            background: -moz-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(227, 58, 12, 1)), color-stop(100%, rgba(234, 91, 31, 1)));
            background: -webkit-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -o-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -ms-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: linear-gradient(to bottom, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e33a0c', endColorstr='#ea5b1f', GradientType=0)
        }

        .fullrounded img {
            -webkit-border-radius: 400px;
            -moz-border-radius: 400px;
            border-radius: 400px
        }
    </style>
    <link rel='stylesheet' id='tp-open-sans-css'
          href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tp-raleway-css'
          href='http://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tp-droid-serif-css'
          href='http://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=4.5.2' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='tp-lato-css'
          href='http://fonts.googleapis.com/css?family=Lato%3A100%2C300&#038;ver=4.5.2' type='text/css' media='all'/>
    <link rel='stylesheet' id='essential-grid-plugin-settings-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/essential-grid/public/assets/css/settings.css?ver=1.1.0'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='control-panel-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/framework/css/control-panel.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='twitter-bootstrap-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/bootstrap.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='fontello-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/fontello.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='prettyphoto-css-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/js/prettyphoto/css/prettyPhoto.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='animation-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/animation.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='flexSlider-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/flexslider.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='perfectscrollbar-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/perfect-scrollbar-0.4.10.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='nouislider-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/jquery.nouislider.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-validity-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/jquery.validity.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/jquery-ui.min.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='style-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/style.css'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='google-fonts-css'
          href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='custom-style-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/style.css?ver=4.5.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ms-main-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/masterslider/public/assets/css/masterslider.main.css?ver=2.10.2'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ms-custom-css'
          href='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/masterslider/custom.css?ver=2.6'
          type='text/css' media='all'/>
    <script type='text/javascript'
            src='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-includes/js/jquery/jquery.js?ver=1.12.3'></script>
    <script type='text/javascript'
            src='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.0'></script>
    <script type='text/javascript'
            src='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.tools.min.js?rev=4.6.0&#038;ver=4.5.2'></script>
    <script type='text/javascript'
            src='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/rs-plugin/js/jquery.themepunch.revolution.min.js?rev=4.6.0&#038;ver=4.5.2'></script>
    <script type='text/javascript'
            src='http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.essential.min.js?ver=1.1.0'></script>
    <script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=false&#038;ver=4.5.2'></script>
    <link rel='https://api.w.org/' href='http://marinewp.wpengine.com/wp-json/'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://marinewp.wpengine.com/xmlrpc.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml"
          href="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-includes/wlwmanifest.xml"/>
    <link rel="canonical" href="http://marinewp.wpengine.com/"/>
    <link rel='shortlink' href='http://marinewp.wpengine.com/'/>
    <link rel="alternate" type="application/json+oembed"
          href="http://marinewp.wpengine.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmarinewp.wpengine.com%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="http://marinewp.wpengine.com/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fmarinewp.wpengine.com%2F&#038;format=xml"/>
    <script>var ms_grabbing_curosr = 'http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/masterslider/public/assets/css/common/grabbing.cur', ms_grab_curosr = 'http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/masterslider/public/assets/css/common/grab.cur';</script>
    <meta name="generator" content="MasterSlider 2.10.2 - Responsive Touch Image Slider"/>
    <link media="screen" type="text/css" rel="stylesheet"
          href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/wp-gmappity-easy-google-maps/styles/wpgmappity-post-styles.css"/>
    <!--[if lt IE 9]>
    <script>document.createElement("header");
    document.createElement("nav");
    document.createElement("section");
    document.createElement("article");
    document.createElement("aside");
    document.createElement("footer");
    document.createElement("hgroup");</script><![endif]--><!--[if lt IE 9]>
    <script src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/js/html5.js"></script>
    <![endif]--><!--[if lt IE 7]>
    <script src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/js/icomoon.js"></script>
    <![endif]--><!--[if lt IE 9]>
    <link href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/ie.css"
          rel="stylesheet"><![endif]-->
    <!--[if lt IE 9]>
    <script src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/js/jquery.placeholder.js"></script>
    <script src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/js/script_ie.js"></script>
    <![endif]-->
    <!-- Preventing FOUC -->
    <style>
        .no-fouc {
            display: none;
        }
    </style>

    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress."/>
    <!--[if IE 8]>
    <link rel="stylesheet" type="text/css"
          href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/js_composer/assets/css/vc-ie8.css"
          media="screen"><![endif]-->
    <link rel="stylesheet"
          href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/css/mobilenav.css"
          media="screen and (max-width: 820px)">
    <noscript>
        <style> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>