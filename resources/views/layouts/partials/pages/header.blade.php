<!-- Header -->
<header id="header" class="style1">


    <!-- Main Header -->
    <div id="main-header">

        <div class="container">

            <div class="row">

                <!-- Logo -->
                <div class="col-lg-4 col-md-4 col-sm-4 logo">
                    <a href='http://marinewp.wpengine.com/' title="Marine" rel="home"><img class="logo"
                                                                                           src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/img/marine-logo.png"
                                                                                           alt="Marine"></a>

                    <div id="main-nav-button">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>

                <div class="col-sm-8 align-right">
                    <!-- Text List -->
                    <ul class="text-list">
                        <li>Call Us: {{ config('company.phone') }}</li>
                    </ul>

                    <!-- Social Media -->

                    <ul class="social-media">


                        <li><a target="_blank" href="http://facebook.com"><i class="icon-facebook"></i></a>

                        </li>


                        <li><a target="_blank" href="http://twiiter.com/google"><i class="icon-twitter"></i></a>

                        </li>


                        <li><a target="_blank" href="http://google.com"><i class="icon-google"></i></a>

                        </li>


                        <li><a target="_blank" href="https://www.youtube.com"><i class="icon-vimeo"></i></a>

                        </li>


                        <li><a target="_blank" href="http://linkedin.com"><i class="icon-linkedin"></i></a>

                        </li>


                        <li><a target="_blank" href="http://instagram.com"><i class="icon-instagram"></i></a>

                        </li>


                    </ul>
                </div>

            </div>

        </div>

    </div>
    <!-- /Main Header -->


    <!-- Lower Header -->
    <div id="lower-header">

        <div class="container">

            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">

                    <div class="lower-logo">
                        <a href='http://marinewp.wpengine.com/' title="Marine" rel="home"><img class="logo"
                                                                                               src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/themes/marine/img/marine-logo.png"
                                                                                               alt="Marine"></a>
                    </div>

                    <!-- Main Navigation -->
                    <ul id="main-nav" class="menu">
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/')  }}">Home</a></li>
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/about-us')  }}">About Us</a></li>
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/team')  }}">Team</a></li>
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/facilities')  }}">Facilities</a></li>
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/courses')  }}">Courses</a></li>
                        <li id="menu-item-4707"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4707"><a
                                    href="{{ url('/contact-us')  }}">Contact Us</a></li>
                    </ul>
                    <!-- /Main Navigation -->

                </div>

            </div>

        </div>

    </div>
    <!-- /Lower Header -->


</header>
<!-- /Header -->