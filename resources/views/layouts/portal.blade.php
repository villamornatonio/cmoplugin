<!DOCTYPE html>
<html lang="en">
@include('partials.head')

<body id="app" class="signwrapper">

<div class="sign-overlay"></div>
<div class="signpanel"></div>
<div class="container">
    @yield('content')
</div>
@include('partials/footer')
@yield('js')
@yield('scripts')
@include('partials.notifications')
</body>
</html>
