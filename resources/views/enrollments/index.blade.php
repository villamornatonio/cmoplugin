@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('enrollments') }}}">Enrollments</a></li>
            <li class="active">Display Enrollments List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Enrollment List</h4>

                <p>Manage Enrollment</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>TRAINEE</th>
                            <th>COURSE</th>
                            <th>PRICE</th>
                            <th>ROOM</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th>COMPLETED</th>
                            <th>COMPANY CHARGED</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/enrollments',
                "columnDefs": [{
                    "targets": 12,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'>" +
                                "<li>" +
                                "<a href='/enrollments/" + row['id'] + "/edit" + "'>" +
                                "<i class='fa fa-pencil'></i>" +
                                "</a>" +
                                "</li>" +
                                "<li>" +
                                "<a  data-url='/enrollments/" + row['id'] + "' data-method='delete' class='btn' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                "<i class='fa fa-trash'></i>" +
                                "</a>" +
                                "</li>" +
                                "</ul>"
                        return actions;
                    }
                }],
                columns: [
                    {data: 'id', name: 'enrollments.id'},
                    {data: 'trainee', name: 'trainee'},
                    {data: 'course', name: 'courses.name'},
                    {data: 'price', name: 'courses.price'},
                    {data: 'room', name: 'rooms.name'},
                    {data: 'startDate', name: 'start_date'},
                    {data: 'endDate', name: 'end_date'},
                    {data: 'startTime', name: 'start_time'},
                    {data: 'endTime', name: 'end_time'},
                    {data: 'completed', name: 'is_completed'},
                    {data: 'companyCharge', name: 'is_company_charged'},
                    {data: 'author', name: 'author'},
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });
    </script>
@endsection
