@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('enrollments') }}}">Enrollment</a></li>
            <li class="active">Add New Enrollment</li>
        </ol>

        @if(isset($trainee))
            <div class="panel panel-profile list-view">
                <div class="panel-heading">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-circle" src="/images/photos/user-default.png" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $trainee['name'] }}}</h4>

                            <p class="media-usermeta"><i
                                        class="glyphicon glyphicon-briefcase"></i>{{ $trainee['position'] }}</p>
                        </div>
                    </div>
                    <!-- media -->
                    <ul class="panel-options">
                        <li><a class="tooltips" href="" data-toggle="tooltip" title="View Options"><i
                                        class="glyphicon glyphicon-option-vertical"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body people-info">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Date of Birt</label>
                                {{{ $trainee['dob'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Email</label>
                                <i class="fa fa-envelope mr5"></i> {{{ $trainee['email'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Phone</label>
                                {{{ $trainee['mobile'] }}}
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Passport</label>
                                {{{ $trainee['passport_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Seamans Book</label>
                                {{{ $trainee['seamans_book_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Emergency Contact Person</label>

                                <div class="social-account-list">
                                    {{{ $trainee['emergency_person'] }}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div>
            </div><!-- panel -->
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">COURSE SCHEDULE : {{{ strtoupper($schedule->course) }}}</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="col-md-6"><strong>START DATE</strong></div>
                                <div class="col-md-6">{{{ Carbon\Carbon::parse($schedule->start_date)->toFormattedDateString() }}}</div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-6"><strong>END DATE</strong></div>
                                <div class="col-md-6">{{{ Carbon\Carbon::parse($schedule->end_date)->toFormattedDateString() }}}</div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-6"><strong>START TIME</strong></div>
                                <div class="col-md-6">{{{ Carbon\Carbon::parse($schedule->start_time)->toTimeString() }}}</div>
                            </div>
                            <div class="col-md-3">
                                <div class="col-md-6"><strong>END TIME</strong></div>
                                <div class="col-md-6">{{{ Carbon\Carbon::parse($schedule->end_time)->toTimeString() }}}</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- panel -->
            </div>


        </div>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Add New Enrollment</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'enrollments',
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}
                        {!! Form::hidden('schedule_id',$schedule->id) !!}
                        {!! Form::hidden('trainee_id',$trainee['id']) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label"></span>

                            </label>

                            <div class="col-sm-8">
                                <label class="ckbox ckbox-primary">
                                    {!! Form::checkbox('is_company_charged',null,['checked' => true,'required' => true])
                                    !!}
                                    <span>Company Charged ?</span>
                                </label>
                            </div>

                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company</label>

                            <div class="col-sm-8">
                                {!! Form::text('company',null,['class' => 'form-control' ,'placeholder' => 'Company
                                Name']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Referred by <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::select('referrer_id',$referrers,null,['class' => 'form-control' , 'placeholder' => 'Select a Referrer']) !!}
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection