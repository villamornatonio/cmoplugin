@extends('layouts.portal')
@section('content')
    <div class="row" id="login-container">
        <div class="row">
            <div class="signin col-md-12">
                <div class="panel-heading">
                    <h1>{{{ config('company.shortName') }}}</h1>
                    <h4 class="panel-title">{{{ config('company.name') }}}</h4>
                </div>
                <div class="panel-body">
                    {!! Form::open([
                    'url' => 'auth/login',
                    'method' => 'POST',
                    'class' => 'form-horizontal'

                    ]) !!}
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            {!! Form::email('email',null,['class' => 'form-control' , 'placeholder' => 'Enter Email',
                            'required' => true]) !!}
                        </div>
                    </div>
                    <div class="form-group nomargin">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            {!! Form::password('password',['class' => 'form-control' , 'placeholder' => 'Enter Password'
                            , 'required' => true]) !!}
                        </div>
                    </div>
                    <div><a href="" class="forgot">Forgot password?</a></div>
                    <div class="form-group">
                        <button class="btn btn-success btn-quirk btn-block">Login</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- panel -->
        </div>
    </div>
@endsection