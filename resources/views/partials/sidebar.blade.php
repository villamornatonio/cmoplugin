<div class="leftpanel">
    <div class="leftpanelinner">

        <!-- ################## LEFT PANEL PROFILE ################## -->

        <div class="media leftpanel-profile">
            <div class="media-left">
                <a href="#">
                    <img src="{{ '/' . config()->get('uploads.user') . auth()->user()->avatar }}" alt=""
                         class="media-object img-circle">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">{{{ auth()->user()->name() }}} <a data-toggle="collapse"
                                                                            data-target="#loguserinfo"
                                                                            class="pull-right"><i
                                class="fa fa-angle-down"></i></a></h4>
                <span>{{{ auth()->user()->role->label }}}</span>
            </div>
        </div>
        <!-- leftpanel-profile -->

        <div class="leftpanel-userinfo collapse" id="loguserinfo">
            {{--<h5 class="sidebar-title">Address</h5>--}}
            <h5 class="sidebar-title">Contact</h5>
            <ul class="list-group">
                <li class="list-group-item">
                    <label class="pull-left">Email</label>
                    <span class="pull-right">{{{ auth()->user()->email }}}</span>
                </li>
                <li class="list-group-item">
                    <label class="pull-left">Mobile</label>
                    <span class="pull-right">{{{ auth()->user()->contact_no }}}</span>
                </li>
                <li class="list-group-item">
                    <label class="pull-left">Social</label>

                    <div class="social-icons pull-right">
                        <a href="#"><i class="fa fa-facebook-official"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                </li>
            </ul>
        </div>
        <!-- leftpanel-userinfo -->

        <ul class="nav nav-tabs nav-justified nav-sidebar">
            <li class="tooltips active" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab"
                                                                                   data-target="#mainmenu"><i
                            class="tooltips fa fa-th-large"></i></a></li>

            <li class="tooltips" data-toggle="tooltip" title="Log Out"><a href="{{{ url('auth/logout') }}}"><i
                            class="fa fa-sign-out"></i></a></li>
        </ul>

        <div class="tab-content">

            <!-- ################# MAIN MENU ################### -->

            <div class="tab-pane active" id="mainmenu">

                <h5 class="sidebar-title">Navigation</h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    @if(in_array(auth()->user()->role->name,['admin','finance']))
                        <li class="nav-parent {{ set_active(['reports*']) }}">
                            <a href=""><i class="fa fa-bar-chart-o"></i> <span>Dashboard</span></a>
                            <ul class="children">
                                <li><a href="{{{ url('reports/daily') }}}">Daily Reports</a></li>
                                <li><a href="{{{ url('reports/weekly') }}}">Weekly Reports</a></li>
                                <li><a href="{{{ url('reports/monthly') }}}">Monthly Reports</a></li>

                            </ul>
                        </li>
                    @endif
                    @if(in_array(auth()->user()->role->name,['admin']))
                        <li class="nav-parent {{ set_active(['users*']) }}">
                            <a href=""><i class="fa fa-user-plus"></i> <span>Users</span></a>
                            <ul class="children">
                                <li><a href="{{{ url('users') }}}">View All Users</a></li>
                                <li><a href="{{{ url('users/create') }}}">Add New User</a></li>
                            </ul>
                        </li>

                        <li class="nav-parent {{ set_active(['orders*']) }}">
                            <a href=""><i class="fa fa-list"></i> <span>Orders</span></a>
                            <ul class="children">
                                <li><a href="{{{ url('orders') }}}">View All Orders</a></li>
                                <li><a href="{{{ url('orders/import') }}}">Order Fulfillments</a></li>
                                <li><a href="{{{ url('orders/export') }}}">Export to CMO Orders List</a></li>
                            </ul>
                        </li>
                    @endif


                    <li class="nav-parent {{ set_active(['config*']) }}">
                        <a href=""><i class="fa fa-cogs"></i> <span>Configuration</span></a>
                        <ul class="children">
                            <li><a href="{{{ url('config/create') }}}">Configuration</a></li>
                        </ul>
                    </li>

                </ul>
            </div>
            <!-- tab-pane -->

        </div>
        <!-- tab-content -->

    </div>
    <!-- leftpanelinner -->
</div><!-- leftpanel -->