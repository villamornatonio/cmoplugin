<header>
    <div class="headerpanel">

        <div class="logopanel">
            <h2><a href="{{{ url('/') }}}">{{ session('company.short_name') }}</a></h2>
        </div>
        <!-- logopanel -->

        <div class="headerbar">

            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

            <div class="searchpanel">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
          </span>
                </div>
                <!-- input-group -->
            </div>

            <div class="header-right">
                <ul class="headermenu">
                    <li>
                        <div id="noticePanel" class="btn-group">

                            <div id="noticeDropdown" class="dropdown-menu dm-notice pull-right">
                                <div role="tabpanel">
                                    <!-- Nav tabs -->

                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="btn-group">
                            <button type="button" class="btn btn-logged" data-toggle="dropdown">
                                <img src="{{ '/' . config()->get('uploads.user') . auth()->user()->avatar }}" alt=""/>
                                {{{ auth()->user()->name() }}}
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{{ url('profiles') }}}"><i class="glyphicon glyphicon-user"></i> My
                                        Profile</a></li>
                                {{--<li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>--}}
                                <li><a href="{{{ url('help') }}}"><i class="glyphicon glyphicon-question-sign"></i> Help</a>
                                </li>
                                <li><a href="{{{ url('auth/logout') }}}"><i class="glyphicon glyphicon-log-out"></i> Log
                                        Out</a></li>
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- header-right -->
        </div>
        <!-- headerbar -->

    </div>
    <!-- header-->
</header>


