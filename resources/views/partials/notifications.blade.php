<script type="text/javascript">

    $(document).ready(function () {


        @if(session()->has('notification'))

            @if(session()->get('notification')['status'] == 'success')
            $.gritter.add({
                title: 'Success',
                text: "{{{ session()->get('notification')['message'] }}}",
                class_name: 'with-icon question-circle success'
            });
            @endif

            @if(session()->get('notification')['status'] == 'info')
            $.gritter.add({
                title: 'Information',
                text: "{{{ session()->get('notification')['message'] }}}",
                class_name: 'with-icon question-circle primary'
            });
            @endif

            @if(session()->get('notification')['status'] == 'warning')
            $.gritter.add({
                title: 'Warning',
                text: "{{{ session()->get('notification')['message'] }}}",
                class_name: 'with-icon exclamation-circle warning'
            });
            @endif

            @if(session()->get('notification')['status'] == 'danger')
            $.gritter.add({
                title: 'Error',
                text: "{{{ session()->get('notification')['message'] }}}",
                class_name: 'with-icon times-circle danger'
            });
            @endif
        @endif

        @if($errors->has())
            @foreach($errors->all() as $error)
            $.gritter.add({
                title: 'Error',
                text: "{{{ $error }}}",
                class_name: 'with-icon times-circle danger'
            });

            @endforeach

        @endif


    });


    $('#flash-overlay-modal').modal();


</script>
