@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('schedules') }}}">Schedules</a></li>
            <li class="active">Display Schedule List</li>
        </ol>

        @if(isset($trainee))
            <div class="panel panel-profile list-view">
                <div class="panel-heading">
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-circle" src="/images/photos/user-default.png" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{{ $trainee['name'] }}}</h4>

                            <p class="media-usermeta"><i
                                        class="glyphicon glyphicon-briefcase"></i>{{ $trainee['position'] }}</p>
                        </div>
                    </div>
                    <!-- media -->
                    <ul class="panel-options">
                        <li><a class="tooltips" href="" data-toggle="tooltip" title="View Options"><i
                                        class="glyphicon glyphicon-option-vertical"></i></a></li>
                    </ul>
                </div>
                <div class="panel-body people-info">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Date of Birt</label>
                                {{{ $trainee['dob'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Email</label>
                                <i class="fa fa-envelope mr5"></i> {{{ $trainee['email'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Phone</label>
                                {{{ $trainee['mobile'] }}}
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Passport</label>
                                {{{ $trainee['passport_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Seamans Book</label>
                                {{{ $trainee['seamans_book_no'] }}}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="info-group">
                                <label>Emergency Contact Person</label>

                                <div class="social-account-list">
                                    {{{ $trainee['emergency_person'] }}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- row -->
                </div>
            </div><!-- panel -->
        @endif

        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Schedule List</h4>

                <p>Manage Schedules</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>COURSE</th>
                            <th>PRICE</th>
                            <th>ROOM</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/schedules',
                "columnDefs": [{
                    "targets": 8,
                    render: function (data, type, row) {
                        ;
                        var actions = "<ul class='table-options'>" +
                                @if(isset($trainee))
                                "</li>" +
                                "<li>" +
                                "<a class='enroll' href='/enrollments/" + row['id'] + "/trainee/{{{ $trainee_id }}}/create'><i class='fa fa-plus-square'></i>" +
                                "</a>" +
                                "</li>" +
                                @endif
                                "<li><a href='/schedules/" + row['id']  + "'><i class='fa fa-users'></i></a></li>" +
                                "<li><a href='/schedules/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li>" +
                                "<li>" +
                                "<a  data-url='/schedules/" + row['id'] + "' data-method='delete' class='btn' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                "<i class='fa fa-trash'></i>" +
                                "</a>" +
                                "</li>" +
                                "</ul>"
                        return actions;
                    }
                }],
                columns: [
                    {data: 'id', name: 'schedules.id'},
                    {data: 'course', name: 'courses.name'},
                    {data: 'price', name: 'courses.price'},
                    {data: 'room', name: 'rooms.name'},
                    {data: 'startDate', name: 'schedules.start_date'},
                    {data: 'endDate', name: 'schedules.end_date'},
                    {data: 'startTime', name: 'schedules.start_time'},
                    {data: 'endTime', name: 'schedules.end_time'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }


            });

        });
    </script>
@endsection
