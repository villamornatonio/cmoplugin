@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('schedules') }}}">Schedules</a></li>
            <li class="active">Add New Schedule</li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Add New Schedule</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'schedules',
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Room <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::select('room_id',$rooms,null ,['class' => 'form-control', 'required' =>
                                true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Course <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::select('course_id',$courses,null ,['class' => 'form-control', 'required' =>
                                true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Start Date <span class="text-danger">*</span></label>

                            <div class="input-group col-sm-8">
                                {!! Form::text('start_date',null,['class' => 'form-control' ,'placeholder' =>
                                'yyyy-mm-dd', 'id' => 'start_date',
                                'required' => true]) !!}
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Start Date <span class="text-danger">*</span></label>

                            <div class="input-group col-sm-8">
                                {!! Form::text('end_date',null,['class' => 'form-control' ,'placeholder' =>
                                'yyyy-mm-dd', 'id' => 'end_date',
                                'required' => true]) !!}
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Start Time <span class="text-danger">*</span></label>

                            <div class="input-group col-sm-8">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                <div class="timepicker">
                                    {!! Form::text('start_time',null,['class' => 'form-control' ,'placeholder' =>
                                    '', 'id' => 'start_time',
                                    'required' => true]) !!}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">End Time <span class="text-danger">*</span></label>

                            <div class="input-group col-sm-8">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-time"></i></span>
                                <div class="timepicker">
                                    {!! Form::text('end_time',null,['class' => 'form-control' ,'placeholder' =>
                                    '', 'id' => 'end_time',
                                    'required' => true]) !!}
                                </div>
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Add</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
            $('#start_date').datepicker({numberOfMonths: 2 , dateFormat: 'yy-mm-dd' });
            $('#end_date').datepicker({numberOfMonths: 2 , dateFormat: 'yy-mm-dd'});
            $('#start_time').timepicker();
            $('#end_time').timepicker();
        });

    </script>
@endsection
