@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('schedules') }}}">Schedules</a></li>
            <li class="active">Add New Schedule</li>
        </ol>

        <div class="row">

            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">COURSE : {{ $course }}</h4>

                    <p>Schedule : {{ $schedule }}</p>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        @if(isset($enrollees))

                            <table class="table nomargin">
                                <thead>
                                <tr>

                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Passport</th>
                                    <th>Seamans Book</th>
                                    <th>Mobile</th>
                                    <th>Email</th>
                                    <th>Date of Birth</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($enrollees as $key => $enrollee)

                                    <tr>
                                        <td>{{ $enrollee['id'] }}</td>
                                        <td>{{ $enrollee['name'] }}</td>
                                        <td>{{ $enrollee['position'] }}</td>
                                        <td>{{ $enrollee['passport_no'] }}</td>
                                        <td>{{ $enrollee['seamans_book_no'] }}</td>
                                        <td>{{ $enrollee['mobile'] }}</td>
                                        <td>{{ $enrollee['email'] }}</td>
                                        <td>{{ $enrollee['dob'] }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                    <!-- table-responsive -->
                </div>
            </div>

        </div>
        <!-- col-md-6 -->

    </div>
    <!--row -->

    </div><!-- contentpanel -->
@endsection
@section('scripts')
    <script type="text/javascript">
        $(function () {
//            $('#start_date').datepicker({numberOfMonths: 2 , dateFormat: 'yy-mm-dd' });
//            $('#end_date').datepicker({numberOfMonths: 2 , dateFormat: 'yy-mm-dd'});
//            $('#start_time').timepicker();
//            $('#end_time').timepicker();
        });

    </script>
@endsection
