@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('payments') }}}">Payments</a></li>
            <li class="active">Display Payments List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Payments List</h4>

                <p>Manage Payments</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>FROM</th>
                            <th>AMOUNT</th>
                            <th>BALANCE</th>
                            <th>PRICE</th>
                            <th>DISCOUNT</th>
                            <th>OR NO</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/payments',
                "columnDefs": [ {
                    "targets": 8,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'><li><a href='/receipts/" + row['orNo'] + "/full" + "'><i class='fa fa-print'></i></a></li>" +
                                "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'payments.id'},
                    {data: 'trainee', name: 'trainee'},
                    {data: 'amount', name: 'payments.amount'},
                    {data: 'remainingBalance', name: 'payments.remaining_balance'},
                    {data: 'price', name: 'payments.price'},
                    {data: 'discount', name: 'payments.discount'},
                    {data: 'orNo', name: 'payments.or_no'},
                    {data: 'author', name: 'author'},
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });
    </script>
@endsection