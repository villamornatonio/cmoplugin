@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('payments') }}}">Pending</a></li>
            <li class="active">Display Pending Payment List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Pending Payment List</h4>

                <p>Manage Pending Payments</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>TRAINEE</th>
                            <th>COURSE</th>
                            <th>PRICE</th>
                            <th>COMPLETED</th>
                            <th>COMPANY CHARGED</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/payments/pending',
                "columnDefs": [ {
                    "targets": 7,
                    render: function (data, type, row) {
                        console.log(row);
                        var actions = "<ul class='table-options'><li><a href='/payments/" + row['id'] + "/create" + "'><i class='fa fa-money'></i></a></li><li><a class='delete' href='#'><i class='fa fa-trash'></i></a></li></ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'trainees.id'},
                    {data: 'trainee', name: 'trainee'},
                    {data: 'course', name: 'courses.name'},
                    {data: 'price', name: 'courses.price'},
                    {data: 'completed', name: 'is_completed'},
                    {data: 'companyCharge', name: 'is_company_charged'},
                    {data: 'author', name: 'author'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });
    </script>
@endsection
