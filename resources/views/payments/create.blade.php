@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('courses') }}}">Courses</a></li>
            <li class="active">Add New Course</li>
        </ol>


        <div class="row">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">Enrolled Course Schedule: {!! isset($enrollments[0]['trainee']) ? $enrollments[0]['trainee'] : null !!}</h4>

                        <p>Courses</p>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-primary nomargin">
                                <thead>

                                <tr>
                                    <th>Name</th>
                                    <th class="text-center">Course</th>
                                    <th class="text-right">Room</th>
                                    <th class="text-right">Start Date</th>
                                    <th class="text-right">End Date</th>
                                    <th class="text-right">Start Time</th>
                                    <th class="text-right">End Time</th>
                                    <th class="text-right">Price</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($enrollments))
                                    @foreach($enrollments as $enrollment)
                                        <tr>
                                            <td>{!! $enrollment['trainee'] !!}</td>
                                            <td class="text-center">{!! $enrollment['course'] !!}</td>
                                            <td class="text-center">{!! $enrollment['room'] !!}</td>
                                            <td class="text-right">{!! $enrollment['startDate'] !!}</td>
                                            <td class="text-right">{!! $enrollment['endDate'] !!}</td>
                                            <td class="text-right">{!! $enrollment['startTime'] !!}</td>
                                            <td class="text-right">{!! $enrollment['endTime'] !!}</td>
                                            <td class="text-right">{!! $enrollment['price'] !!}</td>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td class="text-center"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right"></td>
                                        <td class="text-right text-danger"><strong>TOTAL AMOUNT</strong></td>
                                        <td class="text-right"><strong>{!! $totalAmount !!}</strong></td>
                                    </tr>

                                </tbody>
                                @endif
                            </table>
                        </div>
                        <!-- table-responsive -->
                    </div>
                </div>
                <!-- panel -->
            </div>
        </div>


        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Proceess Course Payment</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'payments',
                        'method' => 'POST',
                        'class' => 'form-horizontal'

                        ]) !!}
                        {!! Form::hidden('trainee_id',$trainee_id) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Amount<span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('amount',null,['class' => 'form-control' ,
                                'required' => true]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Discount <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('discount',0,['class' => 'form-control' ,
                                'required' => true]) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Offical Receipt No. <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('or_no',null,['class' => 'form-control',
                                'required' => true]) !!}
                            </div>
                        </div>
                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Proceess</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection