@extends('layouts.master')
@section('content')
    <div class="contentpanel">


        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('trainings/completed') }}}">Completed Trainees</a></li>
            <li class="active">Display Completed Training List</li>
        </ol>

        <div class="row" v-show="displayFormFields">

            <div class="col-md-12">
                <div class="panel">
                    <ul class="panel-options">
                        <li><a @click.prevent="hideFormFields"><i class="fa fa-remove"></i></a></li>
                    </ul>
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">@{{ formTitle }}</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        <form method="POST" action="http://admin.pmfi.com/rooms" accept-charset="UTF-8"
                              class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-3 control-label">Room Name <span
                                            class="text-danger">*</span></label>

                                <div class="col-sm-8">
                                    <input v-model="item.name" class="form-control" placeholder="example: 209"
                                           required="1" name="name"
                                           type="text">
                                </div>
                            </div>


                            <hr>

                            <div class="row">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button v-if="isCreate" @click.prevent="storeItem"
                                            class="btn btn-success btn-quirk btn-wide mr5">
                                        Add
                                    </button>
                                    <button v-else @click.prevent="updateItem('12')"
                                            class="btn btn-success btn-quirk btn-wide mr5">
                                        Update
                                    </button>
                                    <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Rooms List</h4>

                <p>Manage Rooms</p>
            </div>
            <div class="panel-heading">
                <button v-show="isCreate" @click="create" class="btn btn-success btn-quirk btn-wide mr5">Create</button>
                <button v-if="selectedItems.length > 0" @click="bulkActionDelete" class="
                btn btn-danger btn-quirk btn-wide mr5">Delete</button>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th></th>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->
        @{{ $data | json }}
    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script type="text/javascript">

        Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
        Vue.http.interceptors.push({
            request: function (request) {
                return request;
            },
            response: function (response) {
                return response;
            }
        });

        var vm = new Vue({
            el: '#app',
            data: {
                data: [],
                item: [],
                selectedItems: [],
                displayFormFields: false,
                formTitle: '',
                formAction: 'post',
                isCreate: true
            },
            ready: function () {
                this.fetchData()
            },
            methods: {
                create: function () {
                    this.formTitle = 'Add a New Room';
                    this.displayFormFields = true;
                    this.formAction = 'post';
                    this.item = {
                        name: ''
                    };
                },
                hideFormFields: function () {
                    this.displayFormFields = false;
                },
                fetchData: function () {

                    $('#data-display').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: '/api/v1/rooms?length=10',
                        "columnDefs": [{
                            "targets": 4,
                            render: function (data, type, row) {
                                var actions = "<ul class='table-options'><li><a class='datatables-edit-action' data-id='" + row['id'] + "'><i class='fa fa-pencil'></i></a></li>" +
                                        "<li>" +
                                        "<a  data-url='/rooms/" + row['id'] + "' data-id='" + row['id'] + "' data-method='delete' class='btn datatables-delete-action' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                        "<i class='fa fa-trash'></i>" +
                                        "</a>" +
                                        "</li>" +
                                        "</ul>";
                                return actions;
                            }
                        },
                            {
                                "targets": 0,
                                render: function (data, type, row) {
                                    var actions = "<label class='ckbox ckbox-primary'>" +
                                            "<input type='checkbox' class='toggle-select-action' value='" + row['id'] + "'><span></span>" +
                                            "</label>";
                                    return actions;
                                }
                            }
                        ],
                        columns: [
                            {},
                            {data: 'id', name: 'rooms.id'},
                            {data: 'name', name: 'rooms.name'},
                            {data: 'author', name: 'author'}
                        ],
                        "order": [[0, "desc"]],
                        initComplete: function () {
                            this.api().columns().every(function () {
                                var column = this;
                                var input = document.createElement("input");
                                $(input).appendTo($(column.footer()).empty())
                                        .on('change', function () {
                                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                            column.search(val ? val : '', true, false).draw();
                                        });
                            });
                        }


                    });
                },
                fetchItem: function (id) {
                    this.displayFormFields = true;
                    this.formTitle = 'Edit Room';
                    this.formAction = 'put';
                    this.isCreate = false;
                    var resource = this.$resource('api/v1/rooms{/id}');
                    resource.get({id: id}).then(function (response) {
                        this.$set('item', response.data.data)
                    }, function (error) {
                        for (var key in error.data) {
                            $.gritter.add({
                                title: 'Error',
                                text: error.data[key],
                                class_name: 'with-icon times-circle danger'
                            });
                        }
                    });
                },
                storeItem: function () {
                    var payloadData = {
                        name: this.item.name
                    };
                    var resource = this.$resource('api/v1/rooms{/id}');
                    resource.save(payloadData).then(function (response) {
                        $('#data-display').dataTable()._fnAjaxUpdate();
                        $.gritter.add({
                            title: 'Success',
                            text: response.data.message,
                            class_name: 'with-icon question-circle success'
                        });
                        // RESET VALUES
                        this.item.name = '';
                    }, function (error) {
                        for (var key in error.data) {
                            $.gritter.add({
                                title: 'Error',
                                text: error.data[key],
                                class_name: 'with-icon times-circle danger'
                            });
                        }
                    });
                },
                updateItem: function (id) {
                    var payloadData = {
                        name: this.item.name
                    };
                    var resource = this.$resource('api/v1/rooms{/id}');
                    resource.update(payloadData).then(function (response) {
                        $.gritter.add({
                            title: 'Success',
                            text: response.data.message,
                            class_name: 'with-icon question-circle success'
                        });
                    }, function (error) {
                        for (var key in error.data) {
                            $.gritter.add({
                                title: 'Error',
                                text: error.data[key],
                                class_name: 'with-icon times-circle danger'
                            });
                        }
                    });
                },
                destroyItem: function (id) {
                    var resource = this.$resource('api/v1/rooms{/id}');
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to Delete item ' + id + ' !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Proceed to Delete!',
                        cancelButtonText: 'Cancel Delete!',
                        confirmButtonClass: 'confirm-class',
                        cancelButtonClass: 'cancel-class',
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            resource.delete({id: id}).then(function (response) {
                                $('#data-display').dataTable()._fnAjaxUpdate();
                                $.gritter.add({
                                    title: 'Success',
                                    text: response.data.message,
                                    class_name: 'with-icon question-circle success'
                                });

                            }, function (error) {
                                for (var key in error.data) {
                                    $.gritter.add({
                                        title: 'Error',
                                        text: error.data[key],
                                        class_name: 'with-icon times-circle danger'
                                    });
                                }
                            });

                        } else {
                            $.gritter.add({
                                title: 'Cancelled',
                                text: 'Delete Cancelled!',
                                class_name: 'with-icon question-circle warning'
                            });
                        }
                    });

                },
                bulkActionDelete: function () {
                    var resource = this.$resource('api/v1/rooms/bulk/delete{/id}');
                    swal({
                        title: 'Are you sure?',
                        text: 'You want to Delete item !',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Proceed to Delete!',
                        cancelButtonText: 'Cancel Delete!',
                        confirmButtonClass: 'confirm-class',
                        cancelButtonClass: 'cancel-class',
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            resource.delete({id: vm.selectedItems}).then(function (response) {
                                $('#data-display').dataTable()._fnAjaxUpdate();
                                vm.selectedItems = [];
                                $.gritter.add({
                                    title: 'Success',
                                    text: response.data.message,
                                    class_name: 'with-icon question-circle success'
                                });

                            }, function (error) {
                                for (var key in error.data) {
                                    $.gritter.add({
                                        title: 'Error',
                                        text: error.data[key],
                                        class_name: 'with-icon times-circle danger'
                                    });
                                }
                            });

                        } else {
                            $.gritter.add({
                                title: 'Cancelled',
                                text: 'Delete Cancelled!',
                                class_name: 'with-icon question-circle warning'
                            });
                        }
                    });
                },
                toggleSelectedItem: function (id) {
                    var index = this.selectedItems.indexOf(id);
                    if (index === -1) {
                        this.selectedItems.push(id);
                    } else {
                        this.selectedItems.splice(index, 1);
                    }

                }
            }
        });

        // HANDLE DYNAMIC TABLE ELEMENTS NOT CREATED BY VUEJS
        $(document).on('click', '.datatables-delete-action', function () {
            var id = $(this).attr('data-id');
            vm.destroyItem(id);
        });
        $(document).on('click', '.datatables-edit-action', function () {
            var id = $(this).attr('data-id');
            vm.fetchItem(id);
        });

        $(document).on('change', '.toggle-select-action', function () {
            var id = $(this).val();
            vm.toggleSelectedItem(id);
        });
    </script>
@endsection
