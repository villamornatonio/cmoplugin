<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>{{{ config('company.name') }}}</title>

    <!-- Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="/print/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="/print/css/bootstrap-theme.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        body{
            font-size: 18px; !important;
        }
    </style>
</head>
<body>
<div class="container">
    <br>
    <div class="row" id="print">
        <div class="col-md-12">
            <button class="btn btn-primary" onclick="printReceipt();"> PRINT</button>
            <a class="btn btn-primary" href="{!! url('payments') !!}"> BACK TO PAYMENTS</a>
        </div>
    </div>
    <div class="row">
        <span class="pull-right">Reg No : {{ $receipts[0]['temp_id'] }}</span>
    </div>
    <div class="row">
        <span class="pull-right">Date: : {{ Carbon\Carbon::now()->format('d-m-Y') }}</span>
    </div>
    {{--<br>--}}
    {{--<div class="row">--}}
        {{--<div class="col-md-12">--}}
            {{--<table class="table">--}}
                {{--<tbody style="border: none !important;">--}}
                    {{--<tr style="border: none !important;">--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">Reg No : {{{ $receipts[0]['trainee_id'] }}}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;">&nbsp;</td>--}}
                        {{--<td style="border: none !important;" >Date: : {{{ Carbon\Carbon::now()->format('d-m-Y') }}}</td>--}}
                    {{--</tr>--}}
                {{--</tbody>--}}

            {{--</table>--}}
        {{--</div>--}}

    {{--</div>--}}


    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td style="border: none !important;" >
                            <div class="col-md-2">
                                <img width="140px" height="100px" src="{{ session('company.logo_url') }}">
                            </div>
                        </td>
                        <td style="border: none !important;">
                            <div class="col-md-12">

                                <div class="row text-center">
                                    <h2>{{ session('company.name') }}</h2>
                                </div>
                                <div class="row text-center">
                                    <h3>{{ session('company.address') }}</h3>
                                </div>
                                <div class="row text-center">
                                    <h3>Tel. No. : {{ session('company.phone') }} Email : {{ session('company.email') }}</h3>
                                </div>
                                <div class="row text-center">
                                    <h3>Registration Form</h3>
                                </div>
                                <div class="row text-center">
                                    <h3>Form Ref.: {{ session('company.ref_no') }}</h3>
                                </div>

                            </div>
                        </td>
                        <td style="border: none !important;">
                            <div class="col-md-2">
                                <div class="row">
                                    {{--                <img src="{{ '/' . config()->get('uploads.trainee') . $receipts[0]['avatar'] }}" with="140" height="100">--}}
                                    <img src="http://placehold.it/140x100">
                                </div>


                            </div>
                        </td>
                    </tr>
                </tbody>

            </table>

        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <hr/>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tr style="border: none !important;">
                    <td style="border: none !important;">
                        {{{ $receipts[0]['position'] }}}
                    </td>
                    <td style="border: none !important;">
                        {{{ $receipts[0]['lastname'] }}}
                    </td>
                    <td style="border: none !important;">
                        {{{ $receipts[0]['firstname'] }}}
                    </td>
                    <td style="border: none !important;">
                        {{{ $receipts[0]['middlename'] }}}
                    </td>
                    <td style="border: none !important;">
                         &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                </tr>
                <tr style="border: none !important;">
                    <td >
                        (Rank/Poistion,
                    </td>
                    <td>
                        Last Name,
                    </td>
                    <td >
                        First Name,
                    </td>
                    <td >
                        Middle Name)
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                    <td style="border: none !important;">
                        &nbsp;
                    </td>
                </tr>

            </table>

        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                <tr style="border: none !important;">
                    <td style="border: none !important;" >Address :</td>
                    <td style="border: none !important;" ></td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >Passport No :</td>
                    <td style="border: none !important;" >{{{ $receipts[0]['passport'] }}}</td>

                </tr>
                <tr style="border: none !important;" >
                    <td style="border: none !important;" >Company :</td>
                    <td style="border: none !important;" >{{{ $receipts[0]['company'] }}}</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >Seamans Book No :</td>
                    <td style="border: none !important;" >{{{ $receipts[0]['seamans'] }}}</td>

                </tr>
                <tr style="border: none !important;" >
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >&nbsp;</td>
                    <td style="border: none !important;" >Date of Birth :</td>
                    <td style="border: none !important;" >{{{ $receipts[0]['dob'] }}}</td>
                </tr>

                </tbody>
            </table>

        </div>


    </div>
    <div class="row">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Couse</th>
                    <th>Duration</th>
                    <th>Time</th>
                    <th>Course Fee</th>
                    <th>%</th>
                    <th>Amount Paid</th>
                    <th>Date</th>
                    <th>Remarks</th>
                </tr>
                </thead>
                <tbody>
                @foreach($receipts as $key => $receipt)
                    <tr>
                        <td>{!! $receipt['code'] !!}</td>
                        <td>{!! $receipt['date'] !!}</td>
                        <td>{!! $receipt['time'] !!}</td>
                        <td>{!! $receipt['price'] !!}</td>
                        <td>{!! $receipt['discount'] !!}</td>
                        <td>{!! $receipts[0]['amount_paid'] !!}</td>
                        <td>{!! $receipt['date'] !!}</td>
                        <td></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <table class="table">
                <tbody style="border: none !important;">
                    <tr style="border: none !important;">
                        <td style="border: none !important;" >&nbsp;</td>
                        <td style="border: none !important;" >&nbsp;</td>
                        <td style="border: none !important;" >&nbsp;</td>
                        <td style="border: none !important;" ><strong>Total : </strong></td>
                        <td style="border: 2px solid lightslategray;" >{{{ $receipts[0]['total_amount'] }}}</td>
                        <td style="border: none !important;" >&nbsp;</td>
                        <td style="border: none !important;" ><strong>Balance : </strong></td>
                        <td style="border: 2px solid lightslategray;"  >{{{ $receipts[0]['remaining_balance'] }}}</td>
                    </tr>
                </tbody>

            </table>
        </div>
    </div>
    <br>
    <br>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <tbody>
                    <tr>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">{{{ $receipts[0]['name'] }}}</td>
                        <td style="border: none !important;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td style="border: none !important;">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Trainee</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>

            </table>
        </div>

    </div>
    <div class="row">
        Assesed by: <strong>{{{ strtoupper(auth()->user()->name()) }}}</strong>

    </div>


</div>

<script>
    function printReceipt() {
        $('#print').remove();
        window.print();
    }
</script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/print/js/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- Latest compiled and minified JavaScript -->
<script src="/print/js/bootstrap.js"></script>
</body>
</html>