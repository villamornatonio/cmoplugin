@extends('layouts.master')
@section('content')

    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('referrers') }}}">Referrers</a></li>
            <li class="active">Edit Referrer</li>
        </ol>

        <div class="row">

            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-heading nopaddingbottom">
                        <h4 class="panel-title">Edit Referrer</h4>

                        <p>Please provide the necessary information on the fields</p>
                    </div>
                    <div class="panel-body">
                        <hr>
                        {!! Form::open([
                        'url' => 'referrers/' . $referrer['id'],
                        'method' => 'PUT',
                        'class' => 'form-horizontal'

                        ]) !!}

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Firstname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('firstname',$referrer['firstname'],['class' => 'form-control' ,'placeholder' => 'John',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Lastname <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('lastname',$referrer['lastname'],['class' => 'form-control' ,'placeholder' => 'Doe',
                                'required' => true]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Company <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('company',$referrer['company'],['class' => 'form-control' ,'placeholder' => 'ACME']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Email <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::email('email',$referrer['email'],['class' => 'form-control' ,'placeholder' => 'johndoe@example.com']) !!}
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Mobile No. <span class="text-danger">*</span></label>

                            <div class="col-sm-8">
                                {!! Form::text('contact_no',$referrer['mobile'],['class' => 'form-control' ,'placeholder' => '092300000',
                                'required' => true]) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Date of Birth <span
                                        class="text-danger">*</span></label>

                            <div class="col-sm-8">

                                <div class="input-group mb20">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    {!! Form::text('dob',$referrer['dob'],['class' => 'form-control' ,'placeholder' => 'Date of Birth','id' => 'dob']) !!}
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button class="btn btn-success btn-quirk btn-wide mr5">Update</button>
                                <button type="reset" class="btn btn-quirk btn-wide btn-default">Reset</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <!-- panel-body -->
                </div>
                <!-- panel -->

            </div>
            <!-- col-md-6 -->

        </div>
        <!--row -->

    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script type="text/javascript">
        $(function() {
            $("#dob").mask("9999-99-99");
        });

    </script>
@endsection