@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('trainings/completed') }}}">Pending Trainees</a></li>
            <li class="active">Display Pending Training List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Pending Trainees List</h4>

                <p>Manage Pending Trainees</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>COURSE</th>
                            <th>START DATE</th>
                            <th>END DATE</th>
                            <th>START TIME</th>
                            <th>END TIME</th>
                            <th>COMPLETED</th>
                            <th>COMPANY CHARGE</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/trainings/0/all',
                "columnDefs": [ {
                    "targets": 9,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'>" +
                                "<li>" +
                                "<a href='/trainings/" + row['id'] + "/toggle/completed" + "'>" +
                                "<i class='fa fa-toggle-on'></i>" +
                                "</a>" +
                                "</li>" +
                                "<li><a href='/rooms/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li></ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'enrollments.id'},
                    {data: 'name', name: 'trainee'},
                    {data: 'course', name: 'courses.name'},
                    {data: 'startDate', name: 'start_date'},
                    {data: 'endDate', name: 'end_date'},
                    {data: 'startTime', name: 'start_time'},
                    {data: 'endTime', name: 'end_time'},
                    {data: 'completed', name: 'is_completed'},
                    {data: 'companyCharge', name: 'is_company_charged'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });
    </script>
@endsection
