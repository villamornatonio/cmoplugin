@extends('layouts.pages')
@section('content')
    <!-- Marine Content Inner -->
    <div id="marine-content-inner">


        <!-- Main Content -->
        <section id="main-content">
            <!-- Container -->
            <div class="container">
            </div>
            <section id="slider">
                <div class="container">
                    <!-- START REVOLUTION SLIDER 4.6.0 fullwidth mode -->
                    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet'
                          type='text/css'>
                    <link href='http://fonts.googleapis.com/css?family=Raleway:100,200,300,700,800,900' rel='stylesheet'
                          type='text/css'>
                    <div id="rev_slider_22_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container"
                         style="margin:0px auto;background-color:#eee;padding:0px;margin-top:0px;margin-bottom:0px;max-height:810px;">
                        <div id="rev_slider_22_1" class="rev_slider fullwidthabanner"
                             style="display:none;max-height:810px;height:810px;">
                            <ul>    <!-- SLIDE  -->
                                <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                                    data-thumb="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/slide1-2-320x200.jpg"
                                    data-saveperformance="off" data-title="Slide">
                                    <!-- MAIN IMAGE -->
                                    <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                         alt="slide1-2"
                                         data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/slide1-2.jpg"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption tp-fade rs-parallaxlevel-3"
                                         data-x="center" data-hoffset="1"
                                         data-y="155"
                                         data-speed="800"
                                         data-start="1600"
                                         data-easing="Power4.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 2;"><img
                                                src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                                alt=""
                                                data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/ship-1-copy.png">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption big_extraheavy_60 skewfromrightshort tp-resizeme rs-parallaxlevel-0"
                                         data-x="center" data-hoffset="0"
                                         data-y="285"
                                         data-speed="500"
                                         data-start="1100"
                                         data-easing="Power3.easeInOut"
                                         data-splitin="chars"
                                         data-splitout="none"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                                        PNTCI
                                    </div>

                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption extrabold_littlesub_open customin tp-resizeme rs-parallaxlevel-0"
                                         data-x="center" data-hoffset="0"
                                         data-y="385"
                                         data-customin="x:0;y:0;z:0;rotationX:90;rotationY:0;rotationZ:0;scaleX:1;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:200;transformOrigin:50% 0%;"
                                         data-speed="300"
                                         data-start="2250"
                                         data-easing="Power3.easeInOut"
                                         data-splitin="none"
                                         data-splitout="none"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 4; max-width: auto; max-height: auto; white-space: nowrap;">
                                        We are the Seafarers' Choice.
                                    </div>

                                    <!-- LAYER NR. 4 -->
                                    <div class="tp-caption sfb rs-parallaxlevel-0"
                                         data-x="center" data-hoffset="0"
                                         data-y="380"
                                         data-speed="300"
                                         data-start="2250"
                                         data-easing="Power3.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 5;"><img
                                                src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                                alt=""
                                                data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/lines.png">
                                    </div>
                                </li>
                                <!-- SLIDE  -->
                                <li data-transition="notransition" data-slotamount="7" data-masterspeed="1500"
                                    data-thumb="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/2s.jpg"
                                    data-saveperformance="off" data-title="Mobile Interaction">
                                    <!-- MAIN IMAGE -->
                                    <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                         alt="img_par_11"
                                         data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/img_par_11.jpg"
                                         data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                                    <!-- LAYERS -->

                                    <!-- LAYER NR. 1 -->
                                    <div class="tp-caption lfb rs-parallaxlevel-0"
                                         data-x="center" data-hoffset="0"
                                         data-y="140"
                                         data-speed="1500"
                                         data-start="900"
                                         data-easing="Power4.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 2;"><img
                                                src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                                alt=""
                                                data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/marine.png">
                                    </div>

                                    <!-- LAYER NR. 2 -->
                                    <div class="tp-caption customin rs-parallaxlevel-3"
                                         data-x="center" data-hoffset="5"
                                         data-y="bottom" data-voffset="-416"
                                         data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
                                         data-speed="500"
                                         data-start="1900"
                                         data-easing="Power4.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 3;">
                                        <div style="" class="tp-layer-inner-rotation   rs-pulse"
                                             data-easing="Power4.easeInOut" data-speed="0.5" data-zoomstart="0.75"
                                             data-zoomend="1"><img
                                                    src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                                    alt=""
                                                    data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/pulse1.png">
                                        </div>
                                    </div>

                                    <!-- LAYER NR. 3 -->
                                    <div class="tp-caption lfb rs-parallaxlevel-3"
                                         data-x="520"
                                         data-y="320"
                                         data-speed="1500"
                                         data-start="1400"
                                         data-easing="Power4.easeInOut"
                                         data-elementdelay="0.1"
                                         data-endelementdelay="0.1"
                                         data-endspeed="300"

                                         style="z-index: 4;"><img
                                                src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/plugins/revslider/images/dummy.png"
                                                alt=""
                                                data-lazyload="http://marinewp.wpengine.comhttp://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/hand.png">
                                    </div>
                                </li>
                            </ul>
                            <div class="tp-bannertimer"></div>
                        </div>

                        <script type="text/javascript">

                            /******************************************
                             -    PREPARE PLACEHOLDER FOR SLIDER    -
                             ******************************************/


                            var setREVStartSize = function () {
                                var tpopt = new Object();
                                tpopt.startwidth = 1170;
                                tpopt.startheight = 810;
                                tpopt.container = jQuery('#rev_slider_22_1');
                                tpopt.fullScreen = "off";
                                tpopt.forceFullWidth = "off";

                                tpopt.container.closest(".rev_slider_wrapper").css({height: tpopt.container.height()});
                                tpopt.width = parseInt(tpopt.container.width(), 0);
                                tpopt.height = parseInt(tpopt.container.height(), 0);
                                tpopt.bw = tpopt.width / tpopt.startwidth;
                                tpopt.bh = tpopt.height / tpopt.startheight;
                                if (tpopt.bh > tpopt.bw)tpopt.bh = tpopt.bw;
                                if (tpopt.bh < tpopt.bw)tpopt.bw = tpopt.bh;
                                if (tpopt.bw < tpopt.bh)tpopt.bh = tpopt.bw;
                                if (tpopt.bh > 1) {
                                    tpopt.bw = 1;
                                    tpopt.bh = 1
                                }
                                if (tpopt.bw > 1) {
                                    tpopt.bw = 1;
                                    tpopt.bh = 1
                                }
                                tpopt.height = Math.round(tpopt.startheight * (tpopt.width / tpopt.startwidth));
                                if (tpopt.height > tpopt.startheight && tpopt.autoHeight != "on")tpopt.height = tpopt.startheight;
                                if (tpopt.fullScreen == "on") {
                                    tpopt.height = tpopt.bw * tpopt.startheight;
                                    var cow = tpopt.container.parent().width();
                                    var coh = jQuery(window).height();
                                    if (tpopt.fullScreenOffsetContainer != undefined) {
                                        try {
                                            var offcontainers = tpopt.fullScreenOffsetContainer.split(",");
                                            jQuery.each(offcontainers, function (e, t) {
                                                coh = coh - jQuery(t).outerHeight(true);
                                                if (coh < tpopt.minFullScreenHeight)coh = tpopt.minFullScreenHeight
                                            })
                                        } catch (e) {
                                        }
                                    }
                                    tpopt.container.parent().height(coh);
                                    tpopt.container.height(coh);
                                    tpopt.container.closest(".rev_slider_wrapper").height(coh);
                                    tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(coh);
                                    tpopt.container.css({height: "100%"});
                                    tpopt.height = coh;
                                } else {
                                    tpopt.container.height(tpopt.height);
                                    tpopt.container.closest(".rev_slider_wrapper").height(tpopt.height);
                                    tpopt.container.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").height(tpopt.height);
                                }
                            };

                            /* CALL PLACEHOLDER */
                            setREVStartSize();


                            var tpj = jQuery;
                            tpj.noConflict();
                            var revapi22;

                            tpj(document).ready(function () {

                                if (tpj('#rev_slider_22_1').revolution == undefined)
                                    revslider_showDoubleJqueryError('#rev_slider_22_1');
                                else
                                    revapi22 = tpj('#rev_slider_22_1').show().revolution(
                                            {
                                                dottedOverlay: "none",
                                                delay: 16000,
                                                startwidth: 1170,
                                                startheight: 810,
                                                hideThumbs: 200,

                                                thumbWidth: 100,
                                                thumbHeight: 50,
                                                thumbAmount: 2,


                                                simplifyAll: "off",

                                                navigationType: "none",
                                                navigationArrows: "solo",
                                                navigationStyle: "preview4",

                                                touchenabled: "on",
                                                onHoverStop: "on",
                                                nextSlideOnWindowFocus: "off",

                                                swipe_threshold: 0.7,
                                                swipe_min_touches: 1,
                                                drag_block_vertical: false,

                                                parallax: "mouse",
                                                parallaxBgFreeze: "on",
                                                parallaxLevels: [4, 5, 3, 2, 5, 4, 3, 5, 5, 0],
                                                parallaxDisableOnMobile: "on",


                                                keyboardNavigation: "off",

                                                navigationHAlign: "center",
                                                navigationVAlign: "bottom",
                                                navigationHOffset: 0,
                                                navigationVOffset: 20,

                                                soloArrowLeftHalign: "left",
                                                soloArrowLeftValign: "center",
                                                soloArrowLeftHOffset: 20,
                                                soloArrowLeftVOffset: 0,

                                                soloArrowRightHalign: "right",
                                                soloArrowRightValign: "center",
                                                soloArrowRightHOffset: 20,
                                                soloArrowRightVOffset: 0,

                                                shadow: 0,
                                                fullWidth: "on",
                                                fullScreen: "off",

                                                spinner: "spinner4",

                                                stopLoop: "off",
                                                stopAfterLoops: -1,
                                                stopAtSlide: -1,

                                                shuffle: "off",

                                                autoHeight: "off",
                                                forceFullWidth: "off",


                                                hideThumbsOnMobile: "off",
                                                hideNavDelayOnMobile: 1500,
                                                hideBulletsOnMobile: "off",
                                                hideArrowsOnMobile: "off",
                                                hideThumbsUnderResolution: 0,

                                                hideSliderAtLimit: 0,
                                                hideCaptionAtLimit: 0,
                                                hideAllCaptionAtLilmit: 0,
                                                startWithSlide: 0
                                            });


                            });
                            /*ready*/

                        </script>


                        <style type="text/css">
                            #rev_slider_22_1_wrapper .tp-loader.spinner4 div {
                                background-color: #fff !important;
                            }
                        </style>
                    </div>
                    <!-- END REVOLUTION SLIDER -->
                    <script>
                        /* Fix The Revolution Slider Loading Height issue */
                        jQuery(document).ready(function ($) {
                            $('.rev_slider_wrapper').each(function () {
                                $(this).css('height', '');
                                var revStartHeight = parseInt($('>.rev_slider', this).css('height'));
                                $(this).height(revStartHeight);
                                $(this).parents('#slider').height(revStartHeight);

                                $(window).load(function () {
                                    $('#slider').css('height', '');
                                });
                            });
                        });
                    </script>
                </div>
            </section>
            <div class="container">
                <section class="full-width-bg blue-gradient-bg normal-padding alternate-slider-bg">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="service style1">

                                <div class="service-icon" style="background-color: #2aa7cd">
                                    <i style="color:#a1f1ff" class="icons icon-waves-outline"></i>
                                </div>
                                <h3 style="color:#ffffff">Training Excellence</h3>

                                <div class="content_box" style="color: #8cd9ff">IT WAS HARD TO MAKE IT</div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="service style1">

                                <div class="service-icon" style="background-color: #2aa7cd">
                                    <i style="color:#a1f1ff" class="icons icon-ok-outline"></i>
                                </div>
                                <h3 style="color:#ffffff">State of the Art Facilities</h3>

                                <div class="content_box" style="color: #8cd9ff">IT WAS HARD TO MAKE IT SO COOL</div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="service style1">

                                <div class="service-icon" style="background-color: #2aa7cd">
                                    <i style="color:#a1f1ff" class="icons icon-th-list-1"></i>
                                </div>
                                <h3 style="color:#ffffff">Globally Competitive</h3>

                                <div class="content_box" style="color: #8cd9ff">IT WAS HARD TO MAKE IT SO COOL</div>

                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6">
                            <div class="service style1">

                                <div class="service-icon" style="background-color: #2aa7cd">
                                    <i style="color:#a1f1ff" class="icons icon-resize-small-1"></i>
                                </div>
                                <h3 style="color:#ffffff">Certified Instructors</h3>

                                <div class="content_box" style="color: #8cd9ff">IT WAS HARD TO MAKE IT SO COOL</div>

                            </div>
                        </div>
                    </div>
                </section>

                <div class="row">
                    <section class="col-lg-12 col-md-12 col-sm-12 small-padding">

                        <div class="full_bg full-width-bg " data-animation=""
                             style="background-color: #23323a; background-attachment: scroll; background-position: left top; background-size: 100% 100%; margin-top: -40px; padding-top: 20px;">


                            <h3 class="special-text"
                                style="color: #c4d3de;font-size: 30px;font-weight: 300;text-align: center"
                                data-animation="">Recent Courses </h3>

                            <div class="special-text"
                                 style="color: #658190;font-size: 14px;font-weight: normal;text-align: center"
                                 data-animation="">Morbi a libero eget erat auctor cursus vitae id tortor. Suspendisse
                                rhoncus. Vestibulum eu ligula lorem.
                            </div>
                            <div class="special-text"
                                 style="color: #658190;font-size: 14px;font-weight: normal;margin-bottom: 35px;text-align: center"
                                 data-animation="">Vivamus orci sem, consectetur ut vestibulum a, semper ac dui.
                            </div>
                            <section class="full-width projects-section dark-gray-bg ">
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="project">
                                        <div class="project-image wow animated fadeInLeft">
                                            <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/14884d0eb9aa4ec41df23fd89366866f_3a-776-404-c.jpg"
                                                 class="img-responsive" width="776" height="404" alt="Team Spirit"/>

                                            <div class="project-hover">
                                                <a class="link-icon"
                                                   href="http://marinewp.wpengine.com/portfolio-item/team-spirit/"></a>
                                                <a class="search-icon"
                                                   href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/3a.jpg"
                                                   rel="prettyPhoto"></a>
                                            </div>
                                        </div>

                                        <div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
                                            <h4>Course 1</h4>
                                            <span class="project-category">Print</span>

                                            <div class="project-like" data-post="3820">
                                                <i class=" icons icon-heart-7"></i>
                                                <span class="like-count">497</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="project">
                                        <div class="project-image wow animated fadeInLeft">
                                            <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/003d07355fe5325baa7ef85849e1bd5d_2a-776-404-c.jpg"
                                                 class="img-responsive" width="776" height="404" alt="Sample Item"/>

                                            <div class="project-hover">
                                                <a class="link-icon"
                                                   href="http://marinewp.wpengine.com/portfolio-item/sample-item/"></a>
                                                <a class="search-icon"
                                                   href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/2a.jpg"
                                                   rel="prettyPhoto"></a>
                                            </div>
                                        </div>

                                        <div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
                                            <h4>Course 2</h4>
                                            <span class="project-category">Web</span>

                                            <div class="project-like" data-post="3767">
                                                <i class=" icons icon-heart-7"></i>
                                                <span class="like-count">668</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="project">
                                        <div class="project-image wow animated fadeInLeft">
                                            <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/d27bc0ba25eaf0ebe0db4851d357c6a8_slide1-2-776-404-c.jpg"
                                                 class="img-responsive" width="776" height="404" alt="Project Title"/>

                                            <div class="project-hover">
                                                <a class="link-icon"
                                                   href="http://marinewp.wpengine.com/portfolio-item/test/"></a>
                                                <a class="search-icon"
                                                   href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/04/slide1-2.jpg"
                                                   rel="prettyPhoto"></a>
                                            </div>
                                        </div>

                                        <div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
                                            <h4>Course 3</h4>
                                            <span class="project-category">Print</span>

                                            <div class="project-like" data-post="3605">
                                                <i class=" icons icon-heart-7"></i>
                                                <span class="like-count">453</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6">
                                    <div class="project">
                                        <div class="project-image wow animated fadeInLeft">
                                            <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/c99e4af2f1be6e38dbe78c78bc06cde9_111-776-404-c.jpg"
                                                 class="img-responsive" width="776" height="404"
                                                 alt="Maecenas sodales"/>

                                            <div class="project-hover">
                                                <a class="link-icon"
                                                   href="http://marinewp.wpengine.com/portfolio-item/maecenas-sodales-scelerisque-2/"></a>
                                                <a class="search-icon"
                                                   href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/01/111.jpg"
                                                   rel="prettyPhoto"></a>
                                            </div>
                                        </div>

                                        <div class="project-meta wow animated fadeInLeft" data-wow-offset="0">
                                            <h4>Course 4</h4>
                                            <span class="project-category">Business</span>

                                            <div class="project-like" data-post="2422">
                                                <i class=" icons icon-heart-7"></i>
                                                <span class="like-count">355</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                        </div>

                        <section class="sc-call-to-action full-width-bg light-gray-bg small-padding "
                                 style="background: #fcfcfc;border-top:1px solid #EEEEEE;border-bottom:1px solid #EEEEEE">
                            <div class="row">
                                <div class="col-lg-9 col-md-9 col-sm-9">
                                    <h2 class="big" style="color: #23323a">
                                        Your Training Guide to Success!</h2>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-3 align-right">
                                    <a href="" target="_blank" class="button biggest"><i
                                                class="icons icon-info-circled-alt"></i>CHECK COURSES</a>
                                </div>
                            </div>
                        </section>
                        <div class="full_bg full-width-bg " data-animation=""
                             style="background-image: url(http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/bg1a.jpg); background-attachment: fixed; background-position: center center; padding-top: 60px; padding-bottom: 60px;">


                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="clearfix" style="height: 30px"></div>
                                <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/bg1b.png"
                                     class=" img-responsive alignnone dont_scale  wow animated fadeInLeft"
                                     data-animation="fadeInLeft" alt="">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <ul class="services-list ">
                                    <li>
                                        <img class="icons"
                                             src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/02/paperaero.png"/>

                                        <h3 style="color:#ffffff">Flexible Solutions</h3>

                                        <p style="color:#8cd9ff">Lorem ipsum dolor sit amet, consectetuer adipiscing
                                            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                            aliquam erat volutpat</p>
                                    </li>
                                    <li>
                                        <img class="icons"
                                             src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/02/compass.png"/>

                                        <h3 style="color:#ffffff">Target Approach </h3>

                                        <p style="color:#8ad8fe">Lorem ipsum dolor sit amet, consectetuer adipiscing
                                            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                            aliquam erat volutpat</p>
                                    </li>
                                    <li>
                                        <img class="icons"
                                             src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/02/globe.png"/>

                                        <h3 style="color:#ffffff">Global Developement </h3>

                                        <p style="color:#8ad8fe">Lorem ipsum dolor sit amet, consectetuer adipiscing
                                            elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna
                                            aliquam erat volutpat</p>
                                    </li>
                                </ul>
                                <div class="clearfix" style="height: 0px"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="full_bg full-width-bg " data-animation=""
                             style="background-image: url(http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/bg2a.jpg); background-attachment: fixed; background-position: center center; padding-top: 20px; padding-bottom: 60px; margin-bottom: 30px;">


                            <h3 class="special-text"
                                style="color: #ffffff;font-size: 30px;font-weight: 300;margin-bottom: 40px;text-align: center"
                                data-animation="">Our Team</h3>

                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="team-member " data-animation="">
                                    <a href="http://#" target="_blank" title="Mark Twain">
                                        <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/12e940fa2b09d97ca9b7f6623cca86eb_john-570-543-c.jpg"
                                             class="img-responsive" width="570" height="543" alt="img-responsive"/>
                                    </a>

                                    <a class="read-more"></a>
                                    <h4 style=" color: #ffffff " class="post-title">Capt. German Mendez</h4>
                                    <span style=" color: #2691ac " class="job-title">Chairman of the Board</span>

                                    <div class="text-content" style=" color: #ccd2da "><p>Vivamus pretium imperdiet
                                            dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius
                                            ali luctus. Praesent</p>
                                    </div>
                                    <span class="small-line" style="border-top-color:#ccd2da"></span>
                                    <ul class="social-media">
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-facebook"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-twitter"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-skype"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-google"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-vimeo"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-linkedin"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="team-member " data-animation="">
                                    <a href="http://#" target="_blank" title="John Doe">
                                        <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/0a2607c7609ce77e36bd7de03f4194d9_jan-570-543-c.jpg"
                                             class="img-responsive" width="570" height="543" alt="img-responsive"/>
                                    </a>

                                    <a class="read-more"></a>
                                    <h4 style=" color: #ffffff " class="post-title">Enriquita Mendez</h4>
                                    <span style=" color: #2691ac " class="job-title">President</span>

                                    <div class="text-content" style=" color: #ccd2da "><p>Vivamus pretium imperdiet
                                            dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius
                                            ali luctus. Praesent</p>
                                    </div>
                                    <span class="small-line" style="border-top-color:#ccd2da"></span>
                                    <ul class="social-media">
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-facebook"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-twitter"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-skype"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-google"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-vimeo"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-linkedin"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="team-member " data-animation="">
                                    <a href="http://#" target="_blank" title="Sandra Green">
                                        <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/801c1f6304cf565d6ba2efd082db1178_sarah-570-543-c.jpg"
                                             class="img-responsive" width="570" height="543" alt="img-responsive"/>
                                    </a>

                                    <a class="read-more"></a>
                                    <h4 style=" color: #ffffff " class="post-title">Lilet Mendez</h4>
                                    <span style=" color: #2691ac " class="job-title">Marketing Manager</span>

                                    <div class="text-content" style=" color: #ccd2da "><p>Vivamus pretium imperdiet
                                            dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius
                                            ali luctus. Praesent</p>
                                    </div>
                                    <span class="small-line" style="border-top-color:#ccd2da"></span>
                                    <ul class="social-media">
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-facebook"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-twitter"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-skype"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-google"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-vimeo"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-linkedin"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="team-member " data-animation="">
                                    <a href="http://#" target="_blank" title="Mark Twain">
                                        <img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/12e940fa2b09d97ca9b7f6623cca86eb_john-570-543-c.jpg"
                                             class="img-responsive" width="570" height="543" alt="img-responsive"/>
                                    </a>

                                    <a class="read-more"></a>
                                    <h4 style=" color: #ffffff " class="post-title">Juan Dela Cruz</h4>
                                    <span style=" color: #2691ac " class="job-title">QAR</span>

                                    <div class="text-content" style=" color: #ccd2da "><p>Vivamus pretium imperdiet
                                            dignissim. Duis vehila tis augue. Nulla eu tempus justo. Donec pulvi varius
                                            ali luctus. Praesent</p>
                                    </div>
                                    <span class="small-line" style="border-top-color:#ccd2da"></span>
                                    <ul class="social-media">
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-facebook"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-twitter"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-skype"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-google"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i class="icon-vimeo"></i></a>
                                        </li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-linkedin"></i></a></li>
                                        <li><a href="http://test.com" style="color: #ccd2da"><i
                                                        class="icon-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="clearfix" style="height: 30px"></div>

                        <div class="full_bg full-width-bg " data-animation=""
                             style="background-image: url(http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/map.jpg); background-attachment: scroll; background-position: center center; padding-top: 20px; padding-bottom: 10px; margin-bottom: -41px;">


                            <h3 class="special-text  wow animated fadeInLeft"
                                style="color: #ffffff;font-size: 30px;font-weight: 300;margin-bottom: 40px;text-align: center"
                                data-animation="fadeInLeft">Get In Touch!</h3>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-lg-push-3 col-md-push-3 col-sm-push-3">

                                <form style="text-align: center" ;
                                      class="get-in-touch contact-form  wow animated fadeInUp " method="post">
                                    <input type="hidden" id="contact_nonce" name="contact_nonce"
                                           value="b42249ffd5"/><input type="hidden" name="_wp_http_referer" value="/"/>
                                    <input type="hidden" name="contact-form-value" value="1" id=""/>

                                    <div class="iconic-input">
                                        <input type="text" name="name" placeholder="Name*">
                                        <i class="icons icon-user-1"></i>
                                    </div>
                                    <div class="iconic-input">
                                        <input type="text" name="email" placeholder="Email*">
                                        <i class="icons icon-email"></i>
                                    </div>
                                    <textarea name="msg" placeholder="Message"></textarea>
                                    <input type="submit" value="Send">

                                    <div class="iconic-button">
                                        <input type="reset" value="Clear">
                                        <i class="icons icon-cancel-circle-1"></i>
                                    </div>
                                </form>
                                <div id="msg"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- /Container -->
        </section>
        <!-- /Main Content -->


    </div>
    <!-- /Marine Conten Inner -->
@endsection
