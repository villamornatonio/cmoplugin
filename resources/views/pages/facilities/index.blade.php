@extends('layouts.pages')
@section('content')
    <div id="marine-content-inner">



        <!-- Main Content -->
        <section id="main-content">
            <!-- Container -->
            <div class="container">
                <div class="page-heading style3 wrapper border-bottom  ">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <h1>Services Page</h1>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <p class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#"><span typeof="v:Breadcrumb"><a rel="v:url" property="v:title" href="http://marinewp.wpengine.com/">Home</a></span><span class="delimiter">|</span> <span class="current">Services Page</span></p>						</div>
                    </div>
                </div>
                <div class="row">
                    <section class="col-lg-12 col-md-12 col-sm-12 small-padding">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="flexslider flexslider-thumbnail-gallery ">
                                    <ul class="slides"><li data-tooltip="" data-thumb="http://marinewp.wpengine.com/wp-content/uploads/freshizer/4d9835d8eea4c5c08e3388a128b03e87_61-110-73-c.jpg" class="flex-active-slide" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 1; display: block; z-index: 2;"><img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/4d9835d8eea4c5c08e3388a128b03e87_61-800-530-c.jpg" class="img-responsive" width="800" height="530" alt="" draggable="false">
                                            <div class="project-hover">
                                                <a class="search-icon" href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/61.jpg" rel="prettyPhoto[project-gallery]"></a>
                                            </div></li><li data-tooltip="" data-thumb="http://marinewp.wpengine.com/wp-content/uploads/freshizer/bdef5b8dae87f1baaf698ba411c0f1d3_51-110-73-c.jpg" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class=""><img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/bdef5b8dae87f1baaf698ba411c0f1d3_51-800-530-c.jpg" class="img-responsive" width="800" height="530" alt="" draggable="false">
                                            <div class="project-hover">
                                                <a class="search-icon" href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/51.jpg" rel="prettyPhoto[project-gallery]"></a>
                                            </div></li><li data-tooltip="" data-thumb="http://marinewp.wpengine.com/wp-content/uploads/freshizer/72953e018705f5def26d263bde78a610_8-110-73-c.jpg" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class=""><img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/72953e018705f5def26d263bde78a610_8-800-530-c.jpg" class="img-responsive" width="800" height="530" alt="" draggable="false">
                                            <div class="project-hover">
                                                <a class="search-icon" href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/8.jpg" rel="prettyPhoto[project-gallery]"></a>
                                            </div></li><li data-tooltip="" data-thumb="http://marinewp.wpengine.com/wp-content/uploads/freshizer/59df46c0e40e0f6cf1687b6a3ab5bf28_9-110-73-c.jpg" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class=""><img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/59df46c0e40e0f6cf1687b6a3ab5bf28_9-800-530-c.jpg" class="img-responsive" width="800" height="530" alt="" draggable="false">
                                            <div class="project-hover">
                                                <a class="search-icon" href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/9.jpg" rel="prettyPhoto[project-gallery]"></a>
                                            </div></li><li data-tooltip="" data-thumb="http://marinewp.wpengine.com/wp-content/uploads/freshizer/f1c3c621a88a8ac239188b96459a1283_111-110-73-c.jpg" style="width: 100%; float: left; margin-right: -100%; position: relative; opacity: 0; display: block; z-index: 1;" class=""><img src="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/freshizer/f1c3c621a88a8ac239188b96459a1283_111-800-530-c.jpg" class="img-responsive" width="800" height="530" alt="" draggable="false">
                                            <div class="project-hover">
                                                <a class="search-icon" href="http://3zkasev5puv2jv2aqf2o1xkt.wpengine.netdna-cdn.com/wp-content/uploads/2014/05/111.jpg" rel="prettyPhoto[project-gallery]"></a>
                                            </div></li></ul>
                                    <ol class="flex-control-nav flex-control-thumbs"><li title="" data-original-title=""><img src="http://marinewp.wpengine.com/wp-content/uploads/freshizer/4d9835d8eea4c5c08e3388a128b03e87_61-110-73-c.jpg" class="flex-active" draggable="false"></li><li title="" data-original-title=""><img src="http://marinewp.wpengine.com/wp-content/uploads/freshizer/bdef5b8dae87f1baaf698ba411c0f1d3_51-110-73-c.jpg" draggable="false" class=""></li><li title="" data-original-title=""><img src="http://marinewp.wpengine.com/wp-content/uploads/freshizer/72953e018705f5def26d263bde78a610_8-110-73-c.jpg" draggable="false" class=""></li><li title="" data-original-title=""><img src="http://marinewp.wpengine.com/wp-content/uploads/freshizer/59df46c0e40e0f6cf1687b6a3ab5bf28_9-110-73-c.jpg" draggable="false" class=""></li><li title="" data-original-title=""><img src="http://marinewp.wpengine.com/wp-content/uploads/freshizer/f1c3c621a88a8ac239188b96459a1283_111-110-73-c.jpg" draggable="false" class=""></li></ol></div></div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <h3 class="special-text  wow animated fadeInDown animated" style="font-size: 24px; font-weight: normal; margin-top: 1px; text-align: left; visibility: visible; animation-name: fadeInDown;" data-animation="fadeInDown">What We Do</h3>
                                <p>Maecenas varius, purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.</p>
                                <p>Maecenas varius, purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.</p>
                                <p>Purus nec venenatis vulputate, augue nulla volutpat lorem, ac vulputate metus neque sed tortor. Nullam eleifend mauris elit, et venenatis purus imperdiet sit amet. Morbi ut vehicula sem, quis pellentesque velit. Pellentesque condimentum, augue vel luctus pellentesque, est ante rhoncus lectus, a viverra sem libero a erat. Sed purus sapien, vulputate id aliquet a, vehicula et leo.</p>
                            </div>
                        </div>
                   </section>
                </div>
            </div>
            <!-- /Container -->
        </section>
        <!-- /Main Content -->


    </div>
@endsection
