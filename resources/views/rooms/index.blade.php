@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('rooms') }}}">Rooms</a></li>
            <li class="active">Display Room List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Rooms List</h4>

                <p>Manage Rooms</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>AUTHOR</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/rooms?length=10',
                "columnDefs": [ {
                    "targets": 3,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'><li><a href='/rooms/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li>" +
                                "<li>" +
                                "<a  data-url='/rooms/" + row['id'] + "' data-method='delete' class='btn confirmation-callback' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                "<i class='fa fa-trash'></i>" +
                                "</a>" +
                                "</li>" +
                                "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'rooms.id'},
                    {data: 'name', name: 'rooms.name'},
                    {data: 'author', name: 'author'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }


            });

        });


    </script>
@endsection