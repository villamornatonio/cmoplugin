@extends('layouts.master')
@section('content')
    <div class="contentpanel">

        <ol class="breadcrumb breadcrumb-quirk">
            <li><a href="{{{ url('/') }}}"><i class="fa fa-home mr5"></i> Home</a></li>
            <li><a href="{{{ url('courses') }}}">Courses</a></li>
            <li class="active">Display Course List</li>
        </ol>


        <div class="panel">
            <div class="panel-heading">
                <h4 class="panel-title">Course List</h4>

                <p>Manage Courses</p>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table id="data-display" class="table table-bordered table-striped-col">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>NAME</th>
                            <th>CODE</th>
                            <th>PRICE</th>
                            <th>DURATION</th>
                            <th class="text-center">ACTION</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- table-responsive -->
            </div>
        </div>
        <!-- panel -->


    </div><!-- contentpanel -->
@endsection

@section('scripts')
    <script>
        $(document).ready(function () {

            'use strict';

            $('#data-display').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/api/v1/courses',
                "columnDefs": [ {
                    "targets": 5,
                    render: function (data, type, row) {
                        var actions = "<ul class='table-options'><li><a href='/courses/" + row['id'] + "/edit" + "'><i class='fa fa-pencil'></i></a></li>" +
                                "<li>" +
                                "<a  data-url='/courses/" + row['id'] + "' data-method='delete' class='btn confirmation-callback' data-toggle='confirmation-singleton' data-placement='left' data-original-title='' title=''>" +
                                "<i class='fa fa-trash'></i>" +
                                "</a>" +
                                "</li>" +
                                "</ul>"
                        return actions;
                    }
                } ],
                columns: [
                    {data: 'id', name: 'courses.id'},
                    {data: 'name', name: 'courses.name'},
                    {data: 'code', name: 'courses.code'},
                    {data: 'price', name: 'courses.code'},
                    {data: 'duration', name: 'courses.duration'}
                ],
                "order": [[ 0, "desc" ]],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .on('change', function () {
                                    var val = $.fn.dataTable.util.escapeRegex($(this).val());

                                    column.search(val ? val : '', true, false).draw();
                                });
                    });
                }

            });

        });
    </script>
@endsection