<?php
return [
    'rooms' => [
        'create' => [
            'save' => 'Create A Room',
            'reset' => 'Reset',
        ],
        'update' => [
            'save' => 'Update a Room',
            'reset' => 'Reset',
        ],

    ]
];