<?php
return [
    'rooms' => [
        'create' => [
            'success' => 'Successfuly Created a New Room!',
            'failed' => 'Failed to Create a New Room',
        ],
        'update' => [
            'success' => 'Successfuly Updated a Room!',
            'failed' => 'Failed to Update a Room',
        ],
        'delete' => [
            'success' => 'Successfuly Deleted  Room!',
            'failed' => 'Failed to Delete a New Room',
        ]
    ],
    'courses' => [
        'create' => [
            'success' => 'Successfuly Created a New Course!',
            'failed' => 'Failed to Create a New Course',
        ],
        'update' => [
            'success' => 'Successfuly Updated a Course!',
            'failed' => 'Failed to Update a Course',
        ],
        'delete' => [
            'success' => 'Successfuly Deleted  Course!',
            'failed' => 'Failed to Delete a New Course',
        ]
    ]
];