<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'yajra\\Datatables' => array($vendorDir . '/yajra/laravel-datatables-oracle/src'),
    'Svg\\' => array($vendorDir . '/phenx/php-svg-lib/src'),
    'Sabberworm\\CSS' => array($vendorDir . '/sabberworm/php-css-parser/lib'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'PhpSpec' => array($vendorDir . '/phpspec/phpspec/src'),
    'PHPExcel' => array($vendorDir . '/phpoffice/phpexcel/Classes'),
    'Mockery' => array($vendorDir . '/mockery/mockery/library'),
    'Maatwebsite\\Excel\\' => array($vendorDir . '/maatwebsite/excel/src'),
    'Laracasts\\Flash' => array($vendorDir . '/laracasts/flash/src'),
    'JakubOnderka\\PhpConsoleHighlighter' => array($vendorDir . '/jakub-onderka/php-console-highlighter/src'),
    'JakubOnderka\\PhpConsoleColor' => array($vendorDir . '/jakub-onderka/php-console-color/src'),
    'Dropbox' => array($vendorDir . '/dropbox/dropbox-sdk/lib'),
    'Dotenv' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Diff' => array($vendorDir . '/phpspec/php-diff/lib'),
);
