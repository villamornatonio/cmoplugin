<?php

use App\Models\Payment;
use App\Models\Trainee;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();
        $user_ids = User::all()->lists('id')->toArray();
        $trainee_ids = Trainee::all()->lists('id')->toArray();
        foreach (range(1, 100) as $index) {
            Payment::create([
                'amount' => $fake->randomNumber(),
                'remaining_balance' => $fake->randomNumber(),
                'discount' => $fake->randomNumber(),
                'or_no' => $fake->randomDigit() . $fake->randomLetter . $fake->numberBetween(1,1000),
                'trainee_id' => $fake->randomElement($trainee_ids),
                'created_by' => $fake->randomElement($user_ids),
                'created_at' => new DateTime,
                'updated_at' => new DateTime
            ]);
        }
    }
}
