<?php

use App\Models\Company;
use App\Models\Trainee;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class TraineesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();
        $user_ids = User::all()->lists('id')->toArray();
        $company_ids = Company::all()->lists('id')->toArray();
        foreach (range(1, 100) as $index) {
            Trainee::create([
                'position' => $fake->companySuffix,
                'firstname' => $fake->firstName,
                'middlename' => $fake->lastName,
                'lastname' => $fake->lastName,
                'passport_no' => $fake->randomNumber(),
                'seamans_no' => $fake->randomDigitNotNull,
                'mobile' => $fake->phoneNumber,
                'dob' => $fake->date('Y-m-d'),
                'email' => $fake->email,
                'emergency_person' => $fake->name,
                'emergency_contact' => $fake->phoneNumber,
                'created_by' => $fake->randomElement($user_ids),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }
    }
}

