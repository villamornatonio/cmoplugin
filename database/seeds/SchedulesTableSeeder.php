<?php

use App\Models\Company;
use App\Models\Course;
use App\Models\Room;
use App\Models\Schedule;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class SchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        $room_ids = Room::all()->lists('id')->toArray();
        $courses_ids = Course::all()->lists('id')->toArray();
        $company_ids = Company::all()->lists('id')->toArray();
        foreach(range(1, 100) as $index){
            Schedule::create([
                'room_id' => $fake->randomElement($room_ids),
                'course_id' => $fake->randomElement($courses_ids),
                'created_by' => $fake->randomElement($user_ids),
                'start_date' => $fake->date('Y-m-d'),
                'end_date' => $fake->date('Y-m-d'),
                'start_time' => $fake->time(),
                'end_time' => $fake->time(),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }
    }
}

