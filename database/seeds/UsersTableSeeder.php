<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 8/9/15
 * Time: 5:16 PM
 */


use App\Models\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        $Users = [
            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'admin@cmoplugin.com',
                'password' => Hash::make('password'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'Lilet',
                'lastname' => 'Mendez',
                'email' => 'liletmendez@pmfi.ph',
                'password' => Hash::make('(_mC/:s[ekRuBrA'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'German',
                'lastname' => 'Mendez',
                'email' => 'germanmendez@pmfi.ph',
                'password' => Hash::make('(_mC/:s[ekRuBrA'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'Finance',
                'lastname' => 'Officer 1',
                'email' => 'finance@pmfi.ph',
                'password' => Hash::make('finance2015'),
                'role_id' => '2',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'Registrar',
                'lastname' => 'Officer 1',
                'email' => 'registrar@pmfi.ph',
                'password' => Hash::make('registrar2015'),
                'role_id' => '3',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'Cashier',
                'lastname' => 'Officer 1',
                'email' => 'cashier@pmfi.ph',
                'password' => Hash::make('cashier2015'),
                'role_id' => '4',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],
            ['firstname' => 'Record',
                'lastname' => 'Officer 1',
                'email' => 'record@pmfi.ph',
                'password' => Hash::make('record2015'),
                'role_id' => '5',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 1
            ],

            ['firstname' => 'Villamor',
                'lastname' => 'Natonio Jr',
                'email' => 'admin@pntci.ph',
                'password' => Hash::make('password'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'Lilet',
                'lastname' => 'Mendez',
                'email' => 'liletmendez@pntci.ph',
                'password' => Hash::make('(_mC/:s[ekRuBrA'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'German',
                'lastname' => 'Mendez',
                'email' => 'germanmendez@pntci.ph',
                'password' => Hash::make('(_mC/:s[ekRuBrA'),
                'role_id' => '1',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'Finance',
                'lastname' => 'Officer 1',
                'email' => 'finance@pntci.ph',
                'password' => Hash::make('finance2015'),
                'role_id' => '2',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'Registrar',
                'lastname' => 'Officer 1',
                'email' => 'registrar@pntci.ph',
                'password' => Hash::make('registrar2015'),
                'role_id' => '3',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'Cashier',
                'lastname' => 'Officer 1',
                'email' => 'cashier@pntci.ph',
                'password' => Hash::make('cashier2015'),
                'role_id' => '4',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],
            ['firstname' => 'Record',
                'lastname' => 'Officer 1',
                'email' => 'record@pntci.ph',
                'password' => Hash::make('record2015'),
                'role_id' => '5',
                'is_activated' => 1,
                'avatar' => 'user-default.png',
                'contact_no' => '+639235020361',
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => 2
            ],

        ];

        DB::table('users')->insert($Users);

    }

}