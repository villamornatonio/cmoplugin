<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 8/9/15
 * Time: 5:16 PM
 */

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class RolesTableSeeder extends Seeder
{

    public function run()
    {

        $Roles = [
            ['name' => 'admin','label' => 'Site Administrator', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'finance','label' => 'Finance Officer', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'registrar','label' => 'Registration Officer', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'cashier','label' => 'Cashier', 'created_at' => new DateTime, 'updated_at' => new DateTime],
            ['name' => 'record','label' => 'Record Custodian', 'created_at' => new DateTime, 'updated_at' => new DateTime],
        ];
        DB::table('roles')->insert($Roles);

    }

}