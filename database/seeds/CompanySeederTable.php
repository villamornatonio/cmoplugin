<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $Companies = [
            [
                'name' => 'PENTAGON MARITIME FOUNDATION, INC.',
                'phone' => '516-5008 - 516-1929',
                'address' => '2F PARKVIEW PLAZA TAFT AVE. cor TM KALAW ST., ERMITA MANILA',
                'email' => 'pntci31051@gmail.com',
                'short_name' => 'PMFI',
                'logo_url' => '/images/pntci.png',
                'ref_no' => 'PNTCI-WI 1.1-01-01-AUG.2015',
            ],
            [
                'name' => 'PENTAGON NAUTICAL TECHNOLOGICAL CENTER, INC.',
                'phone' => '254-3873',
                'address' => '2nd FLOOR CITADEL / DELGADO BLDG., 637 BONIFACIO DRIVE COR. 25th STREET, PORT AREA, MANILA, PHILIPPINES',
                'email' => 'pntci1051@gmail.com',
                'short_name' => 'PNTCI',
                'logo_url' => '/images/pntci.png',
                'ref_no' => 'PNTCI-WI 1.1-01-01-AUG.2015',
            ]

        ];

        DB::table('companies')->insert($Companies);
    }
}