<?php

use App\Models\Company;
use App\Models\Room;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
class RoomsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        $company_ids = Company::all()->lists('id')->toArray();
        foreach(range(1, 60) as $index){
            Room::create([
                'name' => $fake->numberBetween(100,999) . $fake->randomLetter,
                'created_by' => $fake->randomElement($user_ids),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }
    }
}
