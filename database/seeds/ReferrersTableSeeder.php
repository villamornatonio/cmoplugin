<?php

use App\Models\Company;
use App\Models\Referrer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ReferrersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();
        $company_ids = Company::all()->lists('id')->toArray();

        foreach (range(1, 60) as $index) {
            Referrer::create([
                'firstname' => $fake->firstName,
                'lastname' => $fake->lastName,
                'contact_no' => $fake->phoneNumber,
                'company' => $fake->company,
                'dob' => $fake->date('Y-m-d'),
                'email' => $fake->companyEmail,
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }

    }
}
