<?php

use App\Models\Company;
use App\Models\Course;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        $company_ids = Company::all()->lists('id')->toArray();
        foreach(range(1, 60) as $index){
            Course::create([
                'name' => $fake->word() . $fake->numberBetween(1,1000),
                'code' => $fake->word() . $fake->randomDigit . $fake->randomLetter,
                'price' => $fake->randomNumber(),
                'duration' => $fake->numberBetween(1,20),
                'created_by' => $fake->randomElement($user_ids),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }
    }
}
