<?php

use App\Models\Company;
use App\Models\Course;
use App\Models\Enrollment;
use App\Models\Referrer;
use App\Models\Room;
use App\Models\Schedule;
use App\Models\Trainee;
use App\User;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class EnrollmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fake = Faker::create();

        $user_ids = User::all()->lists('id')->toArray();
        $trainee_ids = Trainee::all()->lists('id')->toArray();
        $referrer_ids = Referrer::all()->lists('id')->toArray();
        $schedule_ids = Schedule::all()->lists('id')->toArray();
        $company_ids = Company::all()->lists('id')->toArray();


        foreach(range(1, 100) as $index){
            Enrollment::create([
                'trainee_id' => $fake->randomElement($trainee_ids),
                'schedule_id' => $fake->randomElement($schedule_ids),
                'referred_by' => $fake->randomElement($referrer_ids),
                'is_company_charged' => $fake->boolean(),
                'is_completed' => $fake->boolean(),
                'created_by' => $fake->randomElement($user_ids),
                'company' => $fake->company(),
                'has_fully_paid' => $fake->boolean(),
                'has_partially_paid' => $fake->boolean(),
                'date_completed' => $fake->dateTimeThisYear,
                'is_released' => $fake->boolean(),
                'created_at' => new DateTime,
                'updated_at' => new DateTime,
                'company_id' => $fake->randomElement($company_ids),
            ]);
        }
    }
}


