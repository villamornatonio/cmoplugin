<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enrollments', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('trainee_id');
            $table->integer('schedule_id');
            $table->integer('referred_by');
            $table->boolean('is_company_charged');
            $table->boolean('is_completed');
            $table->integer('created_by');
            $table->string('company');
            $table->boolean('has_fully_paid');
            $table->boolean('has_partially_paid');
            $table->date('date_completed');
            $table->boolean('is_released');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('enrollments');
    }
}
