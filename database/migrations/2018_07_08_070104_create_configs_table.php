<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('shop_url');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('shop_hostname');
            $table->string('api_key');
            $table->string('password');
            $table->string('location_id');
            $table->string('tracking_company');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configs');
    }
}
