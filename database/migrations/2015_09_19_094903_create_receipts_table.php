<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('trainee_id');
            $table->integer('room_id');
            $table->string('room_name',60);
            $table->integer('course_id');
            $table->string('course_name',60);
            $table->string('course_code',60);
            $table->string('course_date',60);
            $table->string('course_time',60);
            $table->integer('discount');
            $table->string('or_no',60);
            $table->integer('amount_paid');
            $table->integer('price');
            $table->integer('total_amount');
            $table->integer('remaining_balance');
            $table->integer('created_by');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('receipts');
    }
}
