<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateTraineesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trainees', function (Blueprint $table) {
            $table->increments('id');
            $table->string('position',50);
            $table->string('firstname',50);
            $table->string('middlename',50);
            $table->string('lastname',50);
            $table->string('passport_no',50);
            $table->string('seamans_no',50);
            $table->string('mobile',50);
            $table->date('dob',50);
            $table->string('avatar',50);
            $table->string('email',50);
            $table->string('emergency_person',50);
            $table->string('emergency_contact',50);
            $table->integer('created_by');
            $table->timestamps();
            $table->integer('company_id')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trainees');
    }
}
