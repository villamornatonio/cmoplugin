<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('shopify_order_id');
            $table->integer('user_id');
            $table->string('item_name');
            $table->string('amount');
            $table->string('buyer');
            $table->string('buyer_email');
            $table->string('buyer_phone');
            $table->string('street');
            $table->string('province');
            $table->string('city');
            $table->string('barangay');
            $table->string('zip');
            $table->string('status');
            $table->string('tracking_number')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
