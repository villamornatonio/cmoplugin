<?php
/**
 * Created by PhpStorm.
 * User: villamornatonio
 * Date: 9/13/15
 * Time: 11:49 AM
 */

return [
    'name' => env('COMPANY_NAME'),
    'phone' => env('COMPANY_PHONE'),
    'address' => env('COMPANY_ADDRESS'),
    'shortName' => env('COMPANY_SHORTNAME'),
    'email' => env('COMPANY_EMAIL')
];