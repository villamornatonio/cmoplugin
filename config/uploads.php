<?php

return [
    'user' => 'pmfi/uploads/user/avatar/',
    'trainee' => 'pmfi/uploads/trainee/avatar',
    'video' => 'dragonshub/media/video/',
    'pdf' => 'dragonshub/media/pdf/',
];